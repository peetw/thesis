%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Motivation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

With the shift to sustainable flood risk management and the increasing awareness of the benefits of riparian woodland, it is imperative that the impact of such vegetation on the flood characteristics of natural river systems is understood. This is of particular importance when using numerical models to assess the flood risk of a particular catchment where lives and property may be affected by inaccurate predictions. However, as will be explored briefly here and in detail in section~(\autoref{sec:veg-drag}), current widely-adopted methods for the treatment of vegetative flow resistance in numerical modelling can be improved.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Current Practices and Limitations}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The definition for riparian woodland covers a relatively broad range of habitats and species, including mature natural woodland, restoration woodland planted at a specific density and pattern, and short rotation coppice used as energy crops. For flood flows through such regions the flow-vegetation interactions are numerous and complex. As a result, typical modelling approaches have relied on abstracting the vegetation as an increase to the surface roughness. The most widely utilized of the roughness coefficients are Manning's $n$ and the Darcy-Weisbach friction factor $f$.
\nomenclature[n]{$n$}{Manning friction coefficient}
\nomenclature[f]{$f$}{Darcy-Weisbach friction factor}

While this approach is trivial to implement and requires no extra parameters, it can require significant calibration to obtain a value that provides a reasonable approximation. In addition,  calibration is typically only performed with respect to the water elevation or inundation extent as velocity measurements are rarely available on the floodplain, and therefore localized flow phenomenon may not be accurately recreated. Furthermore, Manning's $n$ values are dependent on the flow depth and constant values for $n$ and $f$ do not take into account the flexibility or heterogeneity of natural vegetation.

An alternative approach is to directly model the drag force exerted on the vegetation by the flood flows. For vegetation which can be assumed to be rigid and cylindrical, such as mangroves, the relationship between the flow velocity and the drag force follows a well-known relationship, with only a standard drag coefficient and the diameter of the vegetation required as inputs. However, natural vegetation is often flexible (compared to the hydrodynamic force exerted) and variable in morphology and therefore cannot be approximated as rigid or cylindrical.

As a consequence, recent research has focused on parameterizing the drag force in terms of the vegetation's measurable physical properties. However, while drag force experiments involving rigid cylinders are numerous, those involving full-scale trees are much rarer due to the size constraints of most experimental facilities. The paucity of data sets means that there is still considerable uncertainty over which physical properties may be most useful in parameterizing the drag force exerted on trees within riparian woodland regions.
