%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Thesis Scope and Objectives}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The main aims of this thesis can be identified as:
%
\begin{enumerate}
	\item Analyse photographs and video footage of full-scale trees from a large experimental drag force study in order to assess how the trees bend and deform in response to hydrodynamic loading and how this affects their hydraulic resistance. 
	\item Formulate practical and physically-based models for predicting \textit{a priori} the hydrodynamic resistance of flexible riparian vegetation, namely trees and saplings. The models will be developed from sound theoretical reasoning so that they may be applicable to as wide a range of flow conditions as possible, without the need for time-consuming calibration.
	\item Optimize an existing numerical modelling software code to take advantages of advances in heterogeneous computing so that flood risk mapping can be carried out over large reaches in a manageable time frame.
	\item Utilize the new hydrodynamic resistance models to asses the potential impact of riparian woodland on typical and extreme fluvial flood events at a case study site in Somerset, UK.  This will be investigated through a range of different floodplain arrangements, including no vegetation, native riparian woodland, energy crops, and floodplain storage.
\end{enumerate}

This research is necessary to further extend existing knowledge and assist policy-makers in making key decisions regarding land use and flood risk management.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Thesis Layout}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Given the aims and objectives outlined above, this thesis is split into two main sections: experimental data analysis and model development; and numerical modelling of the flooding processes at a riparian case study site. Synopses for each of the chapters in this thesis are provided in the following paragraphs.

An overview of the current literature with respect to the aero- and hydro-dynamic modelling of trees and other vegetation is given in the second chapter (\autoref{sec:lit-review}). The topics explored include: a general background to aero- and hydro-dynamic drag force; existing models for predicting the drag force of rigid and flexible vegetation under emergent and submerged flow conditions; and previous numerical modelling studies involving vegetated flows.

The third chapter (\autoref{sec:experimental-data}) begins the first of the two main themes and introduces the two experimental data sets used in this thesis: the first consists of high-resolution drag force and physical property measurements for a large number of foliated and defoliated full-scale trees; the second is independent and contains similar measurements for partially and fully submerged tree branches.

After investigating the impact of foliage on the trees' drag characteristics and quantifying the specimens' reconfiguration, photos and videos of the trees in still air and during drag force testing are analysed using sophisticated image processing techniques. This enables the variation in the trees' projected areas, and thus drag coefficients, with flow velocity to be determined and discussed, providing insight into the deformation of flexible vegetation under hydrodynamic loading.

In the fourth chapter (\autoref{sec:drag-force-models}), two drag force models are proposed: the first is empirical and uses the vegetation's projected area in still air and a modified Reynolds number to predict the variation in drag coefficient with flow velocity; the second is developed from dimensional consideration and strong emphasis is placed on accurately representing the deformation of the trees by considering their flexural rigidity via the Cauchy number. Both models are applied to the experimental tree and branch data from the previous chapter and compared to existing drag force approaches. A parameter sensitivity analysis is also carried out for the second model.

The fifth chapter (\autoref{sec:modelling}) contains the research relevant to the second of the two main themes in this thesis. It begins by revising the underlying equations of motion for a fluid and the numerical scheme chosen to solve those equations. The Cauchy drag force model from the previous chapter is then inserted into an existing computational fluid dynamics code. General optimizations, both serial and parallel, are performed and the code is refactored to enable it to take advantage of recent advances in heterogeneous computing.

The case study site is then introduced and the numerical model set up and subsequently calibrated against flow gauging data. To assess the impact of riparian woodland on the flood characteristics of the site, a number of different scenarios are modelled, including: removing all woodland from the floodplains; utilizing the existing riparian woodland; replacing the existing woodland with short rotation coppice plantations; expanding the short rotation coppices to cover the full extent of the floodplains; and mixing woodland and floodplain storage by introducing earthen bunds at the downstream boundary.

Finally, a summary of the main findings in this thesis and suggested areas for future research are presented in the final chapter (\autoref{sec:conclusion}).
