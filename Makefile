# Utilities
LATEXMK = latexmk
GNUPLOT = gnuplot
INKSCAPE = inkscape

# Main TeX and PDF files
TEX_SRC := $(shell find . -type f -name "*.tex")
MAIN_SRC = Thesis.tex
MAIN_PDF := $(MAIN_SRC:.tex=.pdf)

# BibTeX files
BIB_SRC := $(MAIN_SRC:.tex=.bib) $(MAIN_SRC:.tex=.bst)

# Gnuplot files
IMG_DIR = images
GNU_SRC := $(shell find . -type f -name "*.plt" -o -name "*.gnu")
ifdef GNU_SRC
GNU_OUT := $(shell grep -H -e "set out" -e "RESULTS =" $(GNU_SRC) | sed 's/\(.*\/\).*\.[pltgnu]*.*"\(.*\)"/\1\2/')
endif
GNU_PDF := $(filter %.pdf,$(GNU_OUT))
GNU_DEP = .depend

# SVG files
SVG_SRC := $(shell find . -type f -name "*.svg")
SVG_PDF := $(SVG_SRC:.svg=.pdf)

# Ensure make runs in serial (parallel build breaks gnuplot dependencies)
.NOTPARALLEL:

# Non-file targets
.PHONY: all clean clean-pdf clean-extra clean-imgs clean-all

# Default target
all: $(MAIN_PDF)

# Compile main PDF
$(MAIN_PDF): $(TEX_SRC) $(BIB_SRC) $(GNU_PDF) $(SVG_PDF)
	$(LATEXMK) -f -pdf -pdflatex="pdflatex --shell-escape %O %S" $(MAIN_SRC)

# Determine gnuplot dependencies
$(GNU_DEP): $(GNU_SRC)
	./scripts/makegnudep.sh $^ > $(GNU_DEP)

-include $(GNU_DEP)

# Generate gnuplot PDFs
$(GNU_OUT):
	@mkdir -p $(shell dirname $(shell dirname $<))/$(IMG_DIR)
	cd $(shell dirname $<) && $(GNUPLOT) $(shell basename $<) 2>/dev/null

# Convert SVG to PDF
$(SVG_PDF): %.pdf: %.svg
	$(INKSCAPE) -z -f $< -A $@

# Remove intermediary LaTeX files
clean: clean-extra
	$(LATEXMK) -silent -c $(MAIN_SRC)

# Remove all intermediary LaTeX files, including PDF, DVI and PS
clean-pdf: clean-extra
	$(LATEXMK) -silent -C $(MAIN_SRC)

# Remove intermediary LaTeX files that latexmk misses
clean-extra:
	$(RM) $(MAIN_SRC:.tex=.bbl)
	$(RM) $(MAIN_SRC:.tex=.nlg)
	$(RM) $(MAIN_SRC:.tex=.nlo)
	$(RM) $(MAIN_SRC:.tex=.nls)

# Remove gnuplot plots and converted images
clean-imgs:
	$(RM) $(GNU_OUT)
	$(RM) $(SVG_PDF)

# Remove all generated files
clean-all: clean-pdf clean-imgs
	$(RM) $(GNU_DEP)
