%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Experimental Data}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The drag force and physical property data sets analysed in this thesis are sourced from two previous experimental studies. The larger of the two data sets was collected at the Canal de Experiencias Hidrodinamicas de El Pardo (CEHIPAR), Madrid, within the framework of the EU Hydralab III scheme. The second data set is independent and was obtained from experiments undertaken at the Leichtwei\ss-Institut f\"{u}r Wasserbau (LWI), Technische Universit\"{a}t Braunschweig. The corresponding data sets are described below. It should be noted that only the raw data from the experiments are provided in this section and that the new and original analysis is performed in the following sections (\autoref{sec:drag-force-analysis}--\autoref{sec:drag-coef-analysis}).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Hydralab Experiments} \label{sec:hydralab-data}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this study, full-scale riparian trees (heights ranged from 1.8~m to 4.1~m, with an average of 2.9~m) were sampled from a local floodplain site and towed under fully-submerged conditions in a ship towing tank. In total, twenty-one full-scale trees were tested, including five \textit{Alnus glutinosa} (common alder; A1--A5), four \textit{Populus nigra} (black poplar; P1--P4), and twelve \textit{Salix alba} (white willow; S1--S12) specimens. As the trees were collected during the spring months of mid March to mid April, a range of stages of leaf development were captured, from emerging leaf buds to more fully developed foliage.

The data were collected over a four week period during the spring of 2008 by members of Cardiff University (CU) in collaboration with Technische Universit\"{a}t Braunschweig (UB), Germany, the University of Natural Resources and Life Sciences (BOKU), Austria, and Forest Research UK (see Table~\ref{tab:hydralab-participants}). This section provides the relevant data and an overview of the experimental procedure; full details can be found in \citet{Weissteiner-09, Xavier-09} and \citet{Xavier-10}.

\begin{table}[htbp]
\caption{List of participants in the Hydralab experiments. Reproduced from \citet{Xavier-10}.}
\centering
\begin{footnotesize}
\begin{tabular}{lll}
\toprule
{Person} & {Institute} & {Responsibility} \\ \midrule
Catherine Wilson & CU & Project leader \\
Jochen Aberle & UB & Drag force measurements \\
Hans-Peter Rauch & BOKU & Measurement of tree properties \\
Patricia Xavier & CU & Experimental work and force-velocity data analysis \\ 
Thomas Schoneboom & UB & Experimental work and force-velocity post-processing \\
Walter Lammeranner & BOKU & Determination of bending stiffness \\
Clemens Weissteiner & BOKU & Post-processing of tree properties \\
Huw Thomas & FR & Experimental work \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:hydralab-participants}
\end{table}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Physical Properties}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The tested trees were sourced at two week intervals from a local floodplain woodland site, rather than an artificial nursery, and were selected to cover a broad range of growth habitats \citep{Weissteiner-09}. All trees were brought to the laboratory within five hours and kept with their stems submerged in the water of the canal. From visual inspection, the trees remained fresh and leaf growth was observed to continue for approximately two weeks before obvious signs of decay occurred, at which point they were discarded. It should be noted that the experimental time at the \mbox{CEHIPAR} facilities was limited so that not all of the properties could be determined for every tree.

Prior to the towing tank tests, the trees' physical dimensions were measured and recorded. This included the height $H$, and the diameter of the main stem at each quartile height (\ie basal $d_0$, first-quartile $d_{25}$, mid-stem $d_{50}$, and third-quartile $d_{75}$). These properties are summarized in Table~\ref{tab:hydralab-dimensions}.
\nomenclature[d]{$d_0$}{Basal stem diameter}
\nomenclature[d]{$d_{25}$}{First-quartile stem diameter}
\nomenclature[d]{$d_{50}$}{Mid-stem diameter}
\nomenclature[d]{$d_{75}$}{Third-quartile stem diameter}

\begin{table}[htbp]
\caption{Dimensions of the \alnus (A), \populus (P) and \salix (S) Hydralab trees. A dash (-) indicates data not recorded. The suffix `B' denotes a branch cut from the tree with the same prefix.}
\centering
\sisetup{table-format=2.1}
\begin{footnotesize}
\begin{tabular}{lS[table-format=1.2]SSSS}
\toprule
& {Height} & {Diam.} & {Diam.} & {Diam.} & {Diam.} \\
{Specimen} & {$H$ (m)} & {$d_0$ (mm)} & {$d_{25}$ (mm)} & {$d_{50}$ (mm)} & {$d_{75}$ (mm)} \\ \midrule
A1 & 2.45 & 58 & 29 & 25 & 11 \\
A2 & 3.6 & 35 & 33 & 18 & 9 \\
A3 & 2.6 & 28 & 21 & 16 & 5 \\
A4 & 2.4 & 27 & 22 & 16 & 8 \\
A5 & 1.8 & 28 & 20 & 12 & 8 \\ \midrule
Average & 2.57 & 35.4 & 25 & 17.5 & 8 \\ \midrule
P1 & 2.68 & 35 & 18 & 12 & 8 \\
P2 & 3.77 & 37 & 23 & 21 & 8 \\
P2B1 & 3.77 & 37 & 23 & 21 & 8 \\
P2B2 & 2.5 & {-} & 22 & 20 & 8 \\
P3 & 2.6 & 23 & 20 & 16 & 7 \\
P4 & 3.9 & 35 & 29 & 22 & 16 \\
P4B1 & 2.3 & 22 & {-} & 16 & {-} \\
P4B2 & 1.8 & 16 & 15 & 10 & 6 \\ \midrule
Average & 2.92 & 29.1 & 21.4 & 17.3 & 8.8 \\ \midrule
S1 & 2.1 & 29 & 25 & 17 & 8 \\
S2 & 2.4 & 26 & 21 & 11 & 3 \\
S3 & 3.95 & 44 & 40 & 28 & 10 \\
S4 & 2 & 23 & 16 & 9 & 5 \\
S5 & 3.6 & 47 & 14 & 18 & 9 \\
S5B1 & 3.6 & 47 & 14 & 18 & 9 \\
S5B2 & 3.6 & 47 & 14 & 11 & 8 \\
S6 & 3.2 & 25 & 12 & 9 & 5 \\
S6B1 & 3.2 & 25 & 12 & 9 & 5 \\
S6B2 & 2.8 & {-} & 16 & 8 & 4 \\
S7 & 2.3 & 31 & 19 & 15 & 8 \\
S7B1 & 2.3 & 31 & 19 & 15 & 8 \\
S7B2 & 2.3 & 31 & 21 & 18 & 8 \\
S7B3 & 2.18 & 31 & 19 & 14 & 7 \\
S8 & 3 & 20 & 17 & 13 & 5 \\
S9 & 3.6 & 29 & 23 & 14 & 7 \\
S10 & 3.24 & 33 & 31 & 22 & 14 \\
S11 & 3.5 & 26 & 18 & 11 & 7 \\
S12 & 4.1 & 29 & 21 & 16 & 7 \\ \midrule
Average & 3 & 32.1 & 19.5 & 14.6 & 7.2 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:hydralab-dimensions}
\end{table}

After a specimen had been tested in a foliated state, the foliage was removed and the fresh, or wet leaf mass $M_{L,w}$ was recorded using a scale with an accuracy of 0.1~g. The leaf volume $V_L$ was determined by immersing the collected foliage in a known volume of water and recording the volume of water displaced. The foliage was then allowed to dry in a warm oven for at least 24 hours so that the dry leaf mass $M_{L,d}$ could be noted. Once a defoliated specimen had finished being tested, it was cut into sections and the wet mass $M_{S,w}$, volume $V_S$ and dry mass $M_{S,d}$ of the wood was recorded using the same procedure as for the leaf matter. The trees' stem, leaf, and total volumes and masses are presented in Tables~\ref{tab:hydralab-mass-wood}--\ref{tab:hydralab-mass-total}, respectively.
\nomenclature[mmlw]{$M_{L,w}$}{Wet leaf mass}
\nomenclature[mmld]{$M_{L,d}$}{Dry leaf mass}
\nomenclature[vv]{$V_L$}{Leaf volume}
\nomenclature[mmsw]{$M_{S,w}$}{Wet stem and branch mass}
\nomenclature[mmsd]{$M_{S,d}$}{Dry stem and branch mass}
\nomenclature[vv]{$V_S$}{Stem and branch volume}
\nomenclature[mm]{$M_w$}{Total wet mass}
\nomenclature[mm]{$M_d$}{Total dry mass}
\nomenclature[vv]{$V$}{Total volume}

In addition to the above properties, tests were also carried out to determine the trees' flexural rigidity $EI$ and modulus of elasticity $E$. Each tree was fixed at its base to the edge of a secure table using G-clamps. Weights were then attached to the horizontal tree at either the first-quartile or mid-stem position and the resulting vertical deflection recorded. The weights were increased incrementally from 0.2~kg to 13~kg. The properties derived from placing weights at each position are denoted using a subscript suffix of 25 or 50 respectively. For example, $EI_{50}$ would indicate the flexural rigidity calculated from placing weights at the mid-stem position.
\nomenclature[ee]{$EI_{25}$}{First-quartile stem flexural rigidity}
\nomenclature[ee]{$EI_{50}$}{Mid-stem flexural rigidity}

Although classical beam bending theory (\eg \citealp{Coates-90}) is derived for small deflections and based on the assumption that the beam is uniform in cross-section and has linear elasticity, it is often used in biomechanical studies of natural vegetation \citep{Chen-11, Stone-11}. Since the assumptions may not hold for natural vegetation (non-constant thickness, large deformations, differing properties of young and old wood, \etc) an averaging method was employed.

For each weight applied to the tree, the corresponding flexural rigidity value was calculated. Once the deflections became too large, no more weights were added and the previous flexural rigidity values were averaged to give a single result. While this method provides reasonably accurate approximations, a more detailed or rigorous procedure was outside the scope of this project. Representing the main stem as a beam of length $L$ that is fixed at one end and has a concentrated load $P$ at a distance $l$ from the free end, the deflection $\delta$ can be expressed:
%
\begin{equation}
\delta = \frac{P}{3 E I} (L-l)^3
\label{eq:beam-deflection}
\end{equation}
%
where $I\!=\!\frac{\pi r^4}{4}$ is the second moment of area for a circular cross-section of radius~$r$.
\nomenclature[ii]{$I$}{Second moment of area}

Following the above procedure, flexural rigidity properties were calculated from placing weights at both the first-quartile ($EI_{25}$) and mid-stem ($EI_{50}$) heights. These are summarized in Table~\ref{tab:hydralab-stiffness} for the alder, poplar and willow specimens, along with the equivalent modulus of elasticity values. Due to the tapering thickness of the trees' main stems, the average radius $r$ of the stem between the base and the point of loading was used to determine $I$. The modulus of elasticity values reported in Table~\ref{tab:hydralab-stiffness} are consistent with those given in the literature for similar willow trees \citep{Green-99, Stone-11}.

\begin{table}[htbp]
\caption{Flexural rigidity and modulus of elasticity values for the \alnus (A), \populus (P) and \salix (S) Hydralab trees. A dash (-) indicates data not recorded. The suffix `B' denotes a branch cut from the tree with the same prefix.}
\centering
\begin{footnotesize}
\begin{tabular}{lS[table-format = 3]S[table-format = 3]S[table-format = 1.2e2]S[table-format = 1.2e2]}
\toprule
 & {$EI_{25}$} & {$EI_{50}$} & {$E_{25}$} & {$E_{50}$} \\
{Specimen} & {(\bstiff)} & {(\bstiff)} & {(\stress)} & {(\stress)} \\ \midrule
A1 & 71 & 102 & 3.91e8 & 6.74e8 \\
A2 & 430 & 249 & 6.49e9 & 1.03e10 \\
A3 & 64 & 135 & 3.46e9 & 1.16e10 \\
A4 & 106 & 82 & 5.96e9 & 7.58e9 \\
A5 & 31 & 16 & 1.93e9 & 1.98e9 \\ \midrule
Average & 140 & 117 & 3.64e9 & 6.43e9 \\ \midrule
P1 & 135 & 42 & 5.5e9 & 2.72e9 \\
P2 & 122 & 184 & 3.23e9 & 5.48e9 \\
P2B1 & 122 & 184 & 3.23e9 & 5.48e9 \\
P2B2 & 37 & 43 & {-} & {-} \\
P3 & 59 & 47 & 5.55e9 & 6.24e9 \\
P4 & 664 & 541 & 1.22e10 & 1.66e10 \\
P4B1 & 136 & 111 & {-} & {-} \\
P4B2 & {-} & {-} & {-} & {-} \\ \midrule
Average & 182 & 165 & 5.94e9 & 7.3e9 \\ \midrule
S1 & {-} & {-} & {-} & {-} \\
S2 & 119 & 141 & 7.58e9 & 2.29e10 \\
S3 & 367 & 559 & 2.34e9 & 6.81e9 \\
S4 & 31 & 42 & 4.33e9 & 1.38e10 \\
S5 & {-} & {-} & {-} & {-} \\
S5B1 & 65 & 56 & 1.5e9 & 1.01e9 \\
S5B2 & 157 & 51 & 3.79e9 & 1.44e9 \\
S6 & {-} & {-} & {-} & {-} \\
S6B1 & 20 & 18 & 3.4e9 & 4.06e9 \\
S6B2 & 47 & 20 & {-} & {-} \\
S7 & {-} & {-} & {-} & {-} \\
S7B1 & 85 & 72 & 4.14e9 & 5.07e9 \\
S7B2 & 84 & 48 & 3.68e9 & 2.66e9 \\
S7B3 & {-} & {-} & {-} & {-} \\
S8 & 35 & 40 & 5.89e9 & 1.06e10 \\
S9 & {-} & {-} & {-} & {-} \\
S10 & 258 & 349 & 4.89e9 & 1.24e10 \\
S11 & 64 & 52 & 5.62e9 & 8.7e9 \\
S12 & 141 & 174 & 7.45e9 & 1.4e10 \\ \midrule
Average & 113 & 125 & 4.55e9 & 8.62e9 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:hydralab-stiffness}
\end{table}

\clearpage


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Drag Forces}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The facilities at CEHIPAR included a calm water ship towing tank, with the following dimensions: 320~m long, 12.5~m wide and 6.5~m deep. A towing carriage was mounted on rails running the length of the canal (Fig.~\ref{fig:towing-tank-schematic}) and the speed at which it travelled in either direction could be set with an accuracy of 1~\mmps. The towing carriage had a maximum velocity of 10~\mps and a maximum acceleration of 1~\acc. A dynamometer, consisting of five separate load cells, was suspended underneath the carriage in order to measure the forces and moments in the three Cartesian axes at a rate of 10~Hz and with an accuracy of \num{9.8e-3}~N.

\begin{figure}[htbp]
\centering
\includegraphics{towing-tank-schematic}
\caption{Plan view schematic of the towing carriage at the CEHIPAR facilities.}
\label{fig:towing-tank-schematic}
\end{figure}

The tree specimens were attached upside down to the dynamometer so that they were fully submerged and were towed with velocities ranging from 0.125~\mps to 6~\mps. Although flood flows rarely reach such high velocities in practice, the large range provides important test cases for any proposed models. The use of fully submerged conditions was necessary due to the experimental facilities, but provides insight into the nature of the deformation of full-scale trees in extreme flow conditions.

To ensure repeatability of results each experiment was carried out twice, \ie specimens were towed in both directions along the towing tank by the carriage. After each run the trees were rotated through 180 degrees in the dynamometer so that the direction of bending was consistent. Sufficient time (5--10 minutes) was also reserved between runs to allow any surface waves or eddies to dissipate before towing again.

The majority of trees were tested first in a foliated and then in a defoliated state. However, during preliminary tests it was observed that the trees exhibited apparent signs of inelastic deformation. It was therefore decided that the trees would be exposed to lower towing velocities while in their foliated state in order to minimize the impact on the subsequent defoliated tests. 

Additionally, a number of trees were selected for further testing based on their branch morphology. These trees were only tested in a foliated state and then cut into their constituent stems, which were then tested separately in foliated and defoliated states. This allowed the contribution of the tree's branches to its overall drag force to be investigated. However, it also meant that the original main specimen could not be tested in a defoliated state since it was no longer whole. The sub-branches of the trees that contained multiple main stems are hereafter denoted using the suffix `B' combined with the number of the respective sub-branch. For example, the first branch of the S5 tree is labelled S5B1, with the second branch being S5B2, and so on.

When undertaking the drag force testing, the length of the towing tank enabled multiple velocity readings to taken during a single run. However, the transition between velocities caused a small peak in the measured force which then subsided, as can be seen in Figure \ref{fig:F-vs-time}. This was due to the acceleration of the carriage as it increased its velocity to the next testing velocity.

\begin{figure}[htbp]
\centering
\includegraphics[width=\textwidth]{F-vs-time}
\caption{Component forces against time for a single run incorporating a number of velocities. Reproduced from \cite{Xavier-09}.}
\label{fig:F-vs-time}
\end{figure}

In order to account for the peak at the start of each velocity time series and obtain representative values, a statistical analysis was carried out by Thomas Schoneboom of Technische Universit\"{a}t Braunschweig, Germany. On the basis of his analysis, the force and moment values were averaged over the 20~s period prior to the end of each velocity measurement where the recorded variation was minimal \citep{Xavier-09}. The resulting variation in drag force with towing velocity for each of the Hydralab trees is presented in Figs.~\ref{fig:F-vs-U-alnus}--\ref{fig:F-vs-U-salix}.

\begin{figure}[htbp]
\centering
\includegraphics{F-vs-U-alnus}
\caption{Variation in drag force with towing velocity for the: (a) foliated; and (b) defoliated \alnus trees from the Hydralab experiments.}
\label{fig:F-vs-U-alnus}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics{F-vs-U-populus}
\caption{Variation in drag force with towing velocity for the: (a) foliated; and (b) defoliated \populus trees from the Hydralab experiments.}
\label{fig:F-vs-U-populus}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics{F-vs-U-salix}
\caption{Variation in drag force with towing velocity for the: (a) foliated; and (b) defoliated \salix trees from the Hydralab experiments.}
\label{fig:F-vs-U-salix}
\end{figure}

\clearpage


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{LWI Experiments} \label{sec:LWI-data}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The independent data used here are taken from flume experiments conducted at the Leichtwei\ss-Institut f\"{u}r Wasserbau (LWI), Technische Universit\"{a}t Braunschweig. The experimental set-up is described in detail in \citet{Schoneboom-09,Schoneboom-11} and \citet{Dittrich-12}. The data from this study consist of drag force and physical property measurements for isolated foliated branches of natural willow, natural poplar, and artificial poplar, tested in partially ($\zeta = 0.12$~m) and fully ($\zeta = 0.25$~m) submerged conditions (Fig.~\ref{fig:LWI-specimens}). The artificial poplar is included here as it has been found to show similar resistance behaviour to its natural counterparts \citep{Dittrich-12}.

\begin{figure}[htbp]
\centering
\setkeys{Gin}{width=0.425\textwidth}
\subfloat[Natural willow]{\label{fig:LWI-willow-natural}\includegraphics{LWI-willow-natural}}
\hfill
\subfloat[Natural poplar]{\label{fig:LWI-poplar-natural}\includegraphics{LWI-poplar-natural}}
\\
\hfill
\subfloat[Artificial poplar]{\label{fig:LWI-poplar-artificial}\includegraphics{LWI-poplar-artificial}}
\hfill{}
\caption{Photographs of the fully submerged natural and artificial branches used in the LWI experiments. The images were captured in still water using a submersible digital camera.}
\label{fig:LWI-specimens}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Drag Forces}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Drag forces were recorded using a custom strain gauge setup (described in detail in \citealp{Schoneboom-08}) that had a temporal resolution of 1613~Hz and an accuracy of 0.02~N. The drag force measurement system (DFS) was installed underneath a tilting flume measuring 32~m long, 0.6~m wide and 0.4~m deep. The flume bed was covered with a rubber mat with a pyramidal roughness height of 3~mm.

The individual branches were placed into the flume one at a time and secured to the DFS. Steady uniform flow was then obtained and the drag force on the individual branches measured at a rate of 200~Hz for a duration of 60~s. The measurements were carried out for a range of velocities ($0.198 \le U \le 0.957$~\mps) at two different water depths: partially submerged conditions ($\zeta = 12$~cm) and fully submerged conditions ($\zeta = 25$~cm). The resulting time-averaged drag forces at each velocity are presented in Fig.~\ref{fig:LWI-F-vs-U}.

\begin{figure}[htbp]
\centering
\subfloat{\label{fig:LWI-F-vs-U-h12}\includegraphics{LWI-F-vs-U-h12}}
\hfill
\subfloat{\label{fig:LWI-F-vs-U-h25}\includegraphics{LWI-F-vs-U-h25}}
\caption{Variation in drag force with flow velocity for the: (a) partially submerged; and (b) fully submerged branches from the LWI experiments. The natural and artificial plants are denoted using (N) or (A), respectively.}
\label{fig:LWI-F-vs-U}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Physical Properties}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In addition to the force-velocity data, key physical properties, such as the stem diameter, height, total one-sided leaf area, and projected area in still water were recorded \citep{Schoneboom-11}. The images used to calculate the projected area in still water were obtained using a submersible digital camera and were taken from a front-facing direction. Although the projected area in still water is not directly comparable to the projected area in still air due to buoyancy effects, they are taken to be equivalent in this thesis as the buoyancy effects are observed to be minimal (see Fig.~\ref{fig:LWI-specimens}).

Bending stiffness tests were also carried out to determine the elastic modulus for the wire stem of the artificial poplar \citep{Schoneboom-11}. For the two natural branches, main-stem elastic moduli are taken from the species-averaged values for the Hydralab trees (see Table~\ref{tab:hydralab-stiffness}).

The measured physical properties for the LWI branches are summarized in Table~\ref{tab:LWI-specimens}. Note that the water depth is used as the specimen height for the partially submerged cases. The proportion of the leaf area that was submerged in the partially submerged case was not listed in \citet{Schoneboom-11}.

\begin{table}[htbp]
\caption{Measured physical properties of the poplar (P) and willow (W) LWI branches. The suffixes `A' and `N' denote artificial and natural branches, respectively, while `1' and `2' denote partially and fully submerged flow conditions, respectively.}
\centering
\sisetup{table-format=3}
\begin{footnotesize}
\begin{tabular}{lS[table-format=2]S[table-format=1.1]S[table-format=1.2e2]SS}
\toprule
 & {Height} & {Stem diam.} & {Elastic mod.} & {Leaf area} & {Proj. area} \\
{Specimen} & {$H$ (cm)} & {$d$ (mm)} & {$E$ (\stress)} & {$A_L$ (cm$^2$)} & {$A_{p0}$ (cm$^2$)} \\ \midrule
WN1 & 12 & 3.2 & 4.55e9~\textsuperscript{a} & {-} & 77 \\
WN2 & 25 & 3.2 & 4.55e9~\textsuperscript{a} & 322 & 205 \\
PN1 & 12 & 3.6 & 5.94e9~\textsuperscript{a} & {-} & 137 \\
PN2 & 24 & 3.6 & 5.94e9~\textsuperscript{a} & 455 & 154 \\
PA1 & 12 & 3 & 6.97e9 & {-} & 95 \\
PA2 & 23 & 3 & 6.97e9 & 374 & 148 \\ \bottomrule \addlinespace
\multicolumn{6}{l}{\textsuperscript{a} Elastic modulus taken from Hydralab trees' species-average.}
\end{tabular}
\end{footnotesize}
\label{tab:LWI-specimens}
\end{table}
