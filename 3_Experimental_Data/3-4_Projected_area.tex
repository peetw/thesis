%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Projected Area Analysis} \label{sec:projected-area-analysis}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this section, the photographs and videos of the Hydralab trees are analysed to determine the trees' projected areas, both in still air and during drag force testing. Quantifying exactly how the projected area varies with flow velocity is important as it allows the rate of the trees' reconfiguration in response to increasing flow velocity to be investigated.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{In Still Air} \label{sec:projected-area-still-air}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Specimen Leaf Area}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Once the trees had been drag force tested in a foliated state, the leaves and buds were removed so that they could be tested in a defoliated state (\autoref{sec:hydralab-data}). During this defoliating process, the leaves and buds were collected so that their physical properties, such as wet and dry mass, volume and density, could be recorded (Table~\ref{tab:hydralab-mass-leaf}). In addition to these properties, the leaves from a selection of trees were scanned using an A4 digital scanner, with the leaves pressed flat and not overlapping \citep{Weissteiner-09}. Not all specimens were chosen to have their leaves scanned due to time constraints at the experimental facilities.

Due to the large sizes of the trees, only 100 leaves were scanned for each selected tree. The dry masses $M^{100}_{L,d}$ were also recorded for those 100 leaves. This enables the specific leaf area (SLA) to be calculated as the ratio of the one-sided area per 100 leaves to their dry mass, \ie $A^{100}_L / M^{100}_{L,d}$. Combining the SLA values with the measurements for the total dry leaf masses $M_{L,d}$ (Table~\ref{tab:hydralab-mass-leaf}) thus allows the total one-sided leaf areas $A_L$ to be estimated. In this study, the procedure used to analyse the scanned images and calculate $A_L$ can be summarized as:
%
\begin{enumerate}
	\item Isolate the leaf pixels from the background by selectively removing certain colours using GIMP (v2.8).
	\item Determine the area per pixel by using the known dimensions of the scanned image (A4).
	\item Calculate $A^{100}_L$ by multiplying the number of leaf pixels by the area per pixel.
	\item Calculate the specific leaf area as the ratio of the one-sided area to the dry mass for each set of 100 leaves, \ie $\textnormal{SLA} = A^{100}_L / M^{100}_{L,d}$.
	\item Calculate the total one-sided leaf area by multiplying the SLA by the total dry leaf mass, \ie $A_L = \textnormal{SLA} \times M_{L,d}$.
\end{enumerate}
\nomenclature[mmldx]{$M^{100}_{L,d}$}{Dry mass of 100 leaves}
\nomenclature[aalx]{$A^{100}_L$}{One-sided area for 100 leaves}
\nomenclature[aal]{$A_L$}{Total one-sided leaf area}

The resulting one-sided leaf areas for the selected trees are presented in Table~\ref{tab:hydralab-leaf-area}. Unfortunately the dry mass of the 100 leaves for the alder specimen A3 was not recorded and therefore an equivalent total one-sided leaf area cannot be computed.

\begin{table}[htbp]
\caption{Total one-sided leaf area for the \alnus (A), \populus (P) and \salix (S) Hydralab trees. The one-sided area and dry mass per 100 leaves are also given.}
\centering
\sisetup{table-format=1.3}
\begin{footnotesize}
\begin{tabular}{lSS[table-format=1.2]S}
\toprule
Specimen & {$A^{100}_L$ (m\sps{2})} & {$M^{100}_{L,d}$ (g)} & {$A_L$ (m\sps{2})} \\ \midrule
A1 & 0.065 & 2.59 & 0.797 \\
A2 & 0.1 & 4.8 & 0.998 \\
A3 & 0.118 & {-} & {-} \\
A4 & 0.082 & 3.33 & 0.735 \\
A5 & 0.09 & 3.45 & 0.576 \\ \addlinespace
P2 & 0.048 & 1.6 & 1.086 \\
P3 & 0.052 & 2.05 & 0.408 \\
P4 & 0.066 & 3.7 & 1.201 \\ \addlinespace
S4 & 0.01 & 0.48 & 0.692 \\
S7 & 0.014 & 0.45 & 1.29 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:hydralab-leaf-area}
\end{table}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Specimen Stem Area}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Once the defoliated trees had been drag force tested, they were cut into sections so that they could be placed in an oven and the dry mass recorded (\autoref{sec:hydralab-data}). Before being dried, however, the stem and branch sections for three specimens were sorted into quartiles and photographed on a large white table using a high resolution digital camera (Canon EOS 400D). A sheet of coloured A4 paper and a red plastic ball of known diameter ($d = 62$~mm) were also included for scale. Not all trees were chosen to have their stems photographed due to time constraints at the experimental facilities.

In this thesis, the images are analysed to obtain the total one-sided stem and branch area $A_S$. The procedure can be summarized as:
%
\begin{enumerate}
	\item Isolate the stem pieces from the background by selectively removing certain colours using GIMP (v2.8).
	\item Isolate the red plastic ball and sheet of A4 paper, again using GIMP.
	\item Determine the area per pixel by using the known dimensions of the red ball and the sheet of A4 paper.
	\item Calculate the total stem area for each quartile by multiplying the number of stem pixels by the area per pixel.
\end{enumerate}
\nomenclature[aas]{$A_S$}{Total one-sided stem and branch area}

The one-sided stem area per quartile for each of the selected trees is shown in Fig.~\ref{fig:hydralab-stem-area}. The error bars indicate the minimum and maximum values calculated by using either the ball or paper derived pixel area, rather than the average of the two. The total one-sided stem area for each specimen is also given in Table~\ref{tab:hydralab-stem-area}. Note that the experimental time at the CEHIPAR facilities was limited so that it was not possible to photograph the stems of every tree.

\begin{figure}[htbp]
\centering
\includegraphics{specimen-As}
\caption{One-sided stem area per quartile for the \alnus (A), \populus (P) and \salix (S) Hydralab trees. Error bars show minimum and maximum values depending on pixel area definition.}
\label{fig:hydralab-stem-area}
\end{figure}

\begin{table}[htbp]
\caption{Total and per-quartile one-sided stem area for the \alnus (A), \populus (P) and \salix (S) Hydralab trees.}
\centering
\sisetup{table-format=3}
\begin{footnotesize}
\begin{tabular}{lSSS[table-format=1.4]}
\toprule
{Specimen} & {$H_1$ (\%)} & {$H_2$ (\%)} & {$A_S$ (m\sps{2})} \\ \midrule
A1 & 0 & 25 & 0.0239 \\
A1 & 25 & 50 & 0.0301 \\
A1 & 50 & 75 & 0.0989 \\
A1 & 75 & 100 & 0.0809 \\ \midrule
 & & Total & 0.234 \\ \midrule
P1 & 0 & 25 & 0.0353 \\
P1 & 25 & 50 & 0.0736 \\
P1 & 50 & 75 & 0.0594 \\
P1 & 75 & 100 & 0.0225 \\ \midrule
 & & Total & 0.191 \\ \midrule
S1 & 0 & 25 & 0.0288 \\
S1 & 25 & 50 & 0.09 \\
S1 & 50 & 75 & 0.2744 \\
S1 & 75 & 100 & 0.1825 \\ \midrule
 &  & Total & 0.576 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:hydralab-stem-area}
\end{table}

It can be seen from Fig.~\ref{fig:hydralab-stem-area} that the variation in stem area as derived from using either the plastic ball, the A4 sheet of paper, or an average of the two is essentially negligible. This thus shows that image processing technique used here is valid and that there is minimal error in the reported $A_S$ values.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Specimen Total Area}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In addition to the photographs of the stems, the trees were photographed in still air before drag force testing. The procedure can be summarized as:
%
\begin{enumerate}
	\item The tree's stem was securely tied to a metal stand, with the tree orientated such that a red arrow on one of the stand's four bases corresponded to the direction in which the tree was later towed.
	\item The stand was then placed directly in front of a large white sheet in order to obscure any background items.
	\item A red plastic ball of known diameter (62~mm) was then attached to the top of a tripod and positioned next to the tree.
	\item The tree was then photographed using a high resolution digital camera (Canon EOS 400D) and the stand rotated by 90 degrees until each `face' and the plan view had been photographed.
\end{enumerate}

This was carried out for all of the foliated trees, except for the poplar specimen P1 and the willow specimen S2 which were photographed in a defoliated state. The procedure used here to calculate the projected area of the trees from the images is as follows:
%
\begin{enumerate}
	\item Isolate the tree from the background / foreground by selectively removing certain colours and regions using GIMP (v2.8).
	\item Isolate the red plastic ball from the rest of the image, again using GIMP.
	\item Determine area per pixel using the known diameter of the red ball.
	\item Count the number of tree pixels using ImageMagick (v6.8) and multiply by the area per pixel to generate the total projected area in still air.
\end{enumerate}

The projected areas for each of the four `faces' (here termed north, east, south, and west, with north being the projected area while towing) and the plan view are presented in Table~\ref{tab:hydralab-Ap}.

\begin{table}[htbp]
\caption{Projected area in still air for the foliated \alnus (A), \populus (P) and \salix (S) Hydralab trees. The directions correspond to projected areas for each `face' of the tree, with north being the projected area while towing. A dash (-) indicates data not recorded. Note that the specimens P1 and S2 are defoliated and are not included in their respective species' average.}
\centering
\sisetup{table-format=1.3}
\begin{footnotesize}
\begin{tabular}{lSSSSS}
\toprule
 & {North} & {East} & {South} & {West} & {Top} \\
{Specimen} & {(m\sps{2})} & {(m\sps{2})} & {(m\sps{2})} & {(m\sps{2})} & {(m\sps{2})} \\ \midrule
A1 & 0.523 & 0.421 & 0.552 & 0.439 & 0.331 \\
A2 & 0.664 & 0.617 & 0.783 & 0.568 & 0.947 \\
A3 & 0.398 & {-} & 0.4 & 0.392 & 0.307 \\
A4 & 0.387 & 0.422 & 0.44 & 0.429 & 0.299 \\
A5 & 0.333 & 0.356 & 0.358 & 0.382 & 0.241 \\ \midrule
Average & 0.46 & 0.45 & 0.51 & 0.44 & 0.42 \\ \midrule
P1~\textsuperscript{a} & 0.159 & 0.163 & 0.155 & 0.137 & 0.068 \\
P2 & 0.567 & 0.721 & 0.835 & 0.63 & 1.024 \\
P3 & 0.269 & 0.312 & 0.382 & 0.304 & 0.185 \\
P4 & 0.709 & 0.703 & 0.659 & 0.681 & 0.542 \\ \midrule
Average & 0.52 & 0.58 & 0.63 & 0.54 & 0.58 \\ \midrule
S1 & 0.5 & 0.469 & 0.544 & 0.521 & 0.494 \\
S2~\textsuperscript{a} & 0.235 & 0.201 & 0.208 & 0.189 & 0.215 \\
S3 & 1.025 & 0.848 & 0.931 & 0.765 & 0.912 \\
S4 & 0.304 & 0.287 & 0.33 & 0.304 & 0.316 \\
S5 & 1.559 & 1.219 & 0.747 & 0.883 & 0.5 \\
S6 & 0.636 & 0.804 & 0.831 & 0.689 & 0.524 \\
S7 & 0.539 & 0.569 & 0.502 & 0.541 & 0.35 \\
S8 & 0.393 & 0.348 & 0.333 & 0.261 & 0.22 \\
S9 & 0.506 & 0.575 & 0.697 & 0.601 & 0.481 \\
S10 & 0.601 & 0.688 & 0.708 & 0.68 & 0.505 \\
S11 & 0.41 & 0.265 & 0.277 & 0.278 & 0.223 \\
S12 & 0.28 & 0.459 & 0.374 & 0.45 & 0.282 \\ \midrule
Average & 0.61 & 0.59 & 0.57 & 0.54 & 0.44 \\ \bottomrule \addlinespace
\multicolumn{6}{l}{\textsuperscript{a} Defoliated trees (not included in species-average).}
\end{tabular}
\end{footnotesize}
\label{tab:hydralab-Ap}
\end{table}

Theoretically, the north and south projected areas should be equal; however, errors may be introduced in the programmatic isolation of the tree pixels from the foreground / background image. This is visualized in Fig.~\ref{fig:hydralab-Ap0}, where the projected areas for the north, south and an average of the two are presented for each Hydralab tree.

\begin{figure}[htbp]
\centering
\includegraphics{specimen-Ap}
\caption{Minimum, maximum, and average of the north and south projected areas in still air for the foliated \alnus (A), \populus (P) and \salix (S) Hydralab trees. Note that the specimens P1 and S2 are defoliated.}
\label{fig:hydralab-Ap0}
\end{figure}

It can be seen from Fig.~\ref{fig:hydralab-Ap0} that, for most trees, the north and south projected areas are in good agreement. However, for a couple of the trees, namely the poplar P2 and the willows S5, S6 and S9, there is more significant divergence. Therefore, in order to minimize any uncertainty introduced during the image analysis, the average of the north and south projected areas, denoted $A_{p0}$, is used hereafter as the reference projected area in still air.
\nomenclature[aapp]{$A_{p0}$}{Projected area in still air}

Before presenting the values for $A_{p0}$, it is interesting to note that the total one-sided stem area for the poplar specimen P1 ($A_S = 0.191$~m\sps{2}; see Table~\ref{tab:hydralab-stem-area}) is 21.7\% greater than the defoliated projected area for the same specimen ($A_{p0} = 0.157$~m\sps{2}). The projected area is lower due to some of the branches overlapping when the tree is projected onto a 2D image. Although only derived from one specimen, this relative difference can be used to estimate the defoliated projected areas for the other two specimens for which stem areas were measured: the alder and willow specimens A1 and S1, respectively. The values for $A_{p0}$ are presented in Table~\ref{tab:hydralab-Ap0} for the foliated and defoliated trees.

\begin{table}[htbp]
\caption{Foliated and defoliated projected areas in still air for the \alnus (A), \populus (P) and \salix (S) Hydralab trees. A dash (-) indicates data not recorded.}
\centering
\sisetup{table-format=1.3}
\begin{footnotesize}
\begin{tabular}{lSS}
\toprule
 & {Foliated} & {Defoliated} \\
{Specimen} & {$A_{p0}$ (m\sps{2})} & {$A_{p0}$ (m\sps{2})} \\ \midrule
A1 & 0.538 & 0.187~\textsuperscript{a} \\
A2 & 0.723 & {-} \\
A3 & 0.399 & {-} \\
A4 & 0.414 & {-} \\
A5 & 0.346 & {-} \\ \midrule
Average & 0.484 & 0.187 \\ \midrule
P1 & {-} & 0.157 \\
P2 & 0.701 & {-} \\
P3 & 0.325 & {-} \\
P4 & 0.684 & {-} \\ \midrule
Average & 0.57 & 0.157 \\ \midrule
S1 & 0.522 & 0.461~\textsuperscript{a} \\
S2 & {-} & 0.221 \\
S3 & 0.978 & {-} \\
S4 & 0.317 & {-} \\
S5 & 1.153 & {-} \\
S6 & 0.734 & {-} \\
S7 & 0.52 & {-} \\
S8 & 0.363 & {-} \\
S9 & 0.601 & {-} \\
S10 & 0.654 & {-} \\
S11 & 0.343 & {-} \\
S12 & 0.327 & {-} \\ \midrule
Average & 0.592 & 0.341 \\ \bottomrule \addlinespace
\multicolumn{3}{p{5cm}}{\textsuperscript{a} Projected area estimated from total one-sided stem area.}
\end{tabular}
\end{footnotesize}
\label{tab:hydralab-Ap0}
\end{table}

While it is informative to know the total projected area in still air, it is often important when modelling vegetation to know how the projected area is distributed over the trees' height so that non-submerged cases may also be modelled. In previous studies where the exact distribution is unknown, it has been assumed that this variation is linear \citep{Jarvela-04a, Aberle-13}.

In this study, the exact distribution of the projected area over the trees' height $H$ can be calculated directly. This is achieved by splitting the processed image of each tree into sub-images of equal height $h$ and counting the number of tree pixels within each segment. In this case the images are split into 20 sections to provide projected areas at 5\% increments of the total height. The resulting distributions are illustrated in Fig.~\ref{fig:Ap-vs-height} for each of the species.

\begin{figure}[htbp]
\centering
\subfloat{\label{fig:Ap-vs-height-alnus}\includegraphics{Ap-vs-height-alnus}}
\hfill
\subfloat{\label{fig:Ap-vs-height-populus}\includegraphics{Ap-vs-height-populus}}
\\
\hfill
\subfloat{\label{fig:Ap-vs-height-salix}\includegraphics{Ap-vs-height-salix}}
\hfill{}
\caption{Cumulative vertical distribution of foliated projected area in still air for the: (a) \alnus; (b) \populus; and (c) \salix Hydralab trees. The defoliated specimens P1 and S2 are denoted using squares and are not included in the lines of best fit calculations.}
\label{fig:Ap-vs-height}
\end{figure}

As might be expected, the cumulative projected areas do not follow a linear trend with increasing percentage height due to the roughly oval profile of the trees. The lines of best fit shown in Figs.~\ref{fig:Ap-vs-height-alnus}--\ref{fig:Ap-vs-height-salix} are based on a sigmoid function of the form:
%
\begin{equation}
f(x) = A + \frac{B - A}{1 + \exp\left[(x-C)/D\right]}
\end{equation}
%
where $A$, $B$, $C$, and $D$ are coefficients to be found via regression. The values for these regression coefficients, along with the coefficients of determination, are presented in Table~\ref{tab:Ap-vs-height-params} for each of the species.

\begin{table}[htbp]
\caption{Species-specific regression parameters for the lines of best fit describing the cumulative vertical distribution of projected area for the foliated Hydralab trees.}
\centering
\sisetup{table-format=2.1}
\begin{footnotesize}
\begin{tabular}{lS[table-format=3.1]S[table-format=-2.1]SSS[table-format=1.3]}
\toprule
{Species} & {A} & {B} & {C} & {D} & {$R^2$} \\ \midrule
\alnus & 101.1 & -12.4 & 32.7 & 15.6 & 0.933 \\
\populus & 101.2 & -6.5 & 38.3 & 13.9 & 0.991 \\
\salix & 100.3 & -4.6 & 36.2 & 11.7 & 0.957 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:Ap-vs-height-params}
\end{table}

\clearpage


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Variation with Towing Velocity} \label{sec:projected-area-variation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In conjunction with the photographs of the Hydralab trees in still air, video footage of the trees during the drag force towing tests was also recorded. Underwater cameras were mounted to the towing carriage, such that one was positioned 3~m perpendicular to the specimens, while the other was attached 5~m ahead of the towing carriage (see Fig.~\ref{fig:towing-tank-schematic}). This provides side and front / rear facing footage of the trees under hydrodynamic loading at varying towing velocities.

In this section, the front- and rear-facing towing tank videos are analysed to obtain the variation in projected area with towing velocity. The procedure used here for processing the videos can be summarized as:
%
\begin{enumerate}
	\item For each test run video, a frame is taken at the start of the run when the carriage is stationary and then further separate frames are taken at each tested velocity. The frames are only taken once the tree is observed to have finished reconfiguring due to the towing velocity increase.
	\item The video frames are then cropped to a rectangle, enclosing only the tree as bounded by the water surface.
	\item To reduce pixel noise caused by the low resolution and sensitivity of the underwater camera, an anisotropic bilateral filter is applied using G'MIC (v1.5.5).
	\item High frequency `contours' are then obtained by splitting the images into low and high frequencies based on a smoothness parameter, again using G'MIC.
	\item The outline of the tree at each velocity is then determined by running an edge detection algorithm with a Sobel operator on the high frequency contours using GIMP (v2.8).
	\item The resulting outlines are then converted to binary images based on a brightness threshold, again using GIMP.
	\item An `erode' and `dilate' pass is next applied using G'MIC in order to fill any discontinuities in the tree's areas.
	\item Finally, a particle analysis is performed using ImageJ (v1.47) and any regions not contiguous with the tree are removed.
\end{enumerate}

The fully automated method described above produces binary (\ie black and white) images where the black pixels represent the pixels occupied by the tree. As an example, the cropped raw and processed stills for each velocity from the footage for a single backwards-travelling run are presented in Fig.~\ref{fig:vid-front-S1}.

\begin{figure}[htbp]
\centering
\setkeys{Gin}{height=0.15\textheight}
\subfloat[$U = 0$~\mps]{\label{fig:vid-front-S1-0-000}\includegraphics{vid-front-S1-0-000}}
\hfill
\subfloat[$U = 0.25$~\mps]{\label{fig:vid-front-S1-0-250}\includegraphics{vid-front-S1-0-250}}
\\
\subfloat[$U = 0.5$~\mps]{\label{fig:vid-front-S1-0-500}\includegraphics{vid-front-S1-0-500}}
\hfill
\subfloat[$U = 0.75$~\mps]{\label{fig:vid-front-S1-0-750}\includegraphics{vid-front-S1-0-750}}
\\
\subfloat[$U = 1$~\mps]{\label{fig:vid-front-S1-1-000}\includegraphics{vid-front-S1-1-000}}
\hfill
\subfloat[$U = 1.25$~\mps]{\label{fig:vid-front-S1-1-250}\includegraphics{vid-front-S1-1-250}}
\\
\subfloat[$U = 1.5$~\mps]{\label{fig:vid-front-S1-1-500}\includegraphics{vid-front-S1-1-500}}
\hfill
\subfloat[$U = 1.75$~\mps]{\label{fig:vid-front-S1-1-750}\includegraphics{vid-front-S1-1-750}}
\caption{Raw and processed frames at each velocity for a single test run of the foliated willow tree S1. The camera was positioned in front of the tree as it was towed.}
\label{fig:vid-front-S1}
\end{figure}

As opposed to the image analysis in the previous section, where the area per pixel could be determined, there was no object to provide dimensional scale in the video footage. Therefore, the sum of black (tree) pixels in each binary image is compared to the pixel sum for the still condition to provide the percentage reduction in projected area at each velocity. Combined with the initial projected area in still air $A_{p0}$ (Table~\ref{tab:hydralab-Ap0}), the projected area $A_p$ at each velocity can also be calculated.

It should be noted that during initial testing on the foliated willow specimen S1, it was observed that there was a variation of up to 2.66\% in the measured drag force due to wake effects caused by the front-facing camera \citep{Xavier-09}. Therefore, the camera was removed from its mounting in all other tests where the carriage travelled in a backwards direction. In this study, however, the front-facing footage for the S1 specimen is analysed regardless as the observed variation of up to 2.66\% is deemed to be within acceptable limits.

The majority of the video footage obtained during the drag force towing tests at the CEHIPAR facilities was recorded from behind, \ie the deformation of the rear `face' of the tree was captured. Theoretically, two projections (separated by 180\degrees) of a 3D object onto a 2D plane should provide identical areas. However, during video processing, it was noticed that some trees were deforming and deflecting to such a degree that their proximity to the camera was causing them to either be exaggerated in size or to move out of shot. Unfortunately the focal length of the underwater camera is not known and therefore no allowance can be made for the distortion effects caused by those trees that move particularly close to the camera. The following analysis focuses on those trees where the whole tree is visible and the camera-distance distortion is minimal.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Foliated}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Following the procedure detailed above, the variation in projected area with towing velocity is determined for the foliated trees. A number of artefacts in the video stills, such as patches of bright light from overhead windows, had to be removed manually. The resulting variations are presented in Fig.~\ref{fig:Ap-vs-U-fol}.

\begin{figure}[htbp]
\centering
\includegraphics{Ap-vs-U-fol}
\caption{Variation in: (a) percentage; and (b) absolute projected area with velocity for the foliated Hydralab trees.}
\label{fig:Ap-vs-U-fol}
\end{figure}

From inspection of Fig.~\ref{fig:Ap-vs-U-fol}, it appears that the trees' responses can be split into three regions based on the rate of reduction in the projected area. At the lowest velocities ($U < 0.25$~\mps), there is minimal reduction and the trees may be thought of as `rigid'. As the velocity and thus drag force increases, the trees undergo significant reconfiguration and by $U = 1$~\mps the projected area is around 50\% to 70\% its original value. Above this velocity, the rate of reduction slows again as the majority of the trees' reconfiguration is complete, with the projected area only decreasing by around a further 20\% from $U = 1$~\mps to $U = 2$~\mps.

To the author's knowledge, this is the first time that the variation in projected area with flow velocity has been determined for fully submerged full-scale trees. However, \citet{Vollsinger-05} were able to determine such variation for full-scale trees in a wind tunnel. The authors tested foliated trees of \textit{Populus trichocarpa} (black cottonwood), \textit{Alnus rubra} (red alder) and \textit{Betula papyrifera} (paper birch) for velocities of 4~\mps to 20~\mps and recorded the projected area at each velocity. A similar `three region' pattern to that shown in Fig.~\ref{fig:Ap-vs-U-fol} was observed for the alder and cottonwood trees, with minimal reduction in $A_p$ for $U < 5$~\mps, a rapid reduction in $A_p$ for $5 < U < 12$~\mps, and then a less rapid reduction in $A_p$ for $U > 12$~\mps. However, by a wind velocity of $U = 20$~\mps the trees had reached a greater level of reconfiguration (20\% to 38\%) than those tested in this thesis (Fig.~\ref{fig:Ap-vs-U-fol}). It is also interesting to note that the projected area for the paper birch trees decreased rapidly, even at the lowest velocity tested, perhaps suggesting that the birch trees were more flexible.

The variation in $A_p$ with towing velocity was also measured by \citet{Vastila-13} for stands of foliated \textit{Populus nigra} (black poplar) branches. The branches were tested in fully submerged conditions at velocities of 0.1~\mps to 0.61~\mps. Although the velocities are much lower than those tested for the Hydralab trees in this thesis, the variation follows a similar pattern to that in Fig.~\ref{fig:Ap-vs-U-fol} due to the branches being more flexible.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Defoliated}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The analysis of the video footage for the defoliated trees posed a greater challenge, as the poor resolution ($340 \times 570$ pixels) and contrast of the video made it difficult to detect the edges of the trees using the algorithm described above. Therefore, the outlines for those trees where the defoliated projected area is known (P1 and S2) or can be estimated (A1 and S1; see Table~\ref{tab:hydralab-Ap0}) were manually traced using image processing software. Other trees were not processed in this manner due to time constraints. The resulting variations in projected area with towing velocity for the aforementioned defoliated trees are presented in Fig.~\ref{fig:Ap-vs-U-defol}.

\begin{figure}[htbp]
\centering
\includegraphics{Ap-vs-U-defol}
\caption{Variation in: (a) percentage; and (b) absolute projected area with velocity for the defoliated Hydralab trees.}
\label{fig:Ap-vs-U-defol}
\end{figure}

It can be seen from Fig.~\ref{fig:Ap-vs-U-defol} that the general trend in the reduction in projected area can be characterized in a similar manner to the three regions discussed for the foliated trees (Fig.~\ref{fig:Ap-vs-U-fol}). The poplar specimen P1, however, exhibits slightly different behaviour in that it remains less deformed at lower velocities and only starts to significantly reduce in projected area at $U \approx 0.5$~\mps. It also deforms less overall, with the magnitude of the reduction in projected area at each velocity being lower than the other defoliated trees. This is most likely due to the specimen being stiffer and more resistant to deformation, as seen in the higher flexural rigidity values recorded for the poplar trees compared to the alder and willow trees (see Table~\ref{tab:hydralab-stiffness}).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Impact of Foliage}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The impact of foliage on the reconfiguration of the trees can be examined by comparing the rate of reduction in the projected area for the two trees for which there is both foliated and defoliated data (A1 and S1). The foliated and defoliated variations are presented simultaneously in Fig.~\ref{fig:Ap-vs-U-fol-defol}.

\begin{figure}[htbp]
\centering
\includegraphics{Ap-vs-U-fol-defol}
\caption{Impact of foliage on the variation in: (a) percentage; and (b) absolute projected area with velocity for the available Hydralab trees.}
\label{fig:Ap-vs-U-fol-defol}
\end{figure}

It can be seen from Fig.~\ref{fig:Ap-vs-U-fol-defol} that, for the alder specimen A1, the rate of reduction in projected area with towing velocity is greater when it is foliated. This is to be expected since the foliage will increase in total drag force at a given velocity (see Fig.~\ref{fig:foliage-drag}) and thus cause greater reconfiguration.

In the case of the willow specimen S1, however, the rate of reduction in projected area appears to be more similar between the foliated and defoliated states. This may seem contradictory, but is most likely due to the fact that the S1 specimen was tested during the early stages of the Hydralab experimental study and was thus not as foliated. This can be quantified by calculating the leaf to wood volume ratio $V_L / V_S$ for the two specimens (see Tables~\ref{tab:hydralab-mass-wood} and \ref{tab:hydralab-mass-leaf} for $V_S$ and $V_L$, respectively). For the alder specimen A1 this ratio is 15.9\%, while for the willow specimen S1 it is just 4.4\%, thus confirming that it had a substantially lower level of foliation.

It is also interesting to note that, for both foliage states, the rate of reduction in projected area with towing velocity is greater for the alder specimen A1 (Fig.~\ref{fig:Ap-vs-U-fol-defol}). This suggests that it is more flexible and prone to deformation than the willow specimen S1. Although bending stiffness data is not available for the S1 specimen, the flexural rigidity value for the A1 specimen is indeed roughly two-thirds the willows' overall average (Table~\ref{tab:hydralab-stiffness}).
