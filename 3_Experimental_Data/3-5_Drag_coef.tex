%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Drag Coefficient Analysis} \label{sec:drag-coef-analysis}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this section, the drag force measurements (\autoref{sec:hydralab-data}) and previously derived projected areas (\autoref{sec:projected-area-variation}) for the Hydralab trees are combined in order to determine the variation in drag coefficient with towing velocity. The drag coefficients are calculated at each velocity using Eq.~\eqref{eq:Cd-classical}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Foliated}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The resulting drag coefficients for the foliated trees are presented in Fig.~\ref{fig:Cd-vs-U-fol}. It can be seen that, for all specimens, the drag coefficient rapidly decreases from an initial value of $C_d \approx 1$ for $U < 1$~\mps. Above this `threshold' velocity the drag coefficient is relatively constant and ranges from 0.33 to 0.54, depending on the specimen. This is consistent with the observed pattern for the foliated trees' reduction in projected area, where the majority of the trees' reconfiguration was complete by $U \approx 1$~\mps (Fig.~\ref{fig:Ap-vs-U-fol}).

\begin{figure}[htbp]
\centering
\includegraphics{Cd-vs-U-fol}
\caption{Variation in drag coefficient with towing velocity for the foliated Hydralab trees.}
\label{fig:Cd-vs-U-fol}
\end{figure}

The general pattern shown in Fig.~\ref{fig:Cd-vs-U-fol} is also similar to that found by \citet{Vollsinger-05} for \textit{Populus trichocarpa} (black cottonwood), \textit{Alnus rubra} (red alder) and \textit{Betula papyrifera} (paper birch) trees in a wind tunnel. However, while the drag coefficients in that study did decrease from their initial value of around unity, they only reduced to around 0.5 to 0.7 before becoming relatively constant with velocity. This is most likely due to the fact that the projected areas for the trees in the present study were found to reduce by a maximum of 45\% to 70\%, as opposed to around 60\% to 80\% for those trees tested by \citet{Vollsinger-05}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Defoliated}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The drag coefficients for the defoliated Hydralab trees, calculated using the previously derived variations in projected area with towing velocity (Fig.~\ref{fig:Ap-vs-U-defol}), are plot in Fig.~\ref{fig:Cd-vs-U-defol}.

\begin{figure}[htbp]
\centering
\includegraphics{Cd-vs-U-defol}
\caption{Variation in drag coefficient with towing velocity for the defoliated Hydralab trees.}
\label{fig:Cd-vs-U-defol}
\end{figure}

For the specimens P1 and S2, the drag coefficient is relatively constant for $U < 0.3$~\mps (Fig.~\ref{fig:Cd-vs-U-defol}), suggesting that they could perhaps be characterized as a collection of rigid cylinders at such velocities. This agrees with the minimal reconfiguration seen at low velocities for these trees (Fig.~\ref{fig:Ap-vs-U-defol}). For all specimens, however, a general trend of gradually decreasing drag coefficients is observed.

It is interesting to note that the drag coefficients for the A1 and S1 specimens are somewhat less than those for the P1 and S2 specimens. This indicates that the factor used previously to estimate the defoliated projected area from the one-sided stem area for the A1 and S1 specimens (see Table~\ref{tab:hydralab-Ap0}) may have been too high. Unfortunately, there is no further data available to refine the factor in this study.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Impact of foliage}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The impact of foliage on the trees' drag coefficients can be investigated by comparing the results for those trees for which there is both foliated and defoliated data (A1 and S1). The foliated and defoliated drag coefficients are presented simultaneously in Fig.~\ref{fig:Cd-vs-U-fol-defol}.

\begin{figure}[htbp]
\centering
\includegraphics{Cd-vs-U-fol-defol}
\caption{Impact of foliage on the variation in drag coefficient with towing velocity for the available Hydralab trees.}
\label{fig:Cd-vs-U-fol-defol}
\end{figure}

From Fig.~\ref{fig:Cd-vs-U-fol-defol} it can be seen that, for the alder specimen A1, the magnitude of the drag coefficient is less when the specimen is in a foliated state. On-the-other-hand, the opposite is true for the willow specimen S1. However, as previously discussed for Fig.~\ref{fig:Cd-vs-U-defol}, the factor used to estimate the defoliated projected area $A_{p0}$ from the one-sided stem area for the A1 and S1 specimens (see Table~\ref{tab:hydralab-Ap0}) may have been too high.

If that is the case, then the actual defoliated areas in still air would be lower than those used here and thus the drag coefficients for these defoliated specimens would be correspondingly greater. This would then mean that both the A1 and S1 specimens would have a lower drag coefficient when foliated, as opposed to when defoliated. In drag force tests on foliated and defoliated branches of \textit{Salix viminalis} (osier), \textit{Salix alba} (white willow) and \textit{Salix purpurea} (purple willow), \citet{Wunder-11} also found that the drag coefficients were greater when the branches were defoliated.
