%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Drag Force Analysis} \label{sec:drag-force-analysis}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this section, the force-velocity data sets from the Hydralab (\autoref{sec:hydralab-data}) and LWI (\autoref{sec:LWI-data}) experiments are further analysed. The impact of the foliage on the total drag force is first investigated, although only for the Hydralab trees as the LWI branches were not tested in a defoliated condition. The reconfiguration of both sets of specimens as a result of hydrodynamic loading is then quantified and discussed with regards to an existing metric, namely the Vogel exponent $\psi$ (see Eq.~\ref{eq:vogel-exponent}).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Impact of Foliage} \label{sec:drag-force-analysis-foliage}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The testing of foliated and defoliated trees during the Hydralab experiments allows the impact of foliage on the overall drag force to be investigated by considering the ratio of the total foliated drag force to the defoliated drag force, \ie $F_\textnormal{fol} = (F_\textnormal{tot} - F_\textnormal{defol}) / F_\textnormal{tot}$. This analysis shows that as the velocity increases, the percentage of the drag force caused by foliage rapidly decreases until it becomes relatively constant with velocity for a given specimen (Fig.~\ref{fig:foliage-drag}). This suggests that the reconfiguration and streamlining of the foliage (but not necessarily the entire tree) mostly occurs below a certain threshold velocity (see also \citealp{Dittrich-12, Aberle-13}).
\nomenclature[ff]{$F_\textnormal{fol}$}{Drag force due to foliage}
\nomenclature[ff]{$F_\textnormal{defol}$}{Drag force due to stem and branches}

\begin{figure}[htbp]
\centering
\subfloat{\label{fig:foliage-drag-alnus}\includegraphics{foliage-drag-alnus}}
\hfill
\subfloat{\label{fig:foliage-drag-populus}\includegraphics{foliage-drag-populus}}
\\
\hfill
\subfloat{\label{fig:foliage-drag-salix}\includegraphics{foliage-drag-salix}}
\hfill{}
\caption{Proportion of total drag force caused by foliage for the: (a) \alnus; (b) \populus; and (c) \salix Hydralab trees.}
\label{fig:foliage-drag}
\end{figure}

From examination of where the effect of foliage begins to become constant with velocity in Fig.~\ref{fig:foliage-drag}, the threshold velocity appears to range from 0.35~\mps to 0.75~\mps. This is in agreement with the findings of \citet{Vastila-13}, who observed that the reconfiguration of foliated \textit{Populus nigra} branches was mostly complete at flow velocities of 0.6~\mps. At velocities below this threshold, the foliage of the trees in this study is responsible for a significant proportion (40\% to 75\%) of the total drag force. At higher velocities, the contribution of foliage to the total drag force is reduced and ranges from 10\% to 50\%.

\clearpage


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Reconfiguration} \label{sec:drag-force-analysis-reconf}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Assuming that the density $\rho$ of the water at the testing facilities was constant, it follows from Eq.~\eqref{eq:drag-classical} that any deviation away from a quadratic force-velocity response must be due to changes in the drag coefficient $C_d$ and / or the projected area $A_p$. By combining these two terms into a `characteristic' drag coefficient $C_d A_p$ \citep[\eg][]{Wilson-08}, any divergence in the drag force response from the classical, quadratic relationship can be discussed.

The variation in the characteristic drag coefficient with velocity is plot in Fig.~\ref{fig:CdAp-vs-U} for the foliated and defoliated alder, poplar and willow Hydralab trees. The values for $C_d A_p$ were calculated by rearranging Eq.~\eqref{eq:drag-classical}. For all specimens, it can be seen that the relationship between drag force and velocity does indeed deviate away from the quadratic law described by the classical drag equation (Eq.~\ref{eq:drag-classical}), which assumes that $C_d A_p$ is constant. This confirms the unsuitability of representing submerged flexible riparian woodland as rigid cylinders with the unmodified classical drag equation.

\begin{figure}[htbp]
\centering
\includegraphics{CdAp-vs-U}
\caption{Variation in characteristic drag coefficient with velocity for the: (a,b) \alnus; (c,d) \populus; and (e,f) \salix Hydralab trees. The foliated and defoliated trees are in the left and right columns, respectively.}
\label{fig:CdAp-vs-U}
\end{figure}

For the foliated trees, the characteristic drag coefficient decreases rapidly with velocity for $U \lesssim 0.5$~\mps (Fig.~\ref{fig:CdAp-vs-U}), suggesting that the trees were subject to significant reconfiguration within this range. Above this velocity, the rate of change is not as great and at the highest velocities $C_d A_p$ is almost constant.

The variation in characteristic drag coefficient with velocity for the defoliated trees is slightly different to that of the foliated trees (see Fig.~\ref{fig:CdAp-vs-U}). Although there is a similar pattern of relatively rapid decrease in $C_d A_p$ with velocity before it becomes more constant, the velocities at which it happens are higher. There is also a region at the lowest velocities ($U \lesssim 0.5$~\mps) for which $C_d A_p$ is relatively constant. This initial response can be characterized as a `stiff' regime, where the trees undergo minimal reconfiguration and thus act as a rigid object.

This `stiff' regime is not exhibited for the foliated trees in Fig.~\ref{fig:CdAp-vs-U} since the extra surface area of the foliage creates a larger drag force for a given velocity, thus forcing the trees to start reconfiguring at the lowest velocities tested here ($U = 0.125$~\mps). However, it is expected that the foliated trees would demonstrate such `rigid' behaviour if lower velocities were tested during the experimental procedure.

Values for $C_d A_p$ are also calculated for the LWI branches (Fig.~\ref{fig:CdAp-vs-U-LWI}) so that the effects of scale and relative level of submergence can be discussed. For the fully submerged branches, the variation in the characteristic drag coefficient with velocity is similar to that of the foliated Hydralab trees, suggesting that the mechanisms of reconfiguration are independent of vegetation scale. However, the reduction in $C_d A_p$ with velocity for the partially submerged branches appears to follow a more linear relationship, indicating that the rate of reconfiguration for these specimens was more gradual.

\begin{figure}[htbp]
\centering
\includegraphics{CdAp-vs-U-LWI}
\caption{Variation in characteristic drag coefficient with velocity for the: (a) partially; and (b) fully submerged LWI branches.}
\label{fig:CdAp-vs-U-LWI}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Vogel Exponent Values: Hydralab Trees}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Although the Vogel exponent $\psi$ was originally formulated to investigate regions of high deformation \citep{Vogel-84}, it has since been determined from the full force-velocity range in many studies involving flexible vegetation \citep{Aberle-12, Jalonen-13a, Vastila-13}. This is mainly due to the subjectivity of specifying lower or upper cut-off velocities. In this study, therefore, Vogel exponents are calculated from a power-law regression analysis using the full force-velocity range.

The resulting Vogel exponents and respective velocity ranges for the Hydralab trees are presented in Tables~\ref{tab:vogel-exp-alnus}--\ref{tab:vogel-exp-salix}, with the species-averaged values given in Table~\ref{tab:vogel-exp-stats}. The coefficient of determination $R^2$ was greater than 0.99 for the foliated trees, while for the defoliated trees $R^2 > 0.97$, thus verifying the power law as a suitable approximation.
\nomenclature[rr]{$R^2$}{Coefficient of determination}

\begin{table}[htbp]
\caption{Vogel exponents obtained from a power-law regression on the full force-velocity data sets for the foliated and defoliated \alnus Hydralab trees. The velocity ranges used in the regression analyses are also given.}
\centering
\sisetup{table-format=1.3}
\begin{footnotesize}
\begin{tabular}{lSSS[table-format=-1.3]SSS[table-format=-1.3]}
\toprule
 & \multicolumn{3}{c}{Foliated} & \multicolumn{3}{c}{Defoliated} \\ \cmidrule(lr){2-4} \cmidrule(lr){5-7}
 & {$U_\textnormal{min}$} & {$U_\textnormal{max}$} & {$\psi$} & {$U_\textnormal{min}$} & {$U_\textnormal{max}$} & {$\psi$} \\
Specimen & {(\mps)} & {(\mps)} & {(-)} & {(\mps)} & {(\mps)} & {(-)} \\ \midrule
A1 & 0.125 & 1.25 & -0.677 & 0.125 & 2.5 & -0.774 \\
A2 & 0.125 & 1.75 & -0.814 & {-} & {-} & {-} \\
A3 & 0.125 & 1 & -0.758 & 0.125 & 1 & -0.501 \\
A4 & 0.125 & 1.75 & -0.777 & 0.125 & 1.75 & -0.563 \\
A5 & 0.125 & 1.75 & -0.614 & 0.125 & 1.75 & -0.456 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:vogel-exp-alnus}
\end{table}

\begin{table}[htbp]
\caption{Vogel exponents obtained from a power-law regression on the full force-velocity data sets for the foliated and defoliated \populus Hydralab trees. The velocity ranges used in the regression analyses are also given.}
\centering
\sisetup{table-format=1.3}
\begin{footnotesize}
\begin{tabular}{lSSS[table-format=-1.3]SSS[table-format=-1.3]}
\toprule
 & \multicolumn{3}{c}{Foliated} & \multicolumn{3}{c}{Defoliated} \\ \cmidrule(lr){2-4} \cmidrule(lr){5-7}
 & {$U_\textnormal{min}$} & {$U_\textnormal{max}$} & {$\psi$} & {$U_\textnormal{min}$} & {$U_\textnormal{max}$} & {$\psi$} \\
Specimen & {(\mps)} & {(\mps)} & {(-)} & {(\mps)} & {(\mps)} & {(-)} \\ \midrule
P1 & {-} & {-} & {-} & 0.125 & 2 & -1.002 \\
P2 & 0.125 & 1.5 & -0.826 & {-} & {-} & {-} \\
P2B1 & 0.125 & 1.5 & -0.737 & 0.125 & 2 & -0.739 \\
P2B2 & 0.125 & 1.5 & -0.693 & 0.125 & 1.75 & -0.655 \\
P3 & 0.125 & 1.5 & -0.718 & 0.125 & 2 & -0.749 \\
P4 & 0.125 & 2.5 & -0.885 & {-} & {-} & {-} \\
P4B1 & 0.125 & 2.5 & -0.856 & 0.125 & 2.5 & -0.825 \\
P4B2 & 0.125 & 1.55 & -0.881 & 0.125 & 1.75 & -0.908 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:vogel-exp-populus}
\end{table}

\begin{table}[htbp]
\caption{Vogel exponents obtained from a power-law regression on the full force-velocity data sets for the foliated and defoliated \salix Hydralab trees. The velocity ranges used in the regression analyses are also given.}
\centering
\sisetup{table-format=1.3}
\begin{footnotesize}
\begin{tabular}{lSSS[table-format=-1.3]SSS[table-format=-1.3]}
\toprule
 & \multicolumn{3}{c}{Foliated} & \multicolumn{3}{c}{Defoliated} \\ \cmidrule(lr){2-4} \cmidrule(lr){5-7}
 & {$U_\textnormal{min}$} & {$U_\textnormal{max}$} & {$\psi$} & {$U_\textnormal{min}$} & {$U_\textnormal{max}$} & {$\psi$} \\
Specimen & {(\mps)} & {(\mps)} & {(-)} & {(\mps)} & {(\mps)} & {(-)} \\ \midrule
S1 & 0.125 & 2.5 & -0.8 & 0.25 & 3.5 & -0.866 \\
S2 & 0.125 & 2 & -0.796 & 0.125 & 3 & -0.857 \\
S3 & 0.125 & 0.75 & -0.509 & 0.125 & 2.25 & -0.835 \\
S4 & 0.125 & 1.75 & -0.799 & 0.125 & 2.5 & -0.78 \\
S5 & 0.125 & 1 & -0.97 & {-} & {-} & {-} \\
S5B1 & 0.125 & 1 & -0.869 & 0.125 & 3 & -0.892 \\
S5B2 & 0.125 & 1 & -0.92 & 0.125 & 2.25 & -0.894 \\
S6 & 0.25 & 1.75 & -0.641 & {-} & {-} & {-} \\
S6B1 & 0.125 & 1 & -0.849 & 0.125 & 1 & -0.808 \\
S6B2 & 0.125 & 2 & -1.054 & 0.125 & 1.6 & -0.978 \\
S7 & 0.125 & 1.25 & -0.757 & {-} & {-} & {-} \\
S7B1 & 0.125 & 1.5 & -0.771 & 0.25 & 2 & -0.825 \\
S7B2 & 0.125 & 1.5 & -0.857 & 0.31 & 1.25 & -0.76 \\
S7B3 & 0.125 & 1.5 & -0.705 & 0.125 & 2 & -0.64 \\
S8 & 0.125 & 3.5 & -0.837 & 0.125 & 3.5 & -0.895 \\
S9 & 0.125 & 4 & -0.707 & 0.125 & 3 & -0.891 \\
S10 & 0.125 & 1.25 & -0.787 & 0.125 & 2 & -0.862 \\
S11 & 0.125 & 2 & -0.94 & 0.125 & 6 & -0.777 \\
S12 & 0.125 & 2 & -0.797 & 0.125 & 6 & -0.843 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:vogel-exp-salix}
\end{table}

\begin{table}[htbp]
\caption{Species-averaged Vogel exponents obtained from a power-law regression on the full force-velocity data sets for the foliated and defoliated Hydralab trees. The standard deviations around the averaged values are also given.}
\centering
\sisetup{table-format=-1.3}
\begin{footnotesize}
\begin{tabular}{lSS[table-format=1.3]SS[table-format=1.3]}
\toprule
 & \multicolumn{2}{c}{Foliated} & \multicolumn{2}{c}{Defoliated} \\ \cmidrule(lr){2-3} \cmidrule(lr){4-5}
{Species} & {$\psi$} & {$\sigma$} & {$\psi$} & {$\sigma$} \\ \midrule
\alnus & -0.728 & 0.081 & -0.573 & 0.141 \\
\populus & -0.799 & 0.081 & -0.813 & 0.126 \\
\salix & -0.809 & 0.122 & -0.838 & 0.076 \\
All specimens & -0.794 & 0.11 & -0.791 & 0.135 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:vogel-exp-stats}
\end{table}

Before discussing the Vogel exponent, it is noted that the terms `greater' and `lesser' in this context refer to the magnitude of the exponent, rather than its absolute value, since a more negative exponent implies a greater degree of reconfiguration. For instance, an exponent of $\psi = -0.75$ is said to be greater than an exponent of $\psi = -0.50$. The Vogel exponents and the corresponding species-averaged values for the Hydralab trees are also illustrated in Fig.~\ref{fig:vogel-exponents}.

\begin{figure}[htbp]
\centering
\includegraphics{vogel-exponents}
\caption{Individual and species-averaged Vogel exponents obtained from a power-law regression on the full force-velocity data sets for the \alnus (A), \populus (P) and \salix (S) Hydralab trees.}
\label{fig:vogel-exponents}
\end{figure}

From Tables~\ref{tab:vogel-exp-alnus}--\ref{tab:vogel-exp-salix}, it can be seen that the Vogel exponents obtained here are roughly similar in magnitude, with the minimum and maximum values being $-0.46$ and $-1.05$, respectively. It might be expected that the Vogel exponents for the foliated trees would be greater than those for the defoliated trees (\ie $\psi_\textnormal{fol}$ more negative than $\psi_\textnormal{defol}$) since the addition of foliage significantly increases the total drag force, and thus reconfiguration, for a given velocity (Fig.~\ref{fig:foliage-drag}). Indeed, this is found to be the case for the majority of the alder trees (Table~\ref{tab:vogel-exp-alnus}). However, for the alder specimen A1 and the majority of the poplar and willow trees, the opposite is found to be true ($\psi_\textnormal{fol}$ is less negative than $\psi_\textnormal{defol}$).

The reason for this behaviour is that for the majority of the alder trees, the maximum towing velocities were equal for the foliated and defoliated states (\ie $U_\textnormal{max,fol} = U_\textnormal{max,defol}$). On the other hand, the alder specimen A1 and the majority of the poplar and willow trees were tested to higher velocities when in a defoliated state. Therefore, the regression analyses for those specimens included more points at higher velocities, where $\psi \approx -1$, than the foliated analyses and the resulting Vogel exponents are correspondingly more negative.

Considering the species individually, it can be seen from Table~\ref{tab:vogel-exp-stats} and Fig.~\ref{fig:vogel-exponents} that the alder trees tend to have a Vogel exponent that is less negative than the other two species. This behaviour is especially evident when the alder trees are in a defoliated state, with the exponents of the trees A3, A4 and A5 accounting for three of the least negative exponents overall. The results thus suggest that the alder trees are less flexible than the poplar and willow specimens currently tested. However, the alder specimens actually had the lowest (\ie most flexible) average $E_{25}$ and $E_{50}$ values (Table~\ref{tab:hydralab-stiffness}).

This may seem contradictory, but, as previously discussed, is most likely a result of the generally lower $U_\textnormal{max}$ values obtained for the alder trees compared to the poplar and willow trees (Tables~\ref{tab:vogel-exp-alnus}--\ref{tab:vogel-exp-salix}). In addition, the variation could be due to branch morphology and the distribution of flexural rigidity over the entire tree. For example, while the alder specimens had more flexible main stems ($EI_{25}$ and $EI_{50}$ values), their outer branches may have been thicker and thus less flexible than those of the poplar and willow specimens. The mode of reconfiguration is also likely to differ between species, therefore further altering the Vogel exponent.

Regardless of these issues, the Vogel values calculated here are consistent with those reported in the literature. For example, \citet{Vastila-13} tested arrays of foliated \populus branches in a tilting flume and evaluated an average Vogel exponent of $-1.03$ using the full velocity range (0.03~\mps to 0.61~\mps). This is close to the value of $\psi = -1.0$ for the foliated poplar specimen P1, although the average exponent for the foliated poplar trees in the present study is lower at $-0.799$ (Table~\ref{tab:vogel-exp-stats}). The difference may be due to the fact that only one tree at a time was tested in the current study, as opposed to the arrays of branches utilized in the aforementioned study. Further, the difference in scale (trees versus small branches) and range of testing velocities between the two studies could be responsible for some variation.

\citet{Vastila-13} summarized additional exponent values from similar experiments \citep{Jarvela-04a, Jarvela-06b, Schoneboom-10} involving \textit{Salix caprea} ($\psi = -0.57$), \textit{Salix triandra x viminalis} ($\psi = -0.90$), and artificial branches ($\psi = -0.74$). These values are in good agreement with those presented here for the \salix trees.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Vogel Exponent Values: LWI Branches}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The Vogel exponents from power-law regression analyses of the full force-velocity ranges for the LWI branches are summarized in Table~\ref{tab:vogel-exp-LWI}, along with the upper and lower velocities. The coefficient of determination $R^2$ was greater than 0.98 for all specimens.

\begin{table}[htbp]
\caption{Vogel exponents obtained from a power-law regression on the LWI data sets for the partially and fully submerged branches. Both the natural (N) and artificial (A) plants are included. The velocity ranges used in the regression analyses are also given.}
\centering
\sisetup{table-format=1.3}
\begin{footnotesize}
\begin{tabular}{lSSS[table-format=-1.3]SSS[table-format=-1.3]}
\toprule
 & \multicolumn{3}{c}{Partially sub.} & \multicolumn{3}{c}{Fully sub.} \\ \cmidrule(lr){2-4} \cmidrule(lr){5-7}
 & {$U_\textnormal{min}$} & {$U_\textnormal{max}$} & {$\psi$} & {$U_\textnormal{min}$} & {$U_\textnormal{max}$} & {$\psi$} \\
{Specimen} & {(\mps)} & {(\mps)} & {(-)} & {(\mps)} & {(\mps)} & {(-)} \\ \midrule
Willow (N) & 0.218 & 0.888 & -1 & 0.198 & 0.957 & -0.677 \\
Poplar (N) & 0.218 & 0.888 & -0.467 & 0.198 & 0.957 & -0.679 \\
Poplar (A) & 0.218 & 0.888 & -0.51 & 0.198 & 0.957 & -0.883 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:vogel-exp-LWI}
\end{table}

It can be seen that, apart from the natural willow branch, the Vogel exponent is greater (\ie more negative) when the branches are fully submerged. This is consistent with the idea that the Vogel exponent is a measure of the rate of reconfiguration; a specimen will deform more for a given velocity when fully submerged than compared to when it is partially submerged, since the bending moment acting on the main stem will be greater due to the increased drag force and lever arm length. The reason that the Vogel exponent for the natural willow branch is greater when partially submerged is perhaps due to experimental error. For example, there is a slight bump in the fully submerged drag force for this specimen at $U \approx 0.8$~\mps (see Fig.~\ref{fig:LWI-F-vs-U-h25}). The small number of specimens tested at the LWI facilities, however, makes it difficult to determine whether this exception is an error or an unknown physical phenomenon. 

Examining the Vogel exponent values for the fully submerged natural poplar and willow LWI branches (Table~\ref{tab:vogel-exp-LWI}), it can be seen that there is general agreement with the values determined for the foliated poplar and willow trees from the Hydralab study (Tables~\ref{tab:vogel-exp-populus} and \ref{tab:vogel-exp-salix}, respectively). The Vogel exponent for the LWI poplar is closer to the Hydralab poplar species-average value in Table~\ref{tab:vogel-exp-stats} than the value for the LWI willow is to the Hydralab willow species-average. However, as previously discussed, the Vogel exponent for the fully submerged natural willow from the LWI experiments may have been affected by experimental error.

It is also interesting to note that the Vogel exponents determined here for the LWI willow and poplar branches differ slightly from those reported in \citet{Dittrich-12}. This is due to the authors using a linear least squares regression analysis after transforming the force-velocity data onto log-log space, whereas in this study a non-linear least squares regression analysis based on the Levenberg-Marquardt algorithm is utilized. The method used here is marginally more accurate, with $R^2 \ge 0.986$ compared to $R^2 \approx 0.98$ given in \citet{Dittrich-12}. This highlights the need for consistent and robust regression methodologies across studies when dealing with power laws \citep{Clauset-09}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Physical Property Correlation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In order to investigate whether the Vogel exponent may be predicted \textit{a priori}, linear regression analyses were performed with respect to the Hydralab trees' physical properties, including: height $H$; diameter of the main stem at each quartile height (\ie basal $d_0$, first-quartile $d_{25}$, mid-stem $d_{50}$, and third-quartile $d_{75}$); total wet $M_w$ and dry $M_d$ masses; wood-only wet $M_{S,w}$ and dry $M_{S,d}$ masses; total $V$ and wood-only $V_S$ volumes; main-stem modulus of elasticity $E$; and main-stem flexural rigidity $EI$.

The resulting coefficients of determination are presented in Table~\ref{tab:vogel-exp-correlation} for all of the Hydralab trees combined and in Table~\ref{tab:vogel-exp-correlation-species} for the individual species. Physical property correlations are not determined for the LWI branches as there are an insufficient number of specimens.

\begin{table}[htbp]
\caption{Coefficients of determination obtained from linear regression analyses of the variation in Vogel exponent with the Hydralab trees' physical properties. The number of data points $N$ used in each regression analysis is also given.}
\centering
\sisetup{table-format=1.2}
\begin{footnotesize}
\begin{tabular}{lSS[table-format=2]SS[table-format=2]}
\toprule
 & \multicolumn{2}{c}{Foliated} & \multicolumn{2}{c}{Defoliated} \\ \cmidrule(lr){2-3} \cmidrule(lr){4-5}
{Property} & {$R^2$} & {$N$} & {$R^2$} & {$N$} \\ \midrule
$H$ (m) & 0.01 & 31 & 0.11 & 26 \\
$d_0$ (mm) & 0 & 29 & 0.01 & 24 \\
$d_{25}$ (mm) & 0.21 & 30 & 0.01 & 25 \\
$d_{50}$ (mm) & 0.14 & 31 & 0.02 & 26 \\
$d_{75}$ (mm) & 0.01 & 30 & 0 & 25 \\
$M_w$ (g) & 0.11 & 30 & 0.02 & 24 \\
$M_d$ (g) & 0.03 & 19 & 0 & 14 \\
$V$ (cm\sps{3}) & 0.07 & 24 & 0.08 & 20 \\
$M_{S,w}$ (g) & 0.14 & 30 & 0.01 & 25 \\
$M_{S,d}$ (g) & 0 & 25 & 0.01 & 20 \\
$V_S$ (cm\sps{3}) & 0.09 & 27 & 0.01 & 23 \\
$E_{25}$ (\stress) & 0.17 & 21 & 0.05 & 19 \\
$E_{50}$ (\stress) & 0.02 & 21 & 0 & 19 \\
$EI_{25}$ (\bstiff) & 0 & 24 & 0.07 & 22 \\
$EI_{50}$ (\bstiff) & 0.09 & 24 & 0.01 & 22 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:vogel-exp-correlation}
\end{table}

\begin{table}[htbp]
\caption{Coefficients of determination obtained from linear regression analyses of the variation in Vogel exponent with the Hydralab trees' physical properties. The number of data points $N$ used in each regression analysis is also given.}
\centering
\sisetup{table-format=1.2}
\begin{footnotesize}
\begin{tabular}{lSS[table-format=1]SS[table-format=1]SS[table-format=1]SS[table-format=1]SS[table-format=2]SS[table-format=2]}
\toprule
 & \multicolumn{4}{c}{\alnus} & \multicolumn{4}{c}{\populus} & \multicolumn{4}{c}{\salix} \\ \cmidrule(lr){2-5} \cmidrule(lr){6-9} \cmidrule(lr){10-13}
Property & {$R^2_\textnormal{fol}$} & {$N$} & {$R^2_\textnormal{defol}$} & {$N$} & {$R^2_\textnormal{fol}$} & {$N$} & {$R^2_\textnormal{defol}$} & {$N$} & {$R^2_\textnormal{fol}$} & {$N$} & {$R^2_\textnormal{defol}$} & {$N$} \\ \midrule
$H$ (m) & 0.69 & 5 & 0.2 & 4 & 0 & 7 & 0.11 & 6 & 0 & 19 & 0.13 & 16 \\
$d_0$ (mm) & 0.05 & 5 & 0.88 & 4 & 0.04 & 6 & 0 & 5 & 0.01 & 18 & 0.06 & 15 \\
$d_{25}$ (mm) & 0.18 & 5 & 0.99 & 4 & 0.01 & 6 & 0.6 & 5 & 0.33 & 19 & 0 & 16 \\
$d_{50}$ (mm) & 0.01 & 5 & 0.96 & 4 & 0.08 & 7 & 0.74 & 6 & 0.2 & 19 & 0 & 16 \\
$d_{75}$ (mm) & 0.05 & 5 & 0.63 & 4 & 0.18 & 6 & 0.07 & 5 & 0.03 & 19 & 0 & 16 \\
$M_w$ (g) & 0.26 & 4 & 0.88 & 3 & 0.23 & 7 & 0 & 5 & 0.28 & 19 & 0.03 & 16 \\
$M_d$ (g) & {-} & 2 & {-} & 1 & 0.21 & 7 & 0 & 5 & 0.35 & 10 & 0.01 & 8 \\
$V$ (cm\sps{3}) & 0.41 & 4 & 0.65 & 3 & 0.96 & 3 & 0.94 & 3 & 0.24 & 17 & 0.04 & 14 \\
$M_{S,w}$ (g) & 0.24 & 4 & 0.85 & 3 & 0.21 & 7 & 0 & 6 & 0.3 & 19 & 0.02 & 16 \\
$M_{S,d}$ (g) & {-} & 2 & {-} & 1 & 0.18 & 7 & 0 & 6 & 0.03 & 16 & 0.07 & 13 \\
$V_S$ (cm\sps{3}) & 0.38 & 4 & 0.23 & 3 & 0.28 & 5 & 0.07 & 5 & 0.26 & 18 & 0.01 & 15 \\
$E_{25}$ (\stress) & 0.72 & 5 & 0.21 & 4 & 0.45 & 4 & 0.27 & 3 & 0.04 & 12 & 0 & 12 \\
$E_{50}$ (\stress) & 0.72 & 5 & 0.23 & 4 & 0.6 & 4 & 0.94 & 3 & 0.03 & 12 & 0 & 12 \\
$EI_{25}$ (\bstiff) & 0.48 & 5 & 0.14 & 4 & 0.51 & 6 & 0.53 & 5 & 0.5 & 13 & 0 & 13 \\
$EI_{50}$ (\bstiff) & 0.65 & 5 & 0.14 & 4 & 0.51 & 6 & 0.04 & 5 & 0.65 & 13 & 0 & 13 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:vogel-exp-correlation-species}
\end{table}

It can be seen that there is essentially no correlation between the Hydralab trees' physical properties and the Vogel exponent when the species are combined (Table~\ref{tab:vogel-exp-correlation}). When the species are taken separately (Table~\ref{tab:vogel-exp-correlation-species}), there appear to be possible correlations for the alder and poplar trees. However, on closer inspection, the higher $R^2$ values can be attributed to the small sample sizes ($N < 5$).

The lack of correlation between the Vogel exponent and the trees' physical properties is somewhat unexpected, but is possibly related to the difficulty in classifying the reconfiguration of complex objects such as full-scale trees. For example, the differing flexural rigidities of the stem, branches and leaves are not recorded in the current study. Any relationship between the Vogel exponent and a tree's physical properties may also not be linear, as assumed here.

Therefore, it is recommended that the species-averaged Vogel exponents are used to classify the trees. This is supported by the lower standard of deviations seen in Table~\ref{tab:vogel-exp-stats} for the species-averaged values as compared to the overall-averaged values. The use of species-specific Vogel exponents is also common in previous studies (\eg \citealp{Jarvela-04a, Aberle-12}) and provides a more practical metric for any reconfiguration models.
