#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

# Filenames and variables
FOL_DATA = "../data/Ap-Cd-fol.csv"
DEFOL_DATA = "../data/Ap-Cd-defol.csv"

# CSV input
set datafile separator comma

# Plot options
set terminal pdfcairo monochrome enhanced dashed size 3.6,2.5 font "NimbusRomNo9L-Regu,22" fontscale 0.4
set xlabel "{/NimbusRomNo9L-ReguItal U} (m s^{-1})" offset 0,0.6
set ylabel "{/NimbusRomNo9L-ReguItal C_{d}} (-)" offset 1.25,0
set xrange [0:3]
set yrange [0:1.5]
set xtics 1 scale 0.5 font ",18"
set ytics 0.3 scale 0.5 font ",18"
set key Left reverse box lw 0.5 height 0.3 width 1.5 samplen 3 font ",18"
set bmargin 2.75

set style data linespoints
set style increment user
set style line 1 lt 2 pt 1
set style line 2 lt 3 pt 2
set style line 3 lt 4 pt 3
set style line 4 lt 5 pt 4
set style line 5 lt 2 pt 6
set style line 6 lt 3 pt 8
set pointsize 0.6


##################################################################################################################
# PLOT FOL
##################################################################################################################

set output "../images/Cd-vs-U-fol.pdf"
set key maxrows 3

plot for [idx in "A1 A3 A5 S1 S4 S6"] FOL_DATA index idx u 1:4 t idx


##################################################################################################################
# PLOT DEFOL
##################################################################################################################

set output "../images/Cd-vs-U-defol.pdf"
set key maxrows 4

plot for [idx in "A1 P1 S1 S2"] DEFOL_DATA index idx u 1:4 t idx


##################################################################################################################
# PLOT FOL & DEFOL
##################################################################################################################

set output "../images/Cd-vs-U-fol-defol.pdf"
set key width -0.5

plot	FOL_DATA index "A1" u 1:4 lt 1 pt 1 t "A1 fol.", \
		DEFOL_DATA index "A1" u 1:4 lt 1 pt 2 t "A1 defol.", \
		FOL_DATA index "S1" u 1:4 lt 2 pt 4 t "S1 fol.", \
		DEFOL_DATA index "S1" u 1:4 lt 2 pt 6 t "S1 defol."
