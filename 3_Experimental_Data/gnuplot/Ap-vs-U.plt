#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

# Filenames and variables
FOL_DATA = "../data/Ap-Cd-fol.csv"
DEFOL_DATA = "../data/Ap-Cd-defol.csv"

# CSV input and macro
set datafile separator comma
set macro

# Plot options
set terminal pdfcairo monochrome enhanced dashed size 5.2,2.5 font "NimbusRomNo9L-Regu,22" fontscale 0.4
set xlabel "{/NimbusRomNo9L-ReguItal U} (m s^{-1})" offset 0,0.5
set xrange [0:3]
set xtics 1 scale 0.5 font ",18"
set ytics scale 0.5 font ",18"
set bmargin 3
set rmargin 2

set style data linespoints
set style increment user
set style line 1 lt 2 pt 1
set style line 2 lt 3 pt 2
set style line 3 lt 4 pt 4
set style line 4 lt 5 pt 6
set style line 5 lt 2 pt 8
set style line 6 lt 3 pt 10
set pointsize 0.6

SETTINGS_PERCENTAGE = 'set label 1 "(a)" at graph 0.09,0.91 center font ",18"; \
	set ylabel "{/NimbusRomNo9L-ReguItal A_{p} / A_{p0}} (%)" offset 0.75,0; \
	set yrange [0:125]; \
	set ytics 25; \
	set key bottom left Left reverse box lw 0.5 height 0.3 width 1.5 samplen 3 maxrows 3 font ",18"'

SETTINGS_ABSOLUTE = 'set label 1 "(b)"; \
	set ylabel "{/NimbusRomNo9L-ReguItal A_{p}} (m^2)" offset 0.75,0; \
	set yrange [0:1]; \
	set ytics auto; \
	unset key'


##################################################################################################################
# PLOT FOL
##################################################################################################################

set output "../images/Ap-vs-U-fol.pdf"
set multiplot layout 1,2

@SETTINGS_PERCENTAGE
plot for [idx in "A1 A3 A5 S1 S4 S6"] FOL_DATA index idx u 1:2 t idx

@SETTINGS_ABSOLUTE
plot for [idx in "A1 A3 A5 S1 S4 S6"] FOL_DATA index idx u 1:3

unset multiplot


##################################################################################################################
# PLOT DEFOL
##################################################################################################################

set output "../images/Ap-vs-U-defol.pdf"
set multiplot layout 1,2

@SETTINGS_PERCENTAGE
plot for [idx in "A1 P1 S1 S2"] DEFOL_DATA index idx u 1:2 t idx

@SETTINGS_ABSOLUTE
plot for [idx in "A1 P1 S1 S2"] DEFOL_DATA index idx u 1:3

unset multiplot


##################################################################################################################
# PLOT FOL & DEFOL
##################################################################################################################

set output "../images/Ap-vs-U-fol-defol.pdf"
set multiplot layout 1,2

@SETTINGS_PERCENTAGE
set key top right maxrows 4 width -0.25

plot	FOL_DATA index "A1" u 1:2 lt 1 pt 1 t "A1 fol.", \
		DEFOL_DATA index "A1" u 1:2 lt 1 pt 2 t "A1 defol.", \
		FOL_DATA index "S1" u 1:2 lt 2 pt 4 t "S1 fol.", \
		DEFOL_DATA index "S1" u 1:2 lt 2 pt 6 t "S1 defol."

@SETTINGS_ABSOLUTE
plot	FOL_DATA index "A1" u 1:3 lt 1 pt 1, \
		DEFOL_DATA index "A1" u 1:3 lt 1 pt 2, \
		FOL_DATA index "S1" u 1:3 lt 2 pt 4, \
		DEFOL_DATA index "S1" u 1:3 lt 2 pt 6
