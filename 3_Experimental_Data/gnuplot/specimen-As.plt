#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

# Datafiles
DATA = "../data/specimen-As.csv"

# CSV input
set datafile separator comma

# Plot options
set terminal pdfcairo monochrome enhanced dashed size 4.5,2.5 font "NimbusRomNo9L-Regu,22" fontscale 0.4
set ylabel "{/NimbusRomNo9L-ReguItal A_{S}} (m^2)" offset 1,0
set xrange [0:15]
set yrange [0:*]
set xtics scale 0.5 offset 0,0.5 font ",18"
set ytics 0.1 scale 0.5
unset key
set boxwidth -2
set bmargin 2.5

set xtics ( "{/NimbusRomNo9L-ReguItal q}_{25}" 1, "{/NimbusRomNo9L-ReguItal q}_{50}" 2, "{/NimbusRomNo9L-ReguItal q}_{75}" 3, "{/NimbusRomNo9L-ReguItal q}_{100}" 4, \
			"{/NimbusRomNo9L-ReguItal q}_{25}" 6, "{/NimbusRomNo9L-ReguItal q}_{50}" 7, "{/NimbusRomNo9L-ReguItal q}_{75}" 8, "{/NimbusRomNo9L-ReguItal q}_{100}" 9, \
			"{/NimbusRomNo9L-ReguItal q}_{25}" 11, "{/NimbusRomNo9L-ReguItal q}_{50}" 12, "{/NimbusRomNo9L-ReguItal q}_{75}" 13, "{/NimbusRomNo9L-ReguItal q}_{100}" 14)

# Functions
xspacing(x) = (x<4 ? x+1 : (x<8 ? x+2 : x+3))


##################################################################################################################
# PLOT
##################################################################################################################

set output "../images/specimen-As.pdf"

set label 1 "A1" at 2.25,screen 0.05 font ",18"
set label 2 "P1" at 7.25,screen 0.05 font ",18"
set label 3 "S1" at 12.25,screen 0.05 font ",18"

plot DATA u (xspacing($0)):6:4:5:(0.75) w boxerrorbars

