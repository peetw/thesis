#!/usr/bin/env gnuplot

######################################################################################################################################
# SETTINGS
######################################################################################################################################

# Datafiles
DATA = "../data/F-vs-U.csv"

# CSV input and macros
set datafile separator comma
set macro

# Plot options
set terminal pdfcairo monochrome enhanced dashed size 5,7.5 font "NimbusRomNo9L-Regu,22" fontscale 0.4
set linetype 1 pt 1
set linetype 2 pt 2
set linetype 3 pt 3
set linetype 4 pt 4
set linetype 5 pt 6
set linetype 6 pt 8
set linetype 7 pt 10
set linetype 8 pt 12

RESET = 'set xlabel "{/NimbusRomNo9L-ReguItal U} (m s^{-1})"; \
	set ylabel "{/NimbusRomNo9L-ReguItal F} (N)"; \
	set xtics scale 0.5; \
	set ytics scale 0.5; \
	set border; \
	set multiplot; \
	unset key'

SET_FOL = 'set origin 0,0.45; \
	set size 1,0.45; \
	set label 1 "(a)" at screen 0.16,0.85'

SET_DEFOL = 'set origin 0,0; \
	set label 1 "(b)" at screen 0.16,0.4'

SET_KEY = 'set origin 0,0.9; \
	set key horizontal Left reverse box lw 0.5 height 0.75 width 1 font ",18"; \
	unset xlabel; \
	unset ylabel; \
	unset xtics; \
	unset ytics; \
	unset border; \
	unset label 1'


######################################################################################################################################
# ALNUS
######################################################################################################################################

set output "../images/F-vs-U-alnus.pdf"
set xrange[0:3]
set yrange[0:200]
set xtics 0.5
set ytics 50
@RESET

# Foliated
@SET_FOL
plot	for [i=1:21:5] DATA u i:i+1 w linesp

# Defoliated
@SET_DEFOL
style(n) = (n+4)/5

plot	for [i=1:1] DATA u i:i+2 w linesp lt style(i), \
		for [i=11:21:5] DATA u i:i+2 w linesp lt style(i)

# Key
@SET_KEY
set size 0.925,0.1
plot 	for [i=1:21:5] DATA u i:(NaN) w linesp t columnhead(i)

unset multiplot


######################################################################################################################################
# POPULUS
######################################################################################################################################

set output "../images/F-vs-U-populus.pdf"
set xrange[0:3]
set yrange[0:250]
set xtics 0.5
set ytics 50
@RESET

# Foliated
@SET_FOL
style(n) = (n-21)/5

plot	for [i=31:61:5] DATA u i:i+1 w linesp lt style(i)


# Defoliated
@SET_DEFOL
style(n) = (n-21)/5

plot	for [i=26:26:5] DATA u i:i+2 w linesp lt style(i), \
		for [i=36:46:5] DATA u i:i+2 w linesp lt style(i), \
		for [i=56:61:5] DATA u i:i+2 w linesp lt style(i)

# Key
@SET_KEY
set size 0.91,0.1
plot	for [i=26:61:5] DATA u i:(NaN) w linesp t columnhead(i)

unset multiplot


######################################################################################################################################
# SALIX
######################################################################################################################################

set output "../images/F-vs-U-salix.pdf"
set xrange[0:7]
set yrange[0:400]
set xtics 1
set ytics 100
@RESET

# Foliated
@SET_FOL
lines(n) = (n < 9 ? 1 : (n < 17 ? 3 : 0))
points(n) = (n < 5 ? n : (n < 9 ? n*2-4 : (n < 13 ? n-8 : (n < 17 ? (n-8)*2-4 : n-16))))

plot	for [i=66:156:5] DATA u i:i+1 w linesp lt lines((i-61)/5) pt points((i-61)/5)


# Defoliated
@SET_DEFOL
lines(n) = (n < 7 ? 1 : (n < 14 ? 3 : 0))
points(n) = (n < 5 ? n : (n < 7 ? (n-1)*2 : (n < 8 ? 1 : (n < 14 ? (n-7)*2 : n-13))))

plot	for [i=66:81:5] DATA u i:i+2 w linesp lt lines((i-61)/5) pt points((i-61)/5), \
		for [i=91:96:5] DATA u i:i+2 w linesp lt lines((i-66)/5) pt points((i-66)/5), \
		for [i=106:111:5] DATA u i:i+2 w linesp lt lines((i-71)/5) pt points((i-71)/5), \
		for [i=121:156:5] DATA u i:i+2 w linesp lt lines((i-76)/5) pt points((i-76)/5)

# Key
@SET_KEY
set origin 0,0.92
set size 1,0.1
lines(n) = (n < 9 ? 1 : (n < 17 ? 3 : 0))
points(n) = (n < 5 ? n : (n < 9 ? n*2-4 : (n < 13 ? n-8 : (n < 17 ? (n-8)*2-4 : n-16))))

plot	for [i=66:156:5] DATA u i:(NaN) w linesp lt lines((i-61)/5) pt points((i-61)/5) t columnhead(i)

unset multiplot
