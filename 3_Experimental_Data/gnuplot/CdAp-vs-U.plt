#!/usr/bin/env gnuplot

######################################################################################################################################
# SETTINGS
######################################################################################################################################

# Filenames and variables
DATA = "../data/F-vs-U.csv"
LWI_DATA = "../data/LWI-F-vs-U.csv"

# CSV input and macro
set datafile separator comma
set macro

# Plot options
set terminal pdfcairo monochrome enhanced dashed size 5.2,7 font "NimbusRomNo9L-Regu,22" fontscale 0.4
set xlabel "{/NimbusRomNo9L-ReguItal U} (m s^{-1})" offset 0,0.75
set xtics scale 0.5 font ",18"
set ytics scale 0.5 font ",18"
set label 1 at graph 0.05,0.91
set bmargin 2.5

set style data linespoints
set linetype 1 pt 1
set linetype 2 pt 2
set linetype 3 pt 3
set linetype 4 pt 4
set linetype 5 pt 6
set linetype 6 pt 8
set linetype 7 pt 10
set linetype 8 pt 12
set pointsize 0.8

set output "../images/CdAp-vs-U.pdf"
set multiplot layout 3,2

SET_FOL = 'set ylabel "{/NimbusRomNo9L-ReguItal C_{d} A_{p}} (m^{2})"; \
	set ytics format "%g"; \
	set lmargin at screen 0.115; \
	set rmargin at screen 0.525; \
	unset key'

SET_DEFOL = 'unset ylabel; \
	set ytics format ""; \
	set lmargin at screen 0.55; \
	set rmargin at screen 0.96; \
	set key horizontal Left reverse box lw 0.5 height 0.75 width 1 samplen 3 maxcols 2 noautotitle font ",16"'

# Functions
RHO = 1000.0
CdAp(U,F) = 2 * F / (RHO * U**2)


######################################################################################################################################
# ALNUS
######################################################################################################################################

set xrange [-0.1:2.6]
set yrange [0:1]

# Foliated
set label 1 "(a)"
@SET_FOL

plot	for [i=1:21:5] DATA u i:(CdAp(column(i),column(i+1)))

# Defoliated
set label 1 "(b)"
@SET_DEFOL
style(n) = (n+4)/5

plot	for [i=1:1] DATA u i:(CdAp(column(i),column(i+2))) lt style(i), \
		for [i=11:21:5] DATA u i:(CdAp(column(i),column(i+2))) lt style(i), \
		for [i=1:21:5] DATA u i:(NaN) lt style(i) t columnhead(i)


######################################################################################################################################
# POPULUS
######################################################################################################################################

set xrange [-0.1:2.6]
set yrange [0:1]

# Foliated
set label 1 "(c)"
@SET_FOL
style(n) = (n-21)/5

plot	for [i=31:61:5] DATA u i:(CdAp(column(i),column(i+1))) lt style(i)

# Defoliated
set label 1 "(d)"
@SET_DEFOL

plot	for [i=26:26:5] DATA u i:(CdAp(column(i),column(i+2))) lt style(i), \
		for [i=36:46:5] DATA u i:(CdAp(column(i),column(i+2))) lt style(i), \
		for [i=56:61:5] DATA u i:(CdAp(column(i),column(i+2))) lt style(i), \
		for [i=26:61:5] DATA u i:(NaN) lt style(i) t columnhead(i)


######################################################################################################################################
# SALIX
######################################################################################################################################

set xrange [-0.2:6.2]
set yrange [0:1.5]
set ytics 0.3

# Foliated
set label 1 "(e)"
@SET_FOL
lines_fol(n) = (n < 9 ? 1 : (n < 17 ? 3 : 0))
points_fol(n) = (n < 5 ? n : (n < 9 ? n*2-4 : (n < 13 ? n-8 : (n < 17 ? (n-8)*2-4 : n-16))))

plot	for [i=66:156:5] DATA u i:(CdAp(column(i),column(i+1))) lt lines_fol((i-61)/5) pt points_fol((i-61)/5)

# Defoliated
set label 1 "(f)"
@SET_DEFOL
lines(n) = (n < 7 ? 1 : (n < 14 ? 3 : 0))
points(n) = (n < 5 ? n : (n < 7 ? (n-1)*2 : (n < 8 ? 1 : (n < 14 ? (n-7)*2 : n-13))))

plot	for [i=66:81:5] DATA u i:(CdAp(column(i),column(i+2))) lt lines((i-61)/5) pt points((i-61)/5), \
		for [i=91:96:5] DATA u i:(CdAp(column(i),column(i+2))) lt lines((i-66)/5) pt points((i-66)/5), \
		for [i=106:111:5] DATA u i:(CdAp(column(i),column(i+2))) lt lines((i-71)/5) pt points((i-71)/5), \
		for [i=121:156:5] DATA u i:(CdAp(column(i),column(i+2))) lt lines((i-76)/5) pt points((i-76)/5), \
		for [i=66:156:5] DATA u i:(NaN) lt lines_fol((i-61)/5) pt points_fol((i-61)/5) t columnhead(i)

unset multiplot


######################################################################################################################################
# LWI
######################################################################################################################################

set terminal pdfcairo size 5.2,2.5
set output "../images/CdAp-vs-U-LWI.pdf"
set multiplot layout 1,2
set xrange [-0.05:1.05]
set yrange [0:0.03]
set ytics 0.01

# Partially submerged
set label 1 "(a)"
@SET_FOL

plot	LWI_DATA u 1:(CdAp($1,$2)) lt 1 pt 1, \
		LWI_DATA u 5:(CdAp($5,$6)) lt 3 pt 4, \
		LWI_DATA u 9:(CdAp($9,$10)) lt 4 pt 6

# Fully submerged
set label 1 "(b)"
@SET_DEFOL
set key maxcols 1 width 2

plot	LWI_DATA u 3:(CdAp($3,$4)) lt 1 pt 1 t "WN", \
		LWI_DATA u 7:(CdAp($7,$8)) lt 3 pt 4 t "PN", \
		LWI_DATA u 11:(CdAp($11,$12)) lt 4 pt 6 t "PA"
