#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

# Datafiles
DATA = "../data/~vogel-exponents.csv"

# CSV input
set datafile separator comma

# Plot options
set terminal pdfcairo monochrome enhanced dashed font "NimbusRomNo9L-Regu,22" fontscale 0.4
set xlabel "Specimen" offset 0,0.5
set ylabel "{/rtxmi ψ} (-)" offset 1,0
set xrange [0:35]
set yrange [-1.2:0]
set xtics rotate by -45 scale 0.5 font ",14"
set ytics 0.2 scale 0.5 font ",18"
set key Left reverse box lw 0.5 height 0.3 width -1 samplen 3 noautotitle font ",18"
set pointsize 0.6

# Functions
mean(x,min,max,val) = (x>=min && x<=max ? val : 1/0)


######################################################################################################################################
# GET AVERAGE VALUES
######################################################################################################################################

stats DATA index 0 u 4 name "ALN_FOL" nooutput

stats DATA index 0 u 8 name "ALN_DEFOL" nooutput

stats DATA index 1 u 4 name "POP_FOL" nooutput

stats DATA index 1 u 8 name "POP_DEFOL" nooutput

stats DATA index 2 u 4 name "SAL_FOL" nooutput

stats DATA index 2 u 8 name "SAL_DEFOL" nooutput


##################################################################################################################
# PLOT
##################################################################################################################

set output "../images/vogel-exponents.pdf"

plot	DATA index 0 u ($0 + 1):4:xtic(1) w points pt 6 t "Fol. exponent", \
		DATA index 1 u ($0 + 7):4:xtic(1) w points pt 6, \
		DATA index 2 u ($0 + 16):4:xtic(1) w points pt 6, \
		mean(x,0.5,5.5,ALN_FOL_mean) w lines lt 3 lw 2 t "Fol. average", \
		DATA index 0 u ($0 + 1):8:xtic(1) w points pt 9 t "Defol. exponent", \
		DATA index 1 u ($0 + 7):8:xtic(1) w points pt 9, \
		DATA index 2 u ($0 + 16):8:xtic(1) w points pt 9, \
		mean(x,0.5,5.5,ALN_DEFOL_mean) w lines lt 2 lw 2 t "Defol. average", \
		mean(x,6.5,14.5,POP_FOL_mean) w lines lt 3 lw 2, \
		mean(x,6.5,14.5,POP_DEFOL_mean) w lines lt 2 lw 2, \
		mean(x,15.5,34.5,SAL_FOL_mean) w lines lt 3 lw 2, \
		mean(x,15.5,34.5,SAL_DEFOL_mean) w lines lt 2 lw 2

