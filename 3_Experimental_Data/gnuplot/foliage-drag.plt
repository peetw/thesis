#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

# Datafiles
DATA = "../data/F-vs-U.csv"

# CSV input
set datafile separator comma

# Plot options
set terminal pdfcairo monochrome enhanced dashed size 2.6,2.3 font "NimbusRomNo9L-Regu,22" fontscale 0.4
set xlabel "{/NimbusRomNo9L-ReguItal U} (m s^{-1})" offset 0,0.5
set ylabel "{/NimbusRomNo9L-ReguItal F}_{fol} (%)" offset 1.5,0
set xrange [0:4]
set yrange [0:100]
set xtics 1 scale 0.5 font ",18"
set ytics 20 scale 0.5 font ",18"
set key Left reverse box lw 0.5 height 0.3 width 1.5 samplen 2.5 font ",18" autotitle columnheader maxrows 4
set pointsize 0.6
set bmargin 3

set style data linespoints
set linetype 1 pt 1
set linetype 2 pt 2
set linetype 3 pt 3
set linetype 4 pt 4
set linetype 5 pt 6
set linetype 6 pt 8
set linetype 7 pt 10
set linetype 8 pt 12


##################################################################################################################
# PLOT ALNUS
##################################################################################################################

set output "../images/foliage-drag-alnus.pdf"
set label 1 "(a)" at 0.2,90 font ",18"

style(i) = (i+1) / 5

plot	for [i=4:4:5] DATA u (column(i-3)):i lt style(i), \
		for [i=14:24:5] DATA u (column(i-3)):i lt style(i)


##################################################################################################################
# PLOT POPULUS
##################################################################################################################

set output "../images/foliage-drag-populus.pdf"
set label 1 "(b)"

style(i) = (i-24) / 5

plot	for [i=39:49:5] DATA u (column(i-3)):i lt style(i), \
		for [i=59:64:5] DATA u (column(i-3)):i lt style(i)


##################################################################################################################
# PLOT SALIX
##################################################################################################################

set terminal pdfcairo size 4,3
set output "../images/foliage-drag-salix.pdf"
set label 1 "(c)" at 0.12,93

set style line 1 lt 1 pt 1
set style line 2 lt 1 pt 2
set style line 3 lt 1 pt 3
set style line 4 lt 1 pt 4
set style line 5 lt 1 pt 6
set style line 6 lt 1 pt 8
set style line 7 lt 1 pt 10
set style line 8 lt 1 pt 12
set style line 9 lt 3 pt 1
set style line 10 lt 3 pt 2
set style line 11 lt 3 pt 3
set style line 12 lt 3 pt 4
set style line 13 lt 3 pt 6
set style line 14 lt 3 pt 8
set style line 15 lt 3 pt 10
set style line 16 lt 3 pt 12
set style line 17 lt 0 pt 1
set style line 18 lt 0 pt 2
set style line 19 lt 0 pt 3

style(i) = (i-64) / 5

plot	for [i=69:84:5] DATA u (column(i-3)):i ls style(i), \
		for [i=94:99:5] DATA u (column(i-3)):i ls style(i), \
		for [i=109:114:5] DATA u (column(i-3)):i ls style(i), \
		for [i=124:159:5] DATA u (column(i-3)):i ls style(i)

