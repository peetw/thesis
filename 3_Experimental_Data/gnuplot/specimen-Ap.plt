#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

# Datafiles
DATA = "../data/specimen-Ap.csv"

# CSV input
set datafile separator comma

# Plot options
set terminal pdfcairo monochrome enhanced dashed size 5,2.7 font "NimbusRomNo9L-Regu,22" fontscale 0.4
set xlabel "Specimen" offset 0,0.5
set ylabel "{/NimbusRomNo9L-ReguItal A_p}_0 (m^2)" offset 1,0
set xrange [0:24]
set xtics rotate by -45 scale 0.5 font ",18"
set ytics 0.4 scale 0.5
set key left Left reverse box lw 0.5 height 0.25 width -1 noautotitle
set pointsize 0.6

PT_ALN = 5
PT_POP = 7
PT_SAL = 9


##################################################################################################################
# PLOT
##################################################################################################################

set output "../images/specimen-Ap.pdf"

plot	DATA index 0 u ($0+1):2:3:5:xtic(1) w yerrorbars pt PT_ALN lt 1 t "{/NimbusRomNo9L-ReguItal A. glutinosa}", \
		DATA index 1 every ::0::1 u ($0+7):($0 == 0 ? $8 : $2):($0 == 0 ? $9 : $3):($0 == 0 ? $11 : $5):xtic(1) w yerrorbars pt PT_POP lt 1 t "{/NimbusRomNo9L-ReguItal P. nigra}", \
		DATA index 1 every ::4::5 u ($0+9):2:3:5:xtic(1) w yerrorbars pt PT_POP lt 1, \
		DATA index 2 every ::0::4 u ($0+12):($0 == 1 ? $8 : $2):($0 == 1 ? $9 : $3):($0 == 1 ? $11 : $5):xtic(1) w yerrorbars pt PT_SAL lt 1 t "{/NimbusRomNo9L-ReguItal S. alba}", \
		DATA index 2 every ::7::7 u ($0+17):2:3:5:xtic(1) w yerrorbars pt PT_SAL lt 1, \
		DATA index 2 every ::10::10 u ($0+18):2:3:5:xtic(1) w yerrorbars pt PT_SAL lt 1, \
		DATA index 2 every ::14 u ($0+19):2:3:5:xtic(1) w yerrorbars pt PT_SAL lt 1
