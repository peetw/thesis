#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

# Datafiles
DATA = "../data/LWI-F-vs-U.csv"

# CSV input
set datafile separator comma

# Plot options
set terminal pdfcairo monochrome enhanced dashed size 2.6,2.3 font "NimbusRomNo9L-Regu,22" fontscale 0.4
set xlabel "{/NimbusRomNo9L-ReguItal U} (m s^{-1})" offset 0,0.5
set ylabel "{/NimbusRomNo9L-ReguItal F} (N)" offset 1.5,0
set xrange [0:1]
set yrange [0:2]
set xtics 0.25 scale 0.5 font ",18"
set ytics scale 0.5 font ",18"
set key left Left reverse box lw 0.5 height 0.3 samplen 3 font ",16"
set pointsize 0.6
set bmargin 3
set style data linespoints


##################################################################################################################
# PLOT PARTIALLY SUBMERGED
##################################################################################################################

set output "../images/LWI-F-vs-U-h12.pdf"
set label 1 "(a)" at screen 0.8,0.875 font ",18"

plot	DATA u 1:2 lt 1 pt 1 t "Willow (N)", \
		DATA u 5:6 lt 3 pt 4 t "Poplar (N)", \
		DATA u 9:10 lt 4 pt 6 t "Poplar (A)"


##################################################################################################################
# PLOT FULLY SUBMERGED
##################################################################################################################

set output "../images/LWI-F-vs-U-h25.pdf"
set label 1 "(b)"

plot	DATA u 3:4 lt 1 pt 1 t "Willow (N)", \
		DATA u 7:8 lt 3 pt 4 t "Poplar (N)", \
		DATA u 11:12 lt 4 pt 6 t "Poplar (A)"
