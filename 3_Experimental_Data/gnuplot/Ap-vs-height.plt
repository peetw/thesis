#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

# Data files
DATA = "../data/Ap-vs-height.csv"
TMP = "tmp.dat"

# Macros
set macro

# Plot options
set terminal pdfcairo monochrome enhanced dashed size 2.6,2.3 font "NimbusRomNo9L-Regu,22" fontscale 0.4
set xlabel "{/NimbusRomNo9L-ReguItal h / H} (%)" offset 0,0.5
set ylabel "{/NimbusRomNo9L-ReguItal A_p / A_p}_0 (%)" offset 0.75,0
set xrange [0:100]
set yrange [0:100]
set xtics 25 scale 0.5 font ",18"
set ytics 25 scale 0.5 font ",18"
set key bottom Left reverse box lw 0.5 height 0.3 width 0.25 samplen 2 font ",18"

lab_plot(s) = sprintf('set label 1 "(%s)" at 7,90 font ",18"', s)


##################################################################################################################
# PLOT ALNUS FIT
##################################################################################################################

set datafile separator ","
set table TMP
plot	for[i=2:12:5] DATA u 1:i notitle, \
		for[i=16:21:5] DATA u 1:i notitle
unset table
set datafile separator " "

A = 101.1		# Manually adjust
B = -12.4		# Manually adjust
C = 32.7		# Values from fit
D = 15.6		# Values from fit
f(x) = A + (B-A) / (1 + exp((x-C)/D))	# Sigmoid logistic function, adjusted for scale
fit f(x) TMP u 1:3 via C,D

SSR = FIT_WSSR

g(x) = mean
fit g(x) TMP u 1:3 via mean

SST = FIT_WSSR
R2 = 1 - SSR/SST

set output "../images/Ap-vs-height-alnus.pdf"
eval lab_plot("a")

plot	TMP u 1:3 w points pt 2 ps 0.5 notitle, \
		f(x) w lines lt 1 t "R^2 = ".sprintf("%.3f",R2)


##################################################################################################################
# PLOT POPULUS FIT
##################################################################################################################

set datafile separator ","
set table TMP
plot for[i=26:41:5] DATA u 1:i notitle
unset table
set datafile separator " "

A = 101.2		# Manually adjust
B = -6.5		# Manually adjust
C = 38.3		# Values from fit
D = 13.9		# Values from fit
f(x) = A + (B-A) / (1 + exp((x-C)/D))	# Sigmoid logistic function, adjusted for scale
fit f(x) TMP index 1::1 u 1:3 via C,D

SSR = FIT_WSSR

g(x) = mean
fit g(x) TMP index 1::1 u 1:3 via mean

SST = FIT_WSSR
R2 = 1 - SSR/SST

set output "../images/Ap-vs-height-populus.pdf"
eval lab_plot("b")

plot	TMP index 0 u 1:3 w points pt 4 ps 0.5 notitle, \
		TMP index 1::1 u 1:3 w points pt 2 ps 0.5 notitle, \
		f(x) w lines lt 1 t "R^2 = ".sprintf("%.3f",R2)


##################################################################################################################
# PLOT SALIX FIT
##################################################################################################################

set datafile separator ","
set table TMP
plot	for[i=46:101:5] DATA u 1:i notitle
unset table
set datafile separator " "

A = 100.3		# Manually adjust
B = -4.6		# Manually adjust
C = 36.2		# Values from fit
D = 11.7		# Values from fit
f(x) = A + (B-A) / (1 + exp((x-C)/D))	# Sigmoid logistic function, adjusted for scale
fit f(x) TMP u (column(-2) == 1 ? NaN : $1):3 via C,D

SSR = FIT_WSSR

g(x) = mean
fit g(x) TMP u (column(-2) == 1 ? NaN : $1):3 via mean

SST = FIT_WSSR
R2 = 1 - SSR/SST

set output "../images/Ap-vs-height-salix.pdf"
eval lab_plot("c")

plot	TMP index 0 u 1:3 w points pt 2 ps 0.5 notitle, \
		TMP index 1 u 1:3 w points pt 4 ps 0.5 notitle, \
		TMP index 2::1 u 1:3 w points pt 2 ps 0.5 notitle, \
		f(x) w lines lt 1 t "R^2 = ".sprintf("%.3f",R2)


!rm @TMP "fit.log"
