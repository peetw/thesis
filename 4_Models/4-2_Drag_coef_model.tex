%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Drag Coefficient Model}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Knowing the variation in projected area (and thus drag coefficient) with velocity is interesting from a theoretical point of view; however, it has limited scope in current modelling techniques since it is impractical to determine such relationships for every type of vegetation that may be required at each site under modelling consideration. Therefore, a more practical solution is suggested here, whereby only the projected area in still air is required. This metric can be determined on-site via non-destructive photo-analysis methods.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Theory} \label{sec:drag-coef-theory}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In previous studies involving drag force measurements for full-scale trees in wind tunnels \citep[\eg][]{Vollsinger-05, Koizumi-10} and branches in flumes \citep[\eg][]{Wunder-11, Dittrich-12, Vastila-13}, a `rigid' drag coefficient is often calculated using the projected area in still air, rather than the projected area at each velocity:
%
\begin{equation}
C^*_d = \frac{2 F}{\rho A_{p0} U^2}
\label{eq:Cd-rigid}
\end{equation}
%
where $F$ is the drag force; $\rho$ is the fluid density; $U$ is the flow velocity; and $A_{p0}$ is the projected area in still air for a particular specimen (see Table.~\ref{tab:hydralab-Ap0}). Since the projected area in still air is constant, the `rigid' drag coefficient $C^*_d$ will also incorporate any reduction in the projected area $A_p$ due to the specimen reconfiguring (see Figs.~\ref{fig:Ap-vs-U-fol} and \ref{fig:Ap-vs-U-defol}).
\nomenclature[ccdr]{$C^*_d$}{Rigid drag coefficient}
\nomenclature[ccdri]{$C^*_{d0}$}{Initial rigid drag coefficient}

When deriving empirical relationships for the drag coefficient of rigid objects, such as cylinders, it has been common to normalize the flow velocity using the dimensionless Reynolds number $Re$ (see Fig.~\ref{fig:Cd-vs-Re}). In this study, the Reynolds number is defined so that the projected area in still air is used as the characteristic length scale:
%
\begin{equation}
Re = \frac{U A^{1/2}_{p0}}{\nu}
\label{eq:Re-Ap}
\end{equation}
%
where $\nu$ is the kinematic viscosity of the fluid; $U$ is the flow velocity; and $A_{p0}$ is the projected area in still air.
\nomenclature[zzzn]{$\nu$}{Kinematic viscosity}

The use of the projected area in still air as the characteristic length scale differs from the typical Reynolds number definition for vegetation, where either the flow depth or stem diameter are utilized \citep[\eg][]{Shames-03}. However, it is proposed here since the flow depth or stem diameter provide little information on the portion of the plant or tree that the flow is acting on. Further, the use of the projected area in still air minimizes the number of model parameters as it is already required to calculate the rigid drag coefficient.

The variation in rigid drag coefficient with Reynolds number is plot in Fig.~\ref{fig:Cd-rigid-vs-Re} for the foliated and defoliated Hydralab trees.

\begin{figure}[htbp]
\centering
\includegraphics{Cd-rigid-vs-Re}
\caption{Variation in rigid drag coefficient with Reynolds number for the: (a) foliated; and (b) defoliated Hydralab trees. The projected area in still air was used as the characteristic length scale when calculating the Reynolds number.}
\label{fig:Cd-rigid-vs-Re}
\end{figure}

It can be seen that the foliated alder and poplar specimens collapse onto almost a single line for their respective species, implying that the current definition of the Reynolds number is a useful metric. However, the willow specimens are not quite as tightly grouped and there appears to be less of a singular relationship between the rigid drag coefficient and the Reynolds number.

Although there are a limited number of trees for which the defoliated projected area in still air is available, it can be seen that they mostly collapse onto a single curve (Fig.~\ref{fig:Cd-rigid-vs-Re}). However, as previously discussed (\autoref{sec:projected-area-variation}), the projected areas in still air for the specimens A1 and S1 may have been slightly overestimated from their one-sided stem areas. If the initial projected areas for these trees were reduced, the rigid drag coefficient would increase while the Reynolds number would decrease, for a given velocity. This would then move the curves for the specimens A1 and S1 closer to those for the specimens where the defoliated projected area was directly measured (P1 and S2).

For an empirical model, the curves in Fig.~\ref{fig:Cd-rigid-vs-Re} suggest that a power-law relationship between the rigid drag coefficient and Reynolds number would be appropriate. However, negative-power laws are unbounded at the lower end (\ie asymptotic with the $y$ axis), which would result in non-physical drag coefficients at low velocities. The use of an exponential law would remedy this problem, since they are bounded at the lower end, although they tend to decay much faster than power-law relationships.

A possible solution would be to combine the two relationships; however, the exponential law can be approximated with a constant value at lower Reynolds numbers, thus reducing the complexity of any empirical relationship. Therefore, the following piecewise model is proposed:
%
\begin{equation}
C^*_d =
	\begin{cases}
		C^{*}_{d0} &:\quad Re \le Re_t\\
		\alpha Re^{p} &:\quad Re > Re_t
	\end{cases}
\label{eq:Cd-rigid-Re-model}
\end{equation}
%
where $Re_t$ is a threshold Reynolds number; $C^*_{d0}$ is a constant value, equal to the initial value for $C^*_d$; $\alpha$ is a power-law coefficient; and $p$ is a power to be found through regression analysis.

The model assumes that the rigid drag coefficient does not increase dramatically at low Reynolds numbers due to viscous skin friction (see Fig.~\ref{fig:Cd-vs-Re} for $Re < 100$), since these effects would not be of practical interest when modelling floodplain vegetation where the Reynolds numbers are typically much greater.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Results and Discussion} \label{sec:drag-coef-results}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Calibration}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In order to find values for $Re_t$, $C^{*}_{d0}$, $\alpha$ and $p$ for each of the three Hydralab species, a regression analysis was applied in an iterative fashion. At each iteration the threshold Reynolds number $Re_t$ was increased, up to an arbitrary limit of $Re = \num{3e5}$, determined from visual inspection of Fig.~\ref{fig:Cd-rigid-vs-Re}. Once complete, the parameters from the regression analysis with the greatest coefficient of determination $R^2$ were selected (see Table~\ref{tab:Cd-rigid-Re-params}).

\begin{table}[htbp]
\caption{Regression parameters for the proposed model (Eq.~\ref{eq:Cd-rigid-Re-model}) describing the variation in rigid drag coefficient with Reynolds number for the Hydralab trees. The number of data points $N$ used in each regression analysis is also given.}
\centering
\sisetup{table-format=1.3}
\begin{footnotesize}
\begin{tabular}{lSS[table-format=1.1e1]S[table-format=1.2e1]S[table-format=-1.3]SS[table-format=3]}
\toprule
Species & {$C^{*}_{d0}$} & {$Re_t$} & {$\alpha$} & {$p$} & {$R^2$} & {$N$} \\ \midrule
\alnus & 0.994 & 9e4 & 1.64e3 & -0.661 & 0.923 & 39 \\
\alnus (defol.) & 0.752 & 9e4 & 9.03e2 & -0.605 & 0.981 & 10 \\
\populus & 0.756 & 9e4 & 2.94e2 & -0.54 & 0.916 & 28 \\
\populus (defol.) & 0.984 & 1.6e5 & 1.61e5 & -0.989 & 0.982 & 8 \\
\salix & 0.891 & 1e5 & 2e3 & -0.671 & 0.819 & 101 \\
\salix (defol.) & 1.255 & 8e4 & 2.19e3 & -0.66 & 0.926 & 23 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:Cd-rigid-Re-params}
\end{table}

From Table~\ref{tab:Cd-rigid-Re-params}, it can be seen that the threshold area Reynolds number $Re_t$ is similar across the three species and corresponds to a velocity of roughly 0.2~\mps. This agrees with the velocity at which the trees were observed to begin reconfiguring significantly (see Figs.~\ref{fig:Ap-vs-U-fol} and \ref{fig:Ap-vs-U-defol}).

Excluding the defoliated poplar trees, for which only one specimen was available, the power $p$ to which the Reynolds number is raised in Eq.~\eqref{eq:Cd-rigid-Re-model} is also fairly consistent, only ranging between $-0.54$ and $-0.67$. However, the value for $\alpha$ varies quite significantly between the species, with there being an order of magnitude difference between the poplar trees and the other two species. The sample size for the poplar trees is smaller than that for the alder and willow trees, so it is hard to conclude whether the discrepancy is due to physical processes, differences in species' homogeneity, or sample size.

The relationships none-the-less capture the main variation in rigid drag coefficient, with the coefficient of determination ranging from 0.819 to 0.923. The resulting relationship for the foliated willow trees is illustrated in Fig.~\ref{fig:Cd-rigid-vs-Re-fit}.

\begin{figure}[htbp]
\centering
\includegraphics{Cd-rigid-vs-Re-fit}
\caption{Proposed model (Eq.~\ref{eq:Cd-rigid-Re-model}) for the variation in rigid drag coefficient with Reynolds number for the foliated willow Hydralab trees. The projected area in still air was used as the characteristic length scale when calculating the Reynolds number.}
\label{fig:Cd-rigid-vs-Re-fit}
\end{figure}

Although the power law described in Eq.~\eqref{eq:Cd-rigid-Re-model} is asymptotic with the $x$ axis (\ie $C^*_d > 0$), it is expected that the rigid drag coefficient becomes independent of the Reynolds number for $Re \gg \num{1e6}$, similar to the effect for isolated cylinders \citep{Douglas-05}. However, this is not incorporated into the current model since it is unlikely that riparian flood flows under modelling consideration will achieve such high Reynolds numbers ($U_\textnormal{max} = 4$~\mps for the foliated trees tested in this study).

The model defined above in Eq.~\eqref{eq:Cd-rigid-Re-model} for the rigid drag coefficient can now be used to predict the variation in drag force. The species-specific regression parameters from Table~\ref{tab:Cd-rigid-Re-params} are used to determine the rigid drag coefficient $C^*_d$, which is then substituted into Eq.~\eqref{eq:Cd-rigid}, along with the towing velocity $U$ and projected area in still air $A_{p0}$, to provide the drag force. The resulting drag force predictions are plot against the measured drag forces in Fig.~\ref{fig:Cd-rigid-Re-model} for the foliated and defoliated Hydralab trees.

\begin{figure}[htbp]
\centering
\includegraphics{Cd-rigid-Re-model}
\caption{Drag force as predicted using the rigid drag coefficient model (Eqs.~\ref{eq:Cd-rigid} and \ref{eq:Cd-rigid-Re-model}) for the: (a) foliated; and (b) defoliated Hydralab trees.}
\label{fig:Cd-rigid-Re-model}
\end{figure}

\clearpage

As can be seen from Fig.~\ref{fig:Cd-rigid-Re-model}, the drag force predictions are in good agreement with the measured values, even over the broad range of towing velocities tested in this study ($0.125 \le U \le 6$~\mps). The average errors $\epsilon_{\%}$ between the predicted and measured drag forces are presented in Table~\ref{tab:Cd-rigid-Re-model-errors}.
\nomenclature[zzze]{$\epsilon_{\%}$}{Average percentage error}

\begin{table}[htbp]
\caption{Average percentage errors $\epsilon_{\%}$ for predictions of the foliated and defoliated Hydralab trees' drag forces based on the rigid drag coefficient model (Eqs.~\ref{eq:Cd-rigid} and \ref{eq:Cd-rigid-Re-model}). The number of data points $N$ used to calculate the average errors is also given.}
\centering
\sisetup{table-format=2.1}
\begin{footnotesize}
\begin{tabular}{lSS[table-format=3]SS[table-format=2]}
\toprule
 & \multicolumn{2}{c}{Foliated} & \multicolumn{2}{c}{Defoliated} \\ \cmidrule(lr){2-3} \cmidrule(lr){4-5}
Species & {$\epsilon_{\%}$} & {$N$} & {$\epsilon_{\%}$} & {$N$} \\ \midrule
\alnus & 13.2 & 39 & 6 & 10 \\
\populus & 17.1 & 28 & 5 & 8 \\
\salix & 29.8 & 101 & 15.9 & 23 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:Cd-rigid-Re-model-errors}
\end{table}

The low average errors reported in Table~\ref{tab:Cd-rigid-Re-model-errors} indicate that the model approach is valid and that using species-specific values is applicable. The rigid drag coefficient model (Eqs.~\ref{eq:Cd-rigid} and \ref{eq:Cd-rigid-Re-model}) also represents an order of magnitude improvement over a traditional constant $C_d$ approach, where the drag force predictions would be many times greater than the expected drag force at higher velocities. 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Application}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Now that the rigid drag coefficient model has been shown to be effective at predicting the drag force for the Hydralab trees, it is applied to the independent, branch-scale data set from the LWI experiments (\autoref{sec:LWI-data}). For each force-velocity point, the rigid drag coefficient is first calculated using Eqs.~\eqref{eq:Re-Ap} and \eqref{eq:Cd-rigid-Re-model}, with the values for $C^*_{d0}$, $Re_t$, $\alpha$, and $p$ taken from the foliated \populus and \salix values in Table~\ref{tab:Cd-rigid-Re-params}. The $A_{p0}$ values are taken from Table~\ref{tab:LWI-specimens}. The resulting drag forces, calculated by substituting the rigid drag coefficient back into Eq.~\eqref{eq:Cd-rigid}, for the partially and fully submerged willow and poplar branches are presented in Fig.~\ref{fig:LWI-Cd-rigid-Re-model}.

\begin{figure}[htbp]
\centering
\includegraphics{LWI-Cd-rigid-Re-model}
\caption{Drag force as predicted using the rigid drag coefficient model (Eqs.~\ref{eq:Cd-rigid} and \ref{eq:Cd-rigid-Re-model}) for the: (a) partially; and (b) fully submerged LWI branches.}
\label{fig:LWI-Cd-rigid-Re-model}
\end{figure}

From Fig.~\ref{fig:LWI-Cd-rigid-Re-model} it can be seen that, although the drag coefficient model's drag force predictions are in relatively good agreement with the measured values at the lowest velocities, the model quickly begins to overestimate the drag force by relatively wide margin for $U \gtrsim 0.25$. This is reflected in the high average errors reported in Table~\ref{tab:LWI-Cd-rigid-Re-model-errors}.

\begin{table}[htbp]
\caption{Average percentage errors $\epsilon_{\%}$ for predictions of the partially and fully submerged LWI branches' drag forces based on the rigid drag coefficient model (Eqs.~\ref{eq:Cd-rigid} and \ref{eq:Cd-rigid-Re-model}). The number of data points $N$ used to calculate the average errors is also given.}
\centering
\sisetup{table-format=3}
\begin{footnotesize}
\begin{tabular}{lSS[table-format=1]SS[table-format=1]}
\toprule
 & \multicolumn{2}{c}{Partial sub.} & \multicolumn{2}{c}{Fully sub.} \\ \cmidrule(lr){2-3} \cmidrule(lr){4-5}
Specimen & {$\epsilon_{\%}$} & {$N$} & {$\epsilon_{\%}$} & {$N$} \\ \midrule
WN & 155 & 8 & 256 & 9 \\
PN & 101 & 8 & 110 & 9 \\
PA & 116 & 8 & 142 & 9 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:LWI-Cd-rigid-Re-model-errors}
\end{table}

For both the partially and fully submerged conditions, the predicted drag forces are on average more than double the measured values (Table~\ref{tab:LWI-Cd-rigid-Re-model-errors}). The reason for this overestimation is that the Reynolds numbers stay below the threshold $Re_t$, due to the much lower $A_{p0}$ values for the branches compared to the Hydralab trees from which $Re_t$ was derived (Table~\ref{tab:Cd-rigid-Re-params}). As a consequence, the rigid drag coefficient $C^*_{d}$ is held at its initial value $C^*_{d0}$ and does not decrease with velocity, thereby replicating a quadratic force-velocity response. This therefore implies that the current model (Eq.~\ref{eq:Cd-rigid-Re-model}) is not scale independent.
