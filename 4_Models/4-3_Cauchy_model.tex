%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Cauchy Reconfiguration Model}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The drag coefficient model proposed in the previous section was derived from empirical observations. While it was shown to be accurate when properly calibrated, significant errors were observed when attempting to apply it to vegetation of a different scale and level of submergence. In this section, therefore, a drag force model is developed from theoretical principles, based on the drag force observations and analyses in the previous chapter (see sections \autoref{sec:drag-force-analysis}--\autoref{sec:drag-coef-analysis}).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Theory} \label{sec:cauchy-model-theory}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In order to parameterize the complex flow-body interactions experienced by flexible vegetation, it is assumed that the drag force $F$ is dependent on the velocity $U$, density $\rho$ and kinematic viscosity $\nu$ of the fluid, along with the vegetation's projected area $A_p$ and modulus of elasticity $E$. According to the $\Pi$ theorem of \citet{Buckingham-14}, this system can be described by three dimensionless numbers. Here, we choose the drag coefficient, Reynolds number and the Cauchy number:
%
\begin{align}
& C_d = \frac{2 F}{\rho A_p U^2}; & Re = \frac{A^{1/2}_p U}{\nu}; & & Ca = \frac{\rho U^2}{E}
\label{eq:dimensional-analysis}
\end{align}

This implies that the drag coefficient is dependent on both the Reynolds number and the Cauchy number. For a fully stiff object, the Cauchy number will be less than unity and therefore there will be no deformation under loading \citep{Blevins-90, Cermak-98, Chakrabarti-02}. This leads to the well known observation that for rigid objects, such as plates and cylinders, the drag coefficient is a function of the Reynolds number only. Further, for flows where the Reynolds number effect is essentially constant, the above set of parameters form the classical drag equation \eqref{eq:drag-classical}.

The Cauchy number defined above, however, does not take into account the cross-sectional area or `slenderness' of an object and, as such, cannot accurately represent the compressibility of vegetation \citep{Langre-08}. Therefore, the Cauchy number is re-defined to incorporate the flexural rigidity $EI$:
%
\begin{equation}
Ca = \frac{\rho U^2 A_{p0} H^2}{EI}
\label{eq:cauchy-number}
\end{equation}
%
where $I$ is the second moment of area of the main stem; $A_{p0}$ is the projected area in still air; and $H$ is the vegetation's height.

The definition of the `vegetative' Cauchy number used here (Eq.~\ref{eq:cauchy-number}), differs slightly from that used previously by \citet{Luhar-11} and \citet{Whittaker-13} (see Eqs.~\ref{eq:cauchy-number-luhar} and \ref{eq:drag-cauchy-old}, respectively). In this study, the use of $A_{p0} H^2$ as opposed to the product of the breadth and length ($b l^3$) used by \citet{Luhar-11} is favoured since $A_{p0}$ more accurately describes the vegetation's flow-interacting area when considering vegetation with complex morphology. In the case of idealized slender beams, $A_{p0} H^2$ and $b l^3$ are equivalent. With respect to the Cauchy number utilized in \citet{Whittaker-13}, the volume term is replaced by the projected area, which may be easier to measure \textit{in situ} using non-destructive methods.

In drag force studies on flexible plates and fibres, \citet{Gosselin-10b} and \citet{Gosselin-11} also described the reconfiguration in terms of the Cauchy number. The authors define a separate reconfiguration number $\mathcal{R}$ that compares the measured drag force to that of an equivalent rigid object with the same geometry:
%
\begin{equation}
\mathcal{R} = \frac{2F}{\rho C^*_{d0} A_{p0} U^2}
\label{eq:reconfiguration}
\end{equation}
%
where $C^*_{d0}$ is the drag coefficient of the rigid object (see Eq.~\ref{eq:Cd-rigid}).
\nomenclature[rr]{$\mathcal{R}$}{Reconfiguration number}

If buoyancy and Reynolds number effects are assumed to be negligible, it follows from the dimensional analysis in Eq.~\eqref{eq:dimensional-analysis} that the reconfiguration number is a function of the Cauchy number, \ie $\mathcal{R} = f(Ca)$ \citep{Gosselin-10b, Gosselin-11, Langre-12, Barois-13}. In order to test this relationship, the variation in reconfiguration number with Cauchy number is plot in Fig.~\ref{fig:cauchy-reconf-EI25} for the foliated and defoliated Hydralab trees. The previously calculated initial rigid drag coefficients $C^*_{d0}$ (see \autoref{sec:drag-coef-results}) are used for the values of the rigid drag coefficient in Eq.~\eqref{eq:reconfiguration}.

\begin{figure}[htbp]
\centering
\includegraphics{cauchy-reconf-EI25}
\caption{Variation in reconfiguration number with the Cauchy number for the: (a) foliated; and (b) defoliated Hydralab trees. The value of $EI_{25}$ was used for the flexural rigidity term in the Cauchy number calculation.}
\label{fig:cauchy-reconf-EI25}
\end{figure}

From Fig.~\ref{fig:cauchy-reconf-EI25} it can be seen that the variation follows the expected behaviour, \ie that the drag force exerted on the trees only deviates away from the rigid approximation once they begin to reconfigure ($Ca > 1$). This thus suggests that the parameterization of the `vegetative' Cauchy number in Eq.~\eqref{eq:cauchy-number} is valid. The results for the case where the mid-stem flexural stiffness $EI_{50}$ values are utilized are very similar and are therefore not included here.

Assuming that the density of the fluid is constant, the deviation of the reconfiguration number $\mathcal{R}$ away from unity with increasing velocity (Fig.~\ref{fig:cauchy-reconf-EI25}) must be caused solely by the change in the characteristic drag coefficient term in Eq.~\eqref{eq:drag-classical}. Indeed, in the previous chapter (\autoref{sec:drag-force-analysis-reconf}) it was shown that the characteristic drag coefficient for flexible trees and branches decreases rapidly with velocity, following a roughly inverse power law (see Figs.~\ref{fig:CdAp-vs-U} and \ref{fig:CdAp-vs-U-LWI}).

Considering that $\mathcal{R} = f(Ca)$, it follows that the characteristic drag coefficient is a function of the Cauchy number, i.e. $C_d A_p = f(Ca)$ \citep{Langre-08}. Furthermore, assuming that the relationship between $\mathcal{R}$ and $Ca$ follows a power law, it can be found that the power to which the Cauchy number is raised is equivalent to half the Vogel exponent ($\psi / 2$). The variation in the characteristic drag coefficient with velocity can therefore be described by the following:
%
\begin{equation}
C_d A_p = C^*_{d0} A_{p0} Ca^{\psi/2}
\label{eq:cauchy-CdAp}
\end{equation}
%
where $C^*_{d0}$ is taken to be species-specific, similarly to $C_{d\chi}$ in Eq.~\eqref{eq:form-factor-chi}. The Vogel exponent is also thought to be species-specific \citep{Jarvela-04a, Aberle-12, Aberle-13}.

Physically, Eq.~\eqref{eq:cauchy-CdAp} represents the magnitude ($Ca$) and rate ($\psi$) of the reconfiguration that flexible objects experience in fluid flows, through the reduction in the characteristic drag coefficient from its initial `rigid' value ($C^*_{d0} A_{p0}$) with increasing velocity. It is also applicable to rigid bodies, since when $\psi = 0$ there will be no variation in the characteristic drag coefficient, assuming independence of Reynolds number effects.

It should be noted that, since a Cauchy number of less than unity indicates that there is no deformation or reconfiguration by definition \citep{Blevins-90, Cermak-98, Chakrabarti-02}, a limiter must be placed on the Cauchy number in Eq.~\eqref{eq:cauchy-CdAp}, i.e. $Ca = \max(1, Ca)$. This prevents the characteristic drag coefficient deviating away from its initial rigid value of $C^*_{d0} A_{p0}$ when the drag force exerted on an object is not great enough to overcome the object's bending resistance and cause reconfiguration.

To illustrate the relationship defined in this model between the characteristic drag coefficient and the Cauchy number (Eq.~\ref{eq:cauchy-CdAp}), the variation in characteristic drag coefficient with velocity is plot schematically in Fig.~\ref{fig:cauchy-schematic}.

\begin{figure}[htbp]
\centering
\includegraphics{cauchy-schematic}
\caption{Proposed variation in characteristic drag coefficient with velocity (Eq.~\ref{eq:cauchy-CdAp}) for a flexible object (\ie assuming that $\psi < 0$).}
\label{fig:cauchy-schematic}
\end{figure}

Replacing the drag coefficient and projected area terms in the classical drag equation \eqref{eq:drag-classical} with the relationship defined above (Eq.~\ref{eq:cauchy-CdAp}), we thus arrive at the new `Cauchy reconfiguration model' for predicting the hydrodynamic drag force exerted on flexible vegetation:
%
\begin{equation}
F = \frac{1}{2} \rho C^*_{d0} A_{p0} Ca^{\psi/2} U^2
\label{eq:drag-cauchy}
\end{equation}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Results and Discussion} \label{sec:cauchy-model-results}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Now that the theoretical model has been developed, it can be applied to both the Hydralab and independent LWI data sets.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Validation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In order to validate the model's approach, it is first used to predict the drag forces of the trees from the Hydralab experiments. The physical properties that Eq.~\eqref{eq:drag-cauchy} requires, namely the height $H$, main-stem flexural rigidity $EI$, and projected area in still air $A_{p0}$, are taken from Tables~\ref{tab:hydralab-dimensions}, \ref{tab:hydralab-stiffness}, and \ref{tab:hydralab-Ap0}, respectively. In addition to the trees' physical properties, the model also requires two species-specific coefficients: the rigid drag coefficient $C^*_{d0}$ (Table~\ref{tab:Cd-rigid-Re-params}) and the Vogel exponent $\psi$ (Table~\ref{tab:vogel-exp-stats}).

During the experimental procedure at the CEHIPAR facilities, two values were obtained for the main-stem flexural rigidity of each tree (see \autoref{sec:hydralab-data}). For this validation, both the first quartile $EI_{25}$ and mid-stem $EI_{50}$ flexural rigidities are employed in determining the Cauchy number (Eq.~\ref{eq:cauchy-number}). The resulting drag force predictions for the foliated and defoliated Hydralab trees are presented in Figs.~\ref{fig:cauchy-model-EI25} and \ref{fig:cauchy-model-EI50} for the $EI_{25}$ and $EI_{50}$ cases, respectively.

\begin{figure}[htbp]
\centering
\includegraphics{cauchy-model-EI25}
\caption{Drag force as predicted using the Cauchy reconfiguration model (Eq.~\ref{eq:drag-cauchy}) for the: (a) foliated; and (b) defoliated Hydralab trees. The value of $EI_{25}$ was used for the flexural rigidity term in the Cauchy number calculation.}
\label{fig:cauchy-model-EI25}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics{cauchy-model-EI50}
\caption{Drag force as predicted using the Cauchy reconfiguration model (Eq.~\ref{eq:drag-cauchy}) for the: (a) foliated; and (b) defoliated Hydralab trees. The value of $EI_{50}$ was used for the flexural rigidity term in the Cauchy number calculation.}
\label{fig:cauchy-model-EI50}
\end{figure}

From Figs.~\ref{fig:cauchy-model-EI25} and \ref{fig:cauchy-model-EI50}, it can be seen that there is generally good agreement between the predicted and measured drag forces, although the model tends to somewhat underestimate the drag forces when the trees are defoliated. When comparing the $EI_{25}$ and $EI_{50}$ cases, the predicted drag forces are fairly similar. This is reflected in the average errors presented in Table~\ref{tab:cauchy-model-errors}.

\begin{table}[htbp]
\caption{Average percentage errors $\epsilon_{\%}$ for predictions of the foliated and defoliated Hydralab trees' drag forces based on the Cauchy reconfiguration model (Eq.~\ref{eq:drag-cauchy}). The subscripts `25' and `50' for $\epsilon_{\%}$ denote whether $EI_{25}$ or $EI_{50}$, respectively, was used to calculate the Cauchy number. The number of data points $N$ used to calculate the average errors is also given.}
\centering
\sisetup{table-format=2.1}
\begin{footnotesize}
\begin{tabular}{lSSS[table-format=2]SSS[table-format=2]}
\toprule
 & \multicolumn{3}{c}{Foliated} & \multicolumn{3}{c}{Defoliated} \\ \cmidrule(lr){2-4} \cmidrule(lr){5-7}
Species & {$\epsilon_{\%25}$} & {$\epsilon_{\%50}$} & {$N$} & {$\epsilon_{\%25}$} & {$\epsilon_{\%50}$} & {$N$} \\ \midrule
\alnus & 17.4 & 22.1 & 39 & 10.1 & 6.6 & 10 \\
\populus & 22.9 & 17.3 & 28 & 17.6 & 40.5 & 8 \\
\salix & 24.3 & 20.2 & 54 & 32.9 & 29 & 11 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:cauchy-model-errors}
\end{table}

The difference in accuracy between the $EI_{25}$ and $EI_{50}$ cases is relatively minor, with $EI_{25}$ producing lower errors for some species or foliation states and vice versa (Table~\ref{tab:cauchy-model-errors}). This suggests that the height at which the main-stem flexural rigidity is determined is not crucial, as long as both the second moment of area $I$ and modulus of elasticity $E$ are measured at the same location.

It is interesting to note that the predictions for the defoliated \alnus trees appear to be significantly more accurate than those for the foliated \alnus trees (Table~\ref{tab:cauchy-model-errors}). This is most likely due to the small sample sizes used to determine the species-specific drag coefficient values for the defoliated \alnus trees and thus the species-averaged values better reflect the individual specimens. Overall, the low average errors given in Table~\ref{tab:cauchy-model-errors} represent roughly an order of magnitude improvement over a classical, constant characteristic drag coefficient approach that neglects the effects of reconfiguration (Eq.~\ref{eq:drag-classical}).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Application}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In order to test whether the Cauchy reconfiguration model proposed in Eq.~\eqref{eq:drag-cauchy} is valid for vegetation of differing scale and relative submergence, the model is used to predict the drag force for the partially and fully submerged branches from the LWI experiments (see \autoref{sec:LWI-data}). The artificial poplar branch is included here as it has been shown that its force-velocity response is similar to that of its natural counterparts \citep{Dittrich-12}.

The natural willow and natural and artificial poplar branches' height $H$, main-stem flexural rigidity $EI$, and projected area in still air $A_{p0}$, are taken from Table~\ref{tab:LWI-specimens}. For the partially submerged condition, the water depth is used as the height (\ie $H = 12$~cm). The rigid drag coefficients $C^*_{d0}$ and Vogel exponents $\psi$ are taken from the species-averaged values for the foliated \populus and \salix specimens in Tables~\ref{tab:Cd-rigid-Re-params} and \ref{tab:vogel-exp-stats}, respectively. The resulting drag force predictions from the Cauchy reconfiguration model (Eq.~\ref{eq:drag-cauchy}) are presented in Fig.~\ref{fig:LWI-cauchy-model} for the partially and fully submerged LWI branches.

\begin{figure}[htbp]
\centering
\includegraphics{LWI-cauchy-model}
\caption{Drag force as predicted using the Cauchy reconfiguration model (Eq.~\ref{eq:drag-cauchy}) for the: (a) partially; and (b) fully submerged LWI branches.}
\label{fig:LWI-cauchy-model}
\end{figure}

From Fig.~\ref{fig:LWI-cauchy-model} it can be seen that the model generally predicts the drag force with good accuracy, for both the partially and fully submerged conditions. For the partially submerged branches, however, the model tends to overestimate the drag force at the lower velocities. This is reflected in the average errors presented in Table~\ref{tab:LWI-cauchy-model-errors}, where the average errors for the partially submerged condition are roughly double those for the fully submerged condition.

\begin{table}[htbp]
\caption{Average percentage errors $\epsilon_{\%}$ for predictions of the partially and fully submerged LWI branches' drag forces based on the Cauchy reconfiguration model (Eq.~\ref{eq:drag-cauchy}). The number of data points $N$ used to calculate the average errors is also given.}
\centering
\sisetup{table-format=2.1}
\begin{footnotesize}
\begin{tabular}{lSS[table-format=3]SS[table-format=2]}
\toprule
 & \multicolumn{2}{c}{Partial sub.} & \multicolumn{2}{c}{Fully sub.} \\ \cmidrule(lr){2-3} \cmidrule(lr){4-5}
Specimen & {$\epsilon_{\%}$} & {$N$} & {$\epsilon_{\%}$} & {$N$} \\ \midrule
WN & 20 & 8 & 12 & 9 \\
PN & 17.9 & 8 & 8.5 & 9 \\
PA & 18 & 8 & 6.8 & 9 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:LWI-cauchy-model-errors}
\end{table}

Regardless of the difference between the partially and fully submerged branches, the low average errors presented in Table~\ref{tab:LWI-cauchy-model-errors} suggest that the Cauchy reconfiguration model is independent of both vegetation scale and level of relative submergence.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Comparison to Existing Models} \label{sec:cauchy-model-comparison}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In the second chapter, a number of existing models for predicting the drag force of flexible riparian vegetation were discussed (\autoref{sec:flexible-veg-drag}). In this study, two of the most recent will be used to provide a benchmark against which the Cauchy reconfiguration model proposed in Eq.~\eqref{eq:drag-cauchy} can be measured.

The first is the friction factor model of \citet{Jarvela-04a}, where the leaf area index (LAI) is used as the characteristic reference area (Eq.~\ref{eq:form-factor-chi}). The second is the Cauchy reconfiguration model of \citet{Whittaker-13}, which differs from that proposed here in the definition of the characteristic drag coefficient and Cauchy number terms (Eq.~\ref{eq:drag-cauchy-old}). The drag force predictions from each model are first presented, before the average errors from both models are summarized and compared to those of the Cauchy reconfiguration model developed in this thesis (Eq.~\ref{eq:drag-cauchy}).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{\citet{Jarvela-04a}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

For this approach, the form factor according to Eq.~\eqref{eq:form-factor-chi} is combined with the relationship between the form factor and the spatially-averaged drag force (Eq.~\ref{eq:form-factor}), in order to provide a relationship for the drag force on a single plant:
%
\begin{equation}
F = \frac{1}{2} \rho C_{d\chi} A_L \left(\frac{U}{U_\chi}\right)^\chi U^2
\label{eq:drag-jarvela}
\end{equation}
%
where $C_{d\chi}$ is a species-specific drag coefficient; $A_L$ is the total one-sided leaf area; $\chi$ is equivalent to the Vogel exponent $\psi$; and $U_\chi$ is the lowest velocity used to determine $\chi$.

Although recent work has expanded Eq.~\eqref{eq:drag-jarvela} to be applicable to both foliated and defoliated vegetation by splitting the total drag into its constituent leaf and stem parts \citep{Vastila-14}, the defoliated Hydralab trees are not modelled here as there are only three trees for which one-sided stem areas were recorded (see Table~\ref{tab:hydralab-stem-area}). For the foliated Hydralab trees, the total one-sided leaf areas are taken from Table~\ref{tab:hydralab-leaf-area}, species-specific Vogel exponents $\psi$ are taken from Table~\ref{tab:vogel-exp-stats}, and it is assumed that $C_{d\chi}$ is equivalent to $C^*_{d0}$ (Table~\ref{tab:Cd-rigid-Re-params}). The lowest towing velocity of 0.125~\mps is used as the scaling velocity $U_\chi$.

The drag forces exerted on the fully submerged LWI branches can also be modelled using Eq.~\eqref{eq:drag-jarvela}. The species-specific Vogel exponents $\psi$ and drag coefficients $C_{d\chi}$ are taken from the \populus and \salix values in Tables~\ref{tab:vogel-exp-stats} and \ref{tab:Cd-rigid-Re-params}, respectively. The total one-sided leaf area $A_L$ is taken from Table~\ref{tab:LWI-specimens}, while the scaling velocity $U_\chi$ is equal to the lowest velocity tested for the Hydralab trees (0.125~\mps) as this was the lowest velocity used to determine the Vogel exponents. Unfortunately, the partially submerged branches cannot be modelled since the total one-sided leaf area is unknown for these specimens.

The resulting drag force predictions for the foliated Hydralab trees and fully submerged LWI branches are shown in Fig.~\ref{fig:jarvela04-model}. It can be seen that the model (Eq.~\ref{eq:drag-jarvela}) predicts the trees' drag forces with good accuracy, although there is some overestimation at the lower velocities. However, for the LWI branches, the model consistently overestimates the drag force by a relatively significant margin.

\begin{figure}[htbp]
\centering
\includegraphics{jarvela04-model}
\caption{Drag force as predicted using the friction factor model (Eq.~\ref{eq:drag-jarvela}) of \citet{Jarvela-04a} for the: (a) foliated Hydralab trees; and (b) fully submerged LWI branches.}
\label{fig:jarvela04-model}
\end{figure}

The fact that the drag forces for the LWI branches are overestimated by a relatively constant percentage (Fig.~\ref{fig:jarvela04-model}b) suggests that the curve of the force-velocity response, and thus the Vogel exponent, is correct. However, it also indicates that the 'rigid' drag coefficient $C^*_{d0}$ may not be a suitable replacement for $C_{d\chi}$. Indeed, the values for $C_{d\chi}$ determined in previous studies are typically much less than those found for $C^*_{d0}$ in this study (Table~\ref{tab:Cd-rigid-Re-params}). For example, \cite{Aberle-13} summarized $C_{d\chi}$ values from a number of previous studies, which included branches of \textit{Populus nigra} ($C_{d\chi} = 0.33$), \textit{Salix caprea} ($C_{d\chi} = 0.43$) and \textit{Salix triandra x viminalis} ($C_{d\chi} = 0.53$).

Given that the model (Eq.~\ref{eq:drag-jarvela}) was able to predict the drag forces of the Hydralab trees with good accuracy using the same $C_{d\chi}$ values as used for the LWI branches, this might suggest that the model is not truly scale independent since separate $C_{d\chi}$ values may be required for each scale of vegetation.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{\citet{Whittaker-13}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In this approach, the previous Cauchy reconfiguration model (Eq.~\ref{eq:drag-cauchy-old}) developed by \citet{Whittaker-13} is used to predict the Hydralab trees' drag forces. The drag forces for the LWI branches cannot be modelled here as their volumes were not recorded.

For the Hydralab trees, the model requires the height $H$ (Table~\ref{tab:hydralab-dimensions}), main-stem flexural rigidity $EI$ (Table~\ref{tab:hydralab-stiffness}), and total volume $V$ (see Tables~\ref{tab:hydralab-mass-total} and \ref{tab:hydralab-mass-wood} for the foliated and defoliated trees, respectively). The species-specific Vogel exponents $\psi$ are taken from Table~\ref{tab:vogel-exp-stats}. Species-specific linear relationships between the coefficient $K$ and the trees' volumes are taken from \citet{Whittaker-13}.

The resulting drag force predictions for the foliated and defoliated Hydralab trees are presented in Fig.~\ref{fig:whittaker13-model-EI25}. For the foliated alder and poplar trees, there is reasonable agreement between the measured and predicted drag forces. However, for the foliated willow trees, there appears to be significant deviation for a number of specimens. This is most likely a result of the linear $K$--$V$ relationship not accurately describing the $K$ values for those specimens.

\begin{figure}[htbp]
\centering
\includegraphics{whittaker13-model-EI25}
\caption{Drag force as predicted using the Cauchy reconfiguration model (Eq.~\ref{eq:drag-cauchy-old}) of \citet{Whittaker-13} for the: (a) foliated; and (b) defoliated Hydralab trees. The value of $EI_{25}$ was used for the flexural rigidity term in the Cauchy number calculation.}
\label{fig:whittaker13-model-EI25}
\end{figure}

When the trees are defoliated, the model tends to overestimate the drag force, particularly at the lower velocities (Fig.~\ref{fig:whittaker13-model-EI25}b). This is could possibly be due to an incorrect parameterization of the Cauchy number in Eq.~\eqref{eq:drag-cauchy-old}, since if the Cauchy number is less than unity there will be no reduction in the characteristic drag coefficient, thus leading to higher predicted drag forces.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Comparison}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The average errors in the drag force predictions for each of the three models are summarized in Table~\ref{tab:model-error-comparison} for the foliated Hydralab trees and the fully submerged LWI branches. The defoliated Hydralab trees and partially submerged LWI branches are not included as the two existing models (Eqs.~\ref{eq:drag-jarvela} and \ref{eq:drag-cauchy-old}) could not be applied to those data sets.

\begin{table}[htbp]
\caption{Average percentage errors $\epsilon_{\%}$ for predictions of the foliated Hydralab trees' and fully submerged LWI branches' drag forces based on the current (Eq.~\ref{eq:drag-cauchy}) and two existing models, namely Eq.~\eqref{eq:drag-jarvela} \citep{Jarvela-04a} and Eq.~\eqref{eq:drag-cauchy-old} \citep{Whittaker-13}. The number of data points $N$ used to calculate the average errors is also given. The value of $EI_{25}$ was used for the flexural rigidity term in the Cauchy number calculation.}
\centering
\sisetup{table-format=2.1}
\begin{footnotesize}
\begin{tabular}{lSS[table-format=2]SS[table-format=2]SS[table-format=2]}
\toprule
Species / & \multicolumn{2}{c}{\citet{Jarvela-04a}} & \multicolumn{2}{c}{\citet{Whittaker-13}} & \multicolumn{2}{c}{Current} \\ \cmidrule(lr){2-3} \cmidrule(lr){4-5} \cmidrule(lr){6-7}
specimen & {$\epsilon_{\%}$} & {$N$} & {$\epsilon_{\%}$} & {$N$} & {$\epsilon_{\%}$} & {$N$} \\ \midrule
\alnus & 31.3 & 33 & 26.6 & 33 & 17.4 & 39 \\
\populus & 19.2 & 28 & 29 & 28 & 22.9 & 28 \\
\salix & 28.7 & 16 & 38.2 & 80 & 24.3 & 54 \\
WN & 52.6 & 9 & {-} & {-} & 12 & 9 \\
PN & 91.4 & 9 & {-} & {-} & 8.5 & 9 \\
PA & 73 & 9 & {-} & {-} & 6.8 & 9 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:model-error-comparison}
\end{table}

It can be seen from Table~\ref{tab:model-error-comparison} that the Cauchy reconfiguration model developed here (Eq.~\ref{eq:drag-cauchy}) has the lowest average errors in all cases, apart from for the poplar trees from the Hydralab experiments, where the model developed by \citet{Jarvela-04a} is more accurate. However, the difference of 3\% in overall average error between the two models for those trees is relatively small.

For the independent LWI branch data, the difference in accuracy between the two models is much more pronounced, with the current model being almost 5--10 times more accurate. This may highlight scale dependency problems for the \citet{Jarvela-04a} approach. However, recent work by \citet{Jalonen-13a} found that the total one-sided area $A_T = A_S + A_L$ might be a better characteristic reference area than the one-side leaf area $A_L$ for Eq.~\eqref{eq:drag-jarvela}. Unfortunately $A_T$ is only available here for one specimen, namely the alder tree A1, and therefore this approach was not tested.

The model of \citet{Jarvela-04a} is also dependent on an empirical scaling velocity $U_\chi$ (Eq.~\ref{eq:drag-jarvela}), which has to be determined experimentally and is not based on physical reasoning. Additionally, it is unclear how sensitive the model is to larger values of $U_\chi$, since large errors may be introduced for velocities below this value.

While the Cauchy reconfiguration model proposed here has been found to be an improvement over existing drag force models (Table~\ref{tab:model-error-comparison}), it is noted that the projected area in still air $A_{p0}$ may be more difficult to obtain in practice than the one-sided leaf area $A_L$. This is because $A_L$ can be measured remotely \citep[\eg][]{Rautiainen-03, Zheng-09, Antonarakis-10, Forzieri-11}, while no such method currently exists for $A_{p0}$, which must be determined through photographic analysis.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Model Parameter Sensitivity Analysis} \label{sec:cauchy-model-sensitivity}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Although the current (Eq.~\ref{eq:drag-cauchy}) and previous drag force models (\eg Eqs.~\ref{eq:drag-cauchy-old} and \ref{eq:drag-jarvela}) rely on only a couple of non-physical properties, typically a drag coefficient and a Vogel exponent, it is unclear how varying the values for these parameters affects the models' accuracy. Therefore, a sensitivity analysis for these parameters is undertaken for the Cauchy reconfiguration model developed in this thesis.

The resulting average errors for the model's drag force predictions for the Hydralab trees and LWI branches are presented in Figs.~\ref{fig:cauchy-sensitivity-EI25} and \ref{fig:LWI-cauchy-sensitivity}, respectively. The errors were obtained by varying the rigid drag coefficient $C^*_{d0}$ from 0.25 to 1.5 in increments of 0.05, while the Vogel exponent $\psi$ was varied with the same increment between 0 and $-1.25$. For the Hydralab trees, only the results from using the first quartile stem flexural rigidity $EI_{25}$ in the Cauchy number calculation (Eq.~\ref{eq:cauchy-number}) are presented here as the mid-stem flexural rigidity $EI_{50}$ produced very similar patterns.

\begin{figure}[htbp]
\centering
\includegraphics{cauchy-sensitivity-EI25}
\caption{Sensitivity analysis for the Cauchy reconfiguration model parameters $C^*_{d0}$ and $\psi$. Contour isolines show average percentage errors in the predicted drag force (obtained from Eq.~\ref{eq:drag-cauchy}) for the: (a,b) \alnus; (c,d) \populus; and (e,f) \salix Hydralab trees. The foliated and defoliated trees are in the left and right columns, respectively. The black dot indicates the values used in the model validation (\autoref{sec:cauchy-model-results}). The value of $EI_{25}$ was used for the flexural rigidity term in the Cauchy number calculation.}
\label{fig:cauchy-sensitivity-EI25}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics{LWI-cauchy-sensitivity}
\caption{Sensitivity analysis for the Cauchy reconfiguration model parameters $C^*_{d0}$ and $\psi$. Contour isolines show average percentage errors in the predicted drag force (obtained from Eq.~\ref{eq:drag-cauchy}) for the: (a,b) natural willow (WN); (c,d) natural poplar (PN); and (e,f) artificial poplar (PA) LWI branches. The partially and fully submerged branches are in the left and right columns, respectively. The black dot indicates the values used in the model validation (\autoref{sec:cauchy-model-results}).}
\label{fig:LWI-cauchy-sensitivity}
\end{figure}

\clearpage

From Figs.~\ref{fig:cauchy-sensitivity-EI25} and \ref{fig:LWI-cauchy-sensitivity} it can be seen that, for both the Hydralab trees and the LWI branches, there is a relatively large `parameter space' for which the Cauchy reconfiguration model's average error is less than 30\%. However, outside those regions, the average errors quickly increase, as can be seen by the steep `gradient' of the contour lines towards the upper right and lower left of each sub-plot.

Considering the impact of foliage on the average errors for the Hydralab trees, the parameter space for which the model is reasonably accurate appears to shift towards the upper right corner (Fig.~\ref{fig:cauchy-sensitivity-EI25}). This is consistent with the analyses in the previous chapter (\autoref{sec:experimental-data}), where the defoliated drag coefficients (Fig~\ref{fig:Cd-vs-U-fol-defol}) and Vogel exponents (Table~\ref{tab:vogel-exp-stats}) were found to be greater than those of the foliated trees.

For the partially and fully submerged LWI branches, the parameter spaces for which the model is accurate are relatively similar (Fig.~\ref{fig:LWI-cauchy-sensitivity}). This thus suggests that the Cauchy reconfiguration model is indeed independent of the relative level of submergence as the values for $C^*_{d0}$ and $\psi$ do not need to be altered depending on the flow depth.

In absence of experimental data, inspection of Figs.~\ref{fig:cauchy-sensitivity-EI25} and \ref{fig:LWI-cauchy-sensitivity} indicates that values of $C^*_{d0} \approx 0.8$ and $\psi \approx -0.75$ would be reasonable estimates for foliated trees, while values of $C^*_{d0} \approx 1$ and $\psi \approx -0.7$ would be more suitable for defoliated trees. It is interesting to note that this would appear to contradict the species-specific assumption, since reasonably accurate predictions can be made using the same parameter values across species. However, species-specific values still result in more accurate predictions, at least for the sample sizes presented here.
