#!/usr/bin/env gnuplot

###############################################################################
# SETTINGS
###############################################################################

# Datafiles
DATA = "../data/~Cd-rigid-vs-Re-fol.csv"

# CSV input
set datafile separator comma

# Plot options
set terminal pdfcairo monochrome enhanced dashed size 3,2.5 font "NimbusRomNo9L-Regu,22" fontscale 0.4
set xlabel "{/NimbusRomNo9L-ReguItal Re} (-)" offset 0,0.5
set ylabel "{/NimbusRomNo9L-ReguItal C@^{*}_{d}} (-)" offset 0.5,0
set xrange [1E4:1E7]
set yrange [1E-2:1E1]
set logscale xy
set format xy "10^{%L}"
set xtics scale 0.5 font ",18"
set ytics scale 0.5 font ",18"
set key Left reverse box lw 0.5 height 0.5 width 0.25 samplen 3 font ",18"

set style data linespoints
set pointsize 0.6

# Functions
Re_t = 1E5
Cd0 = 0.891
alpha = 2004
p = -0.671
Rsq = 0.819
f(x) = (x <= Re_t ? Cd0 : alpha * x**p)


###############################################################################
# PLOT
###############################################################################

set output "../images/Cd-rigid-vs-Re-fit.pdf"

plot	DATA index 8::1 u 1:2 lt 3 pt 6 notitle, \
		f(x) w lines lt 1 lw 1.5 t sprintf("{/NimbusRomNo9L-ReguItal R}^2 = %.3f", Rsq)
