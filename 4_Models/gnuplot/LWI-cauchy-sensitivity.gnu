#!/usr/bin/env gnuplot

###############################################################################
# SETTINGS
###############################################################################

# Datafiles
FU_DATA = "../data/LWI-F-vs-U.csv"
PH_DATA = "../data/LWI-physical-properties.csv"

RESULTS = "../data/~LWI-cauchy-sensitivity.csv"

TMP = "tmp.dat"

# Create results file
HEADER = "# Cd0 (-),Vogel (-),WN_h12 (%),WN_h25 (%),PN_h12 (%),PN_h25 (%),PA_h12 (%),PA_h25 (%)"
system(sprintf("echo \"%s\" > %s", HEADER, RESULTS))

# Functions for data fitting
RHO = 1000
max(x,y) = (x > y ? x : y)

Ca(U) = max(1, (RHO * U**2 * AP * H**2) / EI)

F(U) = 0.5 * RHO * CD * AP * Ca(U)**(E/2) * U**2

error(meas,pred) = 100 * abs(meas - pred) / meas

# Cd and E ranges
CD_MIN = 25
CD_MAX = 150
CD_INT = 5
E_MIN = 0
E_MAX = -125
E_INT = -5
N = (CD_MAX-CD_MIN) / CD_INT + 1
count = 0

# Output progress to STDOUT rather than STDERR
set print "-"

# Iterate over CD-E space (cannot iterate using float values)
do for [cd = CD_MIN:CD_MAX:CD_INT] {
	CD = cd / 100.0
	do for [e = E_MIN:E_MAX:E_INT] {
		E = e / 100.0

		# Calculate model error
		set datafile separator comma
		set table TMP
		do for [i = 1:11:2] {
			# Get specimen name
			S = system(sprintf("awk -F, 'NR == 1 {print $%i; exit}' %s", i, FU_DATA))

			# Get physical properties
			H = real(system(sprintf("awk -F, '$1 == \"%s\" {print $2; exit}' %s", S, PH_DATA)))
			AP = real(system(sprintf("awk -F, '$1 == \"%s\" {print $4; exit}' %s", S, PH_DATA)))
			EI = real(system(sprintf("awk -F, '$1 == \"%s\" {print $6; exit}' %s", S, PH_DATA)))

			# Calculate model error
			splot FU_DATA u i+1:(F(column(i))):(error(column(i+1),F(column(i)))) t S
		}
		unset table

		# Format output file
		system(sprintf("sed -i -e '/^# [^C]/d' -e '/u$/d' %s", TMP))

		# Get average errors
		set datafile separator whitespace
		stats TMP index 0 u 3 name "WN_h12_ERR" nooutput
		stats TMP index 1 u 3 name "WN_h25_ERR" nooutput
		stats TMP index 2 u 3 name "PN_h12_ERR" nooutput
		stats TMP index 3 u 3 name "PN_h25_ERR" nooutput
		stats TMP index 4 u 3 name "PA_h12_ERR" nooutput
		stats TMP index 5 u 3 name "PA_h25_ERR" nooutput

		# Write data to file
		system(sprintf("echo \"%g,%g,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\" >> %s", CD, E, WN_h12_ERR_mean, WN_h25_ERR_mean, PN_h12_ERR_mean, PN_h25_ERR_mean, PA_h12_ERR_mean, PA_h25_ERR_mean, RESULTS))
	}

	# Separate rows and output progress indicator
	system(sprintf("echo \"\" >> %s", RESULTS))
	count = count + 1
	print sprintf("Complete: %.1f%%", count * 100.0/N)
}

# Tidy up
system(sprintf("[ -e %s ] && rm %s", TMP, TMP))
