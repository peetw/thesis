#!/usr/bin/env gnuplot

###############################################################################
# SETTINGS
###############################################################################

# Datafiles
FU_DATA = "../data/F-vs-U.csv"
PH_DATA = "../data/physical-properties.csv"

RESULTS = "../data/~cauchy-sensitivity-EI25.csv"

TMP = "tmp.dat"

# Create results file
HEADER = "# Cd0 (-),Vogel (-),Alnus_fol_err (%),Alnus_defol_err (%),Populus_fol_err (%),Populus_defol_err (%),Salix_fol_err (%),Salix_defol_err (%)"
system(sprintf("echo \"%s\" > %s", HEADER, RESULTS))

# Functions for data fitting
RHO = 1000
max(x,y) = (x > y ? x : y)

Ca(U) = max(1, (RHO * U**2 * AP * H**2) / EI_25)

F(U) = 0.5 * RHO * CD * AP * Ca(U)**(E/2) * U**2

error(meas,pred) = 100 * abs(meas - pred) / meas

# Cd and E ranges
CD_MIN = 25
CD_MAX = 150
CD_INT = 5
E_MIN = 0
E_MAX = -125
E_INT = -5
N = (CD_MAX-CD_MIN) / CD_INT + 1
count = 0

# Output progress to STDOUT rather than STDERR
set print "-"

# Iterate over CD-E space (cannot iterate using float values)
do for [cd = CD_MIN:CD_MAX:CD_INT] {
	CD = cd / 100.0
	do for [e = E_MIN:E_MAX:E_INT] {
		E = e / 100.0

		#######################################################################
		# FOLIATED
		#######################################################################

		# Calculate model error
		set datafile separator comma
		set table TMP
		do for [i = 1:156:5] {
			# Get specimen name
			S = system(sprintf("awk -F, 'NR == 1 {print $%i; exit}' %s", i, FU_DATA))

			# Only run for specimens without missing data
			if (S eq "A1" || S eq "A2" || S eq "A3" || S eq "A4" || S eq "A5" || \
				S eq "P2" || S eq "P3" || S eq "P4" || \
				S eq "S3" || S eq "S4" || S eq "S8" || S eq "S10" || S eq "S11" || S eq "S12") {
				# Get physical properties
				H = real(system(sprintf("awk -F, '$1 == \"%s\" {print $2; exit}' %s", S, PH_DATA)))
				AP = real(system(sprintf("awk -F, '$1 == \"%s\" {print $4; exit}' %s", S, PH_DATA)))
				EI_25 = real(system(sprintf("awk -F, '$1 == \"%s\" {print $6; exit}' %s", S, PH_DATA)))

				# Calculate model error
				splot FU_DATA u i+1:(F(column(i))):(error(column(i+1),F(column(i))))
			}
		}
		unset table

		# Format output file
		system(sprintf("sed -i -e '/^# [^C]/d' -e '/u$/d' %s", TMP))

		# Get average errors
		set datafile separator whitespace
		stats TMP index 0:4 u 3 name "ALNUS_FOL_ERR" nooutput
		stats TMP index 5:7 u 3 name "POPULUS_FOL_ERR" nooutput
		stats TMP index 8::1 u 3 name "SALIX_FOL_ERR" nooutput


		#######################################################################
		# DEFOLIATED
		#######################################################################

		# Calculate model error
		set datafile separator comma
		set table TMP
		do for [i = 1:156:5] {
			# Get specimen name
			S = system(sprintf("awk -F, 'NR == 1 {print $%i; exit}' %s", i, FU_DATA))

			# Only run for specimens without missing data
			if (S eq "A1" || S eq "P1" || S eq "S2") {
				# Get physical properties
				H = real(system(sprintf("awk -F, '$1 == \"%s\" {print $2; exit}' %s", S, PH_DATA)))
				AP = real(system(sprintf("awk -F, '$1 == \"%s\" {print $5; exit}' %s", S, PH_DATA)))
				EI_25 = real(system(sprintf("awk -F, '$1 == \"%s\" {print $6; exit}' %s", S, PH_DATA)))

				# Calculate model error
				splot FU_DATA u i+2:(F(column(i))):(error(column(i+2),F(column(i))))
			}
		}
		unset table

		# Format output file
		system(sprintf("sed -i -e '/^# [^C]/d' -e '/u$/d' %s", TMP))

		# Get average errors
		set datafile separator whitespace
		stats TMP index 0 u 3 name "ALNUS_DEFOL_ERR" nooutput
		stats TMP index 1 u 3 name "POPULUS_DEFOL_ERR" nooutput
		stats TMP index 2 u 3 name "SALIX_DEFOL_ERR" nooutput

		# Write data to file
		system(sprintf("echo \"%g,%g,%.2f,%.2f,%.2f,%.2f,%.2f,%.2f\" >> %s", CD, E, ALNUS_FOL_ERR_mean, ALNUS_DEFOL_ERR_mean, POPULUS_FOL_ERR_mean, POPULUS_DEFOL_ERR_mean, SALIX_FOL_ERR_mean, SALIX_DEFOL_ERR_mean, RESULTS))
	}

	# Separate rows and output progress indicator
	system(sprintf("echo \"\" >> %s", RESULTS))
	count = count + 1
	print sprintf("Complete: %.1f%%", count * 100.0/N)
}

# Tidy up
system(sprintf("[ -e %s ] && rm %s", TMP, TMP))
