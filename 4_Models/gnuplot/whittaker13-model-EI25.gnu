#!/usr/bin/env gnuplot

###############################################################################
# SETTINGS
###############################################################################

# Datafiles
FU_DATA = "../data/F-vs-U.csv"
PH_DATA = "../data/physical-properties.csv"
VG_DATA = "../data/~vogel-exponents.csv"

FOL_RESULTS = "../data/~whittaker13-model-EI25-fol.csv"
DEFOL_RESULTS = "../data/~whittaker13-model-EI25-defol.csv"

TMP = "tmp.dat"
HEADER = "# F_meas (N),F_pred (N),err (%)"

# CSV input
set datafile separator comma

# Functions for data fitting
RHO = 1000

Ca(U) = RHO * U**2 * V/100**3 * H / EI_25

F(U) = 0.5 * RHO * K * Ca(U)**(E/2) * U**2

error(meas,pred) = 100 * abs(meas - pred) / meas


###############################################################################
# FOLIATED
###############################################################################

# Species-averaged Vogel exponents
stats VG_DATA index 0 u 4 name "ALNUS_VOGEL" nooutput
stats VG_DATA index 1 u 4 name "POPULUS_VOGEL" nooutput
stats VG_DATA index 2 u 4 name "SALIX_VOGEL" nooutput

# Calculate model error
set table FOL_RESULTS
do for [i = 1:156:5] {
	# Get specimen name
	S = system(sprintf("awk -F, 'NR == 1 {print $%i; exit}' %s", i, FU_DATA))

	# Only run for specimens without missing data
	if (S eq "A1" || S eq "A2" || S eq "A4" || S eq "A5" || \
		S eq "P2B2" || S eq "P3" || S eq "P4B1" || \
		S eq "S2" || S eq "S3" || S eq "S5B1" || S eq "S5B2" || S eq "S6B1" || S eq "S6B2" || S eq "S7B2" || S eq "S10" || S eq "S11" || S eq "S12") {
		# Get physical properties
		V = real(system(sprintf("awk -F, '$1 == \"%s\" {print $8; exit}' %s", S, PH_DATA)))
		H = real(system(sprintf("awk -F, '$1 == \"%s\" {print $2; exit}' %s", S, PH_DATA)))
		EI_25 = real(system(sprintf("awk -F, '$1 == \"%s\" {print $6; exit}' %s", S, PH_DATA)))

		# Get Vogel exponent
		E = (substr(S,1,1) eq "A" ? ALNUS_VOGEL_mean : (substr(S,1,1) eq "P" ? POPULUS_VOGEL_mean : SALIX_VOGEL_mean))

		# K-V relationship
		K = (substr(S,1,1) eq "A" ? 7.912E-7*V+4.122E-2 : (substr(S,1,1) eq "P" ? 4.31E-6*V+1.721E-2 : 3.839E-5*V-2.7E-2))

		# Calculate model error
		splot FU_DATA u i+1:(F(column(i))):(error(column(i+1),F(column(i)))) t S
	}
}
unset table

# Format output file
system(sprintf("awk '{prev[NR] = $4; if ($2 == \"Curve\") {name = $4; gsub(/\"/, \"\", name)} else if ($4 == \"i\") {if (prev[NR-1] == \"z\") {if (NR == 7) {print \"# \"name\"\\n%s\"} else {print \"\\n\\n# \"name\"\\n%s\"}} printf(\"%%.2f,%%.2f,%%.2f\\n\", $1, $2, $3)}}' %s >> %s && mv %s %s", HEADER, HEADER, FOL_RESULTS, TMP, TMP, FOL_RESULTS))


###############################################################################
# DEFOLIATED
###############################################################################

# Species-averaged Vogel exponents
stats VG_DATA index 0 u 8 name "ALNUS_VOGEL" nooutput
stats VG_DATA index 1 u 8 name "POPULUS_VOGEL" nooutput
stats VG_DATA index 2 u 8 name "SALIX_VOGEL" nooutput

# Calculate model error
set table DEFOL_RESULTS
do for [i = 1:156:5] {
	# Get specimen name
	S = system(sprintf("awk -F, 'NR == 1 {print $%i; exit}' %s", i, FU_DATA))

	# Only run for specimens without missing data
	if (S eq "A1" || S eq "A4" || S eq "A5" || \
		S eq "P1" || S eq "P2B2" || S eq "P3" || S eq "P4B1" || \
		S eq "S2" || S eq "S3" || S eq "S5B1" || S eq "S5B2" || S eq "S6B2" || S eq "S7B1" || S eq "S7B2" || S eq "S8" || S eq "S10" || S eq "S11" || S eq "S12") {
		# Get physical properties
		V = real(system(sprintf("awk -F, '$1 == \"%s\" {print $9; exit}' %s", S, PH_DATA)))
		H = real(system(sprintf("awk -F, '$1 == \"%s\" {print $2; exit}' %s", S, PH_DATA)))
		EI_25 = real(system(sprintf("awk -F, '$1 == \"%s\" {print $6; exit}' %s", S, PH_DATA)))

		# Get Vogel exponent
		E = (substr(S,1,1) eq "A" ? ALNUS_VOGEL_mean : (substr(S,1,1) eq "P" ? POPULUS_VOGEL_mean : SALIX_VOGEL_mean))

		# K-V relationship
		K = (substr(S,1,1) eq "A" ? 1.107E-5*V+1.961E-2 : (substr(S,1,1) eq "P" ? 3.386E-6*V+1.368E-2 : 1.588E-5*V-9.239E-4))

		# Calculate model error
		splot FU_DATA u i+2:(F(column(i))):(error(column(i+2),F(column(i)))) t S
	}
}
unset table

# Format output file
system(sprintf("awk '{prev[NR] = $4; if ($2 == \"Curve\") {name = $4; gsub(/\"/, \"\", name)} else if ($4 == \"i\") {if (prev[NR-1] == \"z\") {if (NR == 7) {print \"# \"name\"\\n%s\"} else {print \"\\n\\n# \"name\"\\n%s\"}} printf(\"%%.2f,%%.2f,%%.2f\\n\", $1, $2, $3)}}' %s >> %s && mv %s %s", HEADER, HEADER, DEFOL_RESULTS, TMP, TMP, DEFOL_RESULTS))
