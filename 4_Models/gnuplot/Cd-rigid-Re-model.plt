#!/usr/bin/env gnuplot

###############################################################################
# SETTINGS
###############################################################################

# Datafiles
FOL_DATA = "../data/~Cd-rigid-Re-model-fol.csv"
DEFOL_DATA = "../data/~Cd-rigid-Re-model-defol.csv"

# CSV input
set datafile separator comma

# Plot options
set terminal pdfcairo monochrome enhanced dashed size 5.2,2.3 font "NimbusRomNo9L-Regu,18" fontscale 0.4
set xlabel "Measured, {/NimbusRomNo9L-ReguItal F} (N)" offset 0,0.3 font ",20"
set ylabel "Predicted, {/NimbusRomNo9L-ReguItal F} (N)" offset 1.5,0 font ",20"
set xrange [1:1000]
set yrange [1:1000]
set xtics scale 0.5
set ytics scale 0.5
set logscale xy
set key bottom Left reverse box lw 0.5 height 0.3 width -1 samplen 2 font ",16"
set bmargin 2.75

set style data points
set pointsize 0.6

set output "../images/Cd-rigid-Re-model.pdf"
set multiplot layout 1,2

# Functions
f(x) = x


###############################################################################
# FOLIATED
###############################################################################

set label 1 "(a)" at graph 0.09,0.91 center

plot	FOL_DATA index 0:4 u 1:2 pt 1 t "{/NimbusRomNo9L-ReguItal A. glutinosa}", \
		FOL_DATA index 5:7 u 1:2 pt 4 t "{/NimbusRomNo9L-ReguItal P. nigra}", \
		FOL_DATA index 8::1 u 1:2 pt 6 t "{/NimbusRomNo9L-ReguItal S. alba}", \
		f(x) w lines lt 2 t "1:1"


###############################################################################
# DEFOLIATED
###############################################################################

set key width 1
set label 1 "(b)"

plot	DEFOL_DATA index "A1" u 1:2 pt 1 t "A1", \
		DEFOL_DATA index "P1" u 1:2 pt 4 t "P1", \
		DEFOL_DATA index "S1" u 1:2 pt 6 t "S1", \
		DEFOL_DATA index "S2" u 1:2 pt 8 t "S2", \
		f(x) w lines lt 2 t "1:1"
