#!/usr/bin/env gnuplot

###############################################################################
# SETTINGS
###############################################################################

# Datafiles
FU_DATA = "../data/LWI-F-vs-U.csv"
PH_DATA = "../data/LWI-physical-properties.csv"
TMP = "tmp.dat"
RESULTS = "../data/~LWI-Cd-rigid-Re-model.csv"

# CSV input
set datafile separator comma
set macro

# Functions
RHO = 1000.0
NU = 1.3E-6
Re(U) = U * sqrt(AP) / NU
Cd(U) = (Re(U) <= Ret ? Cd0 : alpha * Re(U)**p)
F(U) = 0.5 * RHO * Cd(U) * AP * U**2

error(meas,pred) = 100 * abs(meas - pred) / meas

# Loop through all specimens
str = ""
do for [i=1:11:2] {

	# Get specimen name
	SPECIMEN = system(sprintf("awk -F, 'NR == 1 {print $%i}' %s", i, FU_DATA))

	# Get projected area
	AP = real(system(sprintf("awk -F, '$1 == \"%s\" {print $4}' %s | sed 's/NULL/NaN/'", SPECIMEN, PH_DATA)))

	# Set model params
	S = substr(SPECIMEN,1,1)
	Cd0 = (S eq "P" ? 0.756 : 0.891)
	Ret = (S eq "P" ? 9E4 : 1E5)
	alpha = (S eq "P" ? 2.94E2 : 2E3)
	p = (S eq "P" ? -0.54 : -0.671)

	# Calculate predicted drag force and percentage error
	set table TMP
		splot FU_DATA u (column(i+1)):(F(column(i))):(error(column(i+1),F(column(i))))
	unset table

	# Format data
	str = str . system(sprintf("awk 'BEGIN {print \"# %s\\n# F_meas (N),F_pred (N),err (%%)\"} $4 == \"i\" {printf(\"%%.2f,%%.2f,%%.2f\\n\", $1, $2, $3)} END {print \"\\n\\n\"}' %s", SPECIMEN, TMP))
}

# Write data to file and tidy up
system(sprintf("echo '%s' > %s", str, RESULTS))
system(sprintf("[ -e %s ] && rm %s", TMP, TMP))
