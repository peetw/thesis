#!/usr/bin/env gnuplot

###############################################################################
# SETTINGS
###############################################################################

# Plot options
set terminal pdfcairo monochrome enhanced dashed size 3,2 font "NimbusRomNo9L-Regu,22" fontscale 0.4
set xrange [0:5]
set yrange [0:2.75]
set xlabel "{/NimbusRomNo9L-ReguItal U} (m s^{-1})" offset 0,0.25
set ylabel "{/NimbusRomNo9L-ReguItal C_d A_p} (m^2)"
unset xtics
unset ytics
unset key
set bmargin 1.75
set samples 300

# Functions
f(x) = (x < 2 ? 2 : 8.0 / x**2)


###############################################################################
# PLOT
###############################################################################

set output "../images/cauchy-schematic.pdf"

set arrow 1 from 2,2.2 to 1,2.2 size 0.25,10
set arrow 2 from 2,2.2 to 3,2.2 size 0.25,10
set arrow 3 from 2,1.45 to 2,2.55 lt 2 lw 2 nohead
set label 1 "{/NimbusRomNo9L-ReguItal Ca} < 1" at 1.15,2.4 font ",18"
set label 2 "{/NimbusRomNo9L-ReguItal Ca} > 1" at 2.25,2.4 font ",18"
set label 3 "{/NimbusRomNo9L-ReguItal U_t}" at 2,1.25 center font ",18"
set label 4 "({/NimbusRomNo9L-ReguItal Ca} = 1)" at 2,0.95 center font ",18"

plot f(x) w lines
