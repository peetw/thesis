#!/usr/bin/env gnuplot

###############################################################################
# SETTINGS
###############################################################################

# Datafiles
DATA = "../data/~LWI-Cd-rigid-Re-model.csv"

# CSV input
set datafile separator comma

# Plot options
set terminal pdfcairo monochrome enhanced dashed size 5.2,2.3 font "NimbusRomNo9L-Regu,18" fontscale 0.4
set xlabel "Measured, {/NimbusRomNo9L-ReguItal F} (N)" offset 0,0.3 font ",20"
set ylabel "Predicted, {/NimbusRomNo9L-ReguItal F} (N)" offset 1.5,0 font ",20"
set xrange [0.1:10]
set yrange [0.1:10]
set xtics scale 0.5
set ytics scale 0.5
set logscale xy
set key bottom Left reverse box lw 0.5 height 0.3 width 1 samplen 2 font ",16"
set bmargin 2.75

set style data points
set pointsize 0.6

set output "../images/LWI-Cd-rigid-Re-model.pdf"
set multiplot layout 1,2

# Functions
f(x) = x


###############################################################################
# PARTIALLY SUBMERGED
###############################################################################

set label 1 "(a)" at graph 0.09,0.91 center

plot	DATA index "WN_h12" u 1:2 pt 1 t "WN", \
		DATA index "PN_h12" u 1:2 pt 4 t "PN", \
		DATA index "PA_h12" u 1:2 pt 6 t "PA", \
		f(x) w lines lt 2 t "1:1"


###############################################################################
# FULLY SUBMERGED
###############################################################################

set label 1 "(b)"

plot	DATA index "WN_h25" u 1:2 pt 1 t "WN", \
		DATA index "PN_h25" u 1:2 pt 4 t "PN", \
		DATA index "PA_h25" u 1:2 pt 6 t "PA", \
		f(x) w lines lt 2 t "1:1"
