#!/usr/bin/env gnuplot

###############################################################################
# SETTINGS
###############################################################################

# Datafiles
FU_DATA = "../data/F-vs-U.csv"
PH_DATA = "../data/physical-properties.csv"
TMP = "tmp.dat"
FOL_RESULTS = "../data/~Cd-rigid-vs-Re-fol.csv"
DEFOL_RESULTS = "../data/~Cd-rigid-vs-Re-defol.csv"

# CSV input
set datafile separator comma
set macro

# Functions
RHO = 1000.0
NU = 1.3E-6
Re(U) = U * sqrt(AP) / NU
Cd(U,F) = 2.0 * F / (RHO * AP * U**2)

# Loop through all specimens
data_str_fol = ""
data_str_defol = ""
do for [i=1:156:5] {

	# Get specimen name
	SPECIMEN = system(sprintf("awk -F, 'NR == 1 {print $%i}' %s", i, FU_DATA))

	###########################################################################
	# FOLIATED
	###########################################################################

	# Get projected area
	AP = real(system(sprintf("awk -F, '$1 == \"%s\" {print $4}' %s | sed 's/NULL/NaN/'", SPECIMEN, PH_DATA)))

	# Ignore data sets with no valid points
	if (AP > 0 && SPECIMEN ne "P1") {
		# Calculate Re and Cd
		set table TMP
			plot FU_DATA u (Re(column(i))):(Cd(column(i),column(i+1)))
		unset table

		# Format data
		data_str_fol = data_str_fol . system(sprintf("awk 'BEGIN {print \"# %s\\n# Re (-),Cd (-)\"} $3 == \"i\" {printf (\"%%.3e,%%.4f\\n\", $1, $2)} END {print \"\\n\\n\"}' %s", SPECIMEN, TMP))
	}


	###########################################################################
	# DEFOLIATED
	###########################################################################

	# Get projected area
	AP = real(system(sprintf("awk -F, '$1 == \"%s\" {print $5}' %s | sed 's/NULL/NaN/'", SPECIMEN, PH_DATA)))

	# Ignore data sets with no valid points
	if (AP > 0 && SPECIMEN ne "A2" && \
		SPECIMEN ne "P2" && SPECIMEN ne "P4" && \
		SPECIMEN ne "S5" && SPECIMEN ne "S6" && SPECIMEN ne "S7") {
		# Calculate Re and Cd
		set table TMP
			plot FU_DATA u (Re(column(i))):(Cd(column(i),column(i+2)))
		unset table

		# Format data
		data_str_defol = data_str_defol . system(sprintf("awk 'BEGIN {print \"# %s\\n# Re (-),Cd (-)\"} $3 == \"i\" {printf (\"%%.3e,%%.4f\\n\", $1, $2)} END {print \"\\n\\n\"}' %s", SPECIMEN, TMP))
	}
}

# Write data to file and tidy up
system(sprintf("echo '%s' > %s", data_str_fol, FOL_RESULTS))
system(sprintf("echo '%s' > %s", data_str_defol, DEFOL_RESULTS))
system(sprintf("[ -e %s ] && rm %s", TMP, TMP))
