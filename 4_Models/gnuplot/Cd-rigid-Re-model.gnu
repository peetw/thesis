#!/usr/bin/env gnuplot

###############################################################################
# SETTINGS
###############################################################################

# Datafiles
FU_DATA = "../data/F-vs-U.csv"
PH_DATA = "../data/physical-properties.csv"
TMP = "tmp.dat"
FOL_RESULTS = "../data/~Cd-rigid-Re-model-fol.csv"
DEFOL_RESULTS = "../data/~Cd-rigid-Re-model-defol.csv"

# CSV input
set datafile separator comma
set macro

# Functions
RHO = 1000.0
NU = 1.3E-6
Re(U) = U * sqrt(AP) / NU
Cd(U) = (Re(U) <= Ret ? Cd0 : alpha * Re(U)**p)
F(U) = 0.5 * RHO * Cd(U) * AP * U**2

error(meas,pred) = 100 * abs(meas - pred) / meas

# Loop through all specimens
data_str_fol = ""
data_str_defol = ""
do for [i=1:156:5] {

	# Get specimen name
	SPECIMEN = system(sprintf("awk -F, 'NR == 1 {print $%i}' %s", i, FU_DATA))

	###########################################################################
	# FOLIATED
	###########################################################################

	# Get projected area
	AP = real(system(sprintf("awk -F, '$1 == \"%s\" {print $4}' %s | sed 's/NULL/NaN/'", SPECIMEN, PH_DATA)))

	# Ignore data sets with no valid points
	if (AP > 0 && SPECIMEN ne "P1") {
		# Set model params
		S = substr(SPECIMEN,1,1)
		Cd0 = (S eq "A" ? 0.994 : (S eq "P" ? 0.756 : 0.891))
		Ret = (S eq "A" ? 9E4 : (S eq "P" ? 9E4 : 1E5))
		alpha = (S eq "A" ? 1.64E3 : (S eq "P" ? 2.94E2 : 2E3))
		p = (S eq "A" ? -0.661 : (S eq "P" ? -0.54 : -0.671))

		# Calculate predicted drag force and percentage error
		set table TMP
			splot FU_DATA u (column(i+1)):(F(column(i))):(error(column(i+1),F(column(i))))
		unset table

		# Format data
		data_str_fol = data_str_fol . system(sprintf("awk 'BEGIN {print \"# %s\\n# F_meas (N),F_pred (N),err (%%)\"} $4 == \"i\" {printf(\"%%.2f,%%.2f,%%.2f\\n\", $1, $2, $3)} END {print \"\\n\\n\"}' %s", SPECIMEN, TMP))
	}


	###########################################################################
	# DEFOLIATED
	###########################################################################

	# Get projected area
	AP = real(system(sprintf("awk -F, '$1 == \"%s\" {print $5}' %s | sed 's/NULL/NaN/'", SPECIMEN, PH_DATA)))

	# Ignore data sets with no valid points
	if (AP > 0 && SPECIMEN ne "A2" && \
		SPECIMEN ne "P2" && SPECIMEN ne "P4" && \
		SPECIMEN ne "S5" && SPECIMEN ne "S6" && SPECIMEN ne "S7") {
		# Set model params
		S = substr(SPECIMEN,1,1)
		Cd0 = (S eq "A" ? 0.752 : (S eq "P" ? 0.984 : 1.255))
		Ret = (S eq "A" ? 9E4 : (S eq "P" ? 1.6E5 : 8E4))
		alpha = (S eq "A" ? 9.03E2 : (S eq "P" ? 1.61E5 : 2.19E3))
		p = (S eq "A" ? -0.605 : (S eq "P" ? -0.989 : -0.66))

		# Calculate predicted drag force and percentage error
		set table TMP
			splot FU_DATA u (column(i+2)):(F(column(i))):(error(column(i+2),F(column(i))))
		unset table

		# Format data
		data_str_defol = data_str_defol . system(sprintf("awk 'BEGIN {print \"# %s\\n# F_meas (N),F_pred (N),err (%%)\"} $4 == \"i\" {printf(\"%%.2f,%%.2f,%%.2f\\n\", $1, $2, $3)} END {print \"\\n\\n\"}' %s", SPECIMEN, TMP))
	}
}

# Write data to file and tidy up
system(sprintf("echo '%s' > %s", data_str_fol, FOL_RESULTS))
system(sprintf("echo '%s' > %s", data_str_defol, DEFOL_RESULTS))
system(sprintf("[ -e %s ] && rm %s", TMP, TMP))
