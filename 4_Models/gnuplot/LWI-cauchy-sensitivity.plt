#!/usr/bin/env gnuplot

###############################################################################
# SETTINGS
###############################################################################

# Datafiles
DATA = "../data/~LWI-cauchy-sensitivity.csv"
VG_DATA = "../data/~vogel-exponents.csv"
CD_DATA = "../data/~Cd-rigid.csv"

CNTRS = "contours.dat"

# Use macros
set macro

# Plot options
set terminal pdfcairo monochrome enhanced dashed size 5.2,6.6 font "NimbusRomNo9L-Regu,18" fontscale 0.4
set xlabel "{/NimbusRomNo9L-ReguItal C@^{*}_{d}}_0 (-)" offset 0,0.5 font ",20"
set ylabel "{/rtxmi ψ} (-)" offset 1,0 font ",20"
set xrange [0.25:1.5]
set yrange [-1.25:0]
set xtics 0.25 scale 0.5
set ytics 0.25 scale 0.5
unset key
set bmargin 2.75
unset surface
set contour
set cntrparam levels increment 0,10
set cntrparam levels 50

set label 1 at graph 0.09,0.91 center front
set object 1 circle size 0.015 fs solid fc rgb 'black' front

set output "../images/LWI-cauchy-sensitivity.pdf"
set multiplot layout 3,2


# Contour variables
xi = 0.25; xa = 1.5; yi = -1.25; ya = 0;
xl = xi + 0.15*(xa-xi); xh = xa - 0.3*(xa-xi);
yl = yi + 0.15*(ya-yi); yh = ya - 0.22*(ya-yi);
x0 = xi - (xa-xi); y0 = yi - (ya-yi)

# Contour functions
g(x,y) = ((x > xl && x < xh && y > yl && y < yh ? (x0 = x, y0 = y) : NaN), NaN)
ws(x,y) = (x == x0 && y == y0 ? y : NaN)
lab(x,y) = (x == x0 && y == y0 ? stringcolumn(3) : "")

# Contour macros
PLOT = "\"CNTRS index %i u 1:2 w lines lt 1 lw 1.5 lc rgb 'grey'\""
SEARCH = '"CNTRS index %i u (g($1,$2))"'
SPACE = "\"CNTRS index %i u 1:(ws($1,$2)) w points pt 5 ps 1.5 lc rgb 'white'\""
LABEL = "\"CNTRS index %i u 1:2:(lab($1,$2)) w labels tc rgb 'grey50' font ',16'\""


###############################################################################
# NATURAL WILLOW
###############################################################################

########################################
# PARTIALLY SUBMERGED
########################################

# Calculate contour lines
set datafile separator comma
set table CNTRS
	splot DATA u 1:2:3
unset table

# Get CD and E values used in actual model and idicate on plot
stats VG_DATA index 2 u 4 name "Vogel" nooutput
stats [*:*] [*:*] CD_DATA index 2 u 2 name "Cd" nooutput
set object 1 center Cd_mean,Vogel_mean

# Get number of contour blocks
set datafile separator whitespace
stats CNTRS nooutput
num_blocks = STATS_blocks - 1

# Add label
set label 1 "(a)"

# Plot contours with labels
PLOT_STR = ""
do for [i = 0:num_blocks] {
	PLOT_STR = PLOT_STR . sprintf(@PLOT, i)
	if (i > num_blocks-4 || (i+1)%2 && i > num_blocks-8) {
		PLOT_STR = PLOT_STR . ", " . sprintf(@SEARCH, i)
		PLOT_STR = PLOT_STR . ", " . sprintf(@SPACE, i)
		PLOT_STR = PLOT_STR . ", " . sprintf(@LABEL, i)
	}
	if (i != num_blocks) {
		PLOT_STR = PLOT_STR . ", "
	}
}
plot @PLOT_STR


########################################
# FULLY SUBMERGED
########################################

# Calculate contour lines
set datafile separator comma
set table CNTRS
	splot DATA u 1:2:4
unset table

# Get CD and E values used in actual model and idicate on plot
stats VG_DATA index 2 u 4 name "Vogel" nooutput
stats [*:*] [*:*] CD_DATA index 2 u 2 name "Cd" nooutput
set object 1 center Cd_mean,Vogel_mean

# Get number of contour blocks
set datafile separator whitespace
stats CNTRS nooutput
num_blocks = STATS_blocks - 1

# Add label
set label 1 "(b)"

# Plot contours with labels
PLOT_STR = ""
do for [i = 0:num_blocks] {
	PLOT_STR = PLOT_STR . sprintf(@PLOT, i)
	if (i > num_blocks-4 || (i+1)%3 && i > num_blocks-6) {
		PLOT_STR = PLOT_STR . ", " . sprintf(@SEARCH, i)
		PLOT_STR = PLOT_STR . ", " . sprintf(@SPACE, i)
		PLOT_STR = PLOT_STR . ", " . sprintf(@LABEL, i)
	}
	if (i != num_blocks) {
		PLOT_STR = PLOT_STR . ", "
	}
}
plot @PLOT_STR


###############################################################################
# NATURAL POPLAR
###############################################################################

########################################
# PARTIALLY SUBMERGED
########################################

# Calculate contour lines
set datafile separator comma
set table CNTRS
	splot DATA u 1:2:5
unset table

# Get CD and E values used in actual model and idicate on plot
stats VG_DATA index 1 u 4 name "Vogel" nooutput
stats [*:*] [*:*] CD_DATA index 1 u 2 name "Cd" nooutput
set object 1 center Cd_mean,Vogel_mean

# Get number of contour blocks
set datafile separator whitespace
stats CNTRS nooutput
num_blocks = STATS_blocks - 1

# Add label
set label 1 "(c)"

# Plot contours with labels
PLOT_STR = ""
do for [i = 0:num_blocks] {
	PLOT_STR = PLOT_STR . sprintf(@PLOT, i)
	if (i > num_blocks-1 || (i+1)%2 && i > num_blocks-7) {
		PLOT_STR = PLOT_STR . ", " . sprintf(@SEARCH, i)
		PLOT_STR = PLOT_STR . ", " . sprintf(@SPACE, i)
		PLOT_STR = PLOT_STR . ", " . sprintf(@LABEL, i)
	}
	if (i != num_blocks) {
		PLOT_STR = PLOT_STR . ", "
	}
}
plot @PLOT_STR


########################################
# FULLY SUBMERGED
########################################

# Calculate contour lines
set datafile separator comma
set table CNTRS
	splot DATA u 1:2:6
unset table

# Get CD and E values used in actual model and idicate on plot
stats VG_DATA index 1 u 4 name "Vogel" nooutput
stats [*:*] [*:*] CD_DATA index 1 u 2 name "Cd" nooutput
set object 1 center Cd_mean,Vogel_mean

# Get number of contour blocks
set datafile separator whitespace
stats CNTRS nooutput
num_blocks = STATS_blocks - 1

# Add label
set label 1 "(d)"

# Plot contours with labels
PLOT_STR = ""
do for [i = 0:num_blocks] {
	PLOT_STR = PLOT_STR . sprintf(@PLOT, i)
	if (i > num_blocks-2 || (i+1)%2 && i > num_blocks-8) {
		PLOT_STR = PLOT_STR . ", " . sprintf(@SEARCH, i)
		PLOT_STR = PLOT_STR . ", " . sprintf(@SPACE, i)
		PLOT_STR = PLOT_STR . ", " . sprintf(@LABEL, i)
	}
	if (i != num_blocks) {
		PLOT_STR = PLOT_STR . ", "
	}
}
plot @PLOT_STR


###############################################################################
# ARTIFICIAL POPLAR
###############################################################################

########################################
# PARTIALLY SUBMERGED
########################################

# Calculate contour lines
set datafile separator comma
set table CNTRS
	splot DATA u 1:2:7
unset table

# Get CD and E values used in actual model and idicate on plot
stats VG_DATA index 1 u 4 name "Vogel" nooutput
stats [*:*] [*:*] CD_DATA index 1 u 2 name "Cd" nooutput
set object 1 center Cd_mean,Vogel_mean

# Get number of contour blocks
set datafile separator whitespace
stats CNTRS nooutput
num_blocks = STATS_blocks - 1

# Add label
set label 1 "(e)"

# Plot contours with labels
PLOT_STR = ""
do for [i = 0:num_blocks] {
	PLOT_STR = PLOT_STR . sprintf(@PLOT, i)
	if (i > num_blocks-3 || (i+1)%2 && i > num_blocks-8) {
		PLOT_STR = PLOT_STR . ", " . sprintf(@SEARCH, i)
		PLOT_STR = PLOT_STR . ", " . sprintf(@SPACE, i)
		PLOT_STR = PLOT_STR . ", " . sprintf(@LABEL, i)
	}
	if (i != num_blocks) {
		PLOT_STR = PLOT_STR . ", "
	}
}
plot @PLOT_STR


########################################
# FULLY SUBMERGED
########################################

# Calculate contour lines
set datafile separator comma
set table CNTRS
	splot DATA u 1:2:8
unset table

# Get CD and E values used in actual model and idicate on plot
stats VG_DATA index 1 u 4 name "Vogel" nooutput
stats [*:*] [*:*] CD_DATA index 1 u 2 name "Cd" nooutput
set object 1 center Cd_mean,Vogel_mean

# Get number of contour blocks
set datafile separator whitespace
stats CNTRS nooutput
num_blocks = STATS_blocks - 1

# Add label
set label 1 "(f)"

# Plot contours with labels
PLOT_STR = ""
do for [i = 0:num_blocks] {
	PLOT_STR = PLOT_STR . sprintf(@PLOT, i)
	if (i > num_blocks-3 || (i+1)%2 && i > num_blocks-9) {
		PLOT_STR = PLOT_STR . ", " . sprintf(@SEARCH, i)
		PLOT_STR = PLOT_STR . ", " . sprintf(@SPACE, i)
		PLOT_STR = PLOT_STR . ", " . sprintf(@LABEL, i)
	}
	if (i != num_blocks) {
		PLOT_STR = PLOT_STR . ", "
	}
}
plot @PLOT_STR


# Tidy up
system(sprintf("[ -e %s ] && rm %s", CNTRS, CNTRS))
