#!/usr/bin/env gnuplot

###############################################################################
# SETTINGS
###############################################################################

# Datafiles
FU_DATA = "../data/F-vs-U.csv"
PH_DATA = "../data/physical-properties.csv"
VG_DATA = "../data/~vogel-exponents.csv"
CD_DATA = "../data/~Cd-rigid.csv"

RESULTS = "../data/~jarvela04-model.csv"

TMP = "tmp.dat"
HEADER = "# F_meas (N),F_pred (N),err (%)"

# CSV input
set datafile separator comma

# Functions for data fitting
RHO = 1000
Umin = 0.125

F(U) = 0.5 * RHO * CD * AL * (U/Umin)**E * U**2

error(meas,pred) = 100 * abs(meas - pred) / meas


###############################################################################
# FOLIATED
###############################################################################

# Species-averaged Vogel exponents
stats VG_DATA index 0 u 4 name "ALNUS_VOGEL" nooutput
stats VG_DATA index 1 u 4 name "POPULUS_VOGEL" nooutput
stats VG_DATA index 2 u 4 name "SALIX_VOGEL" nooutput

# Species-averaged drag coefficients
stats CD_DATA index 0 u 2 name "ALNUS_CD" nooutput
stats CD_DATA index 1 u 2 name "POPULUS_CD" nooutput
stats CD_DATA index 2 u 2 name "SALIX_CD" nooutput

# Calculate model error
set table RESULTS
do for [i = 1:156:5] {
	# Get specimen name
	S = system(sprintf("awk -F, 'NR == 1 {print $%i; exit}' %s", i, FU_DATA))

	# Only run for specimens without missing data
	if (S eq "A1" || S eq "A2" || S eq "A4" || S eq "A5" || \
		S eq "P2" || S eq "P3" || S eq "P4" || \
		S eq "S4" || S eq "S7") {
		# Get physical properties
		H = real(system(sprintf("awk -F, '$1 == \"%s\" {print $2; exit}' %s", S, PH_DATA)))
		AL = real(system(sprintf("awk -F, '$1 == \"%s\" {print $3; exit}' %s", S, PH_DATA)))

		# Get Vogel exponent and rigid drag coefficient
		E = (substr(S,1,1) eq "A" ? ALNUS_VOGEL_mean : (substr(S,1,1) eq "P" ? POPULUS_VOGEL_mean : SALIX_VOGEL_mean))
		CD = (substr(S,1,1) eq "A" ? ALNUS_CD_mean : (substr(S,1,1) eq "P" ? POPULUS_CD_mean : SALIX_CD_mean))

		# Calculate model error
		splot FU_DATA u i+1:(F(column(i))):(error(column(i+1),F(column(i)))) t S
	}
}
unset table

# Format output file
system(sprintf("awk '{prev[NR] = $4; if ($2 == \"Curve\") {name = $4; gsub(/\"/, \"\", name)} else if ($4 == \"i\") {if (prev[NR-1] == \"z\") {if (NR == 7) {print \"# \"name\"\\n%s\"} else {print \"\\n\\n# \"name\"\\n%s\"}} printf(\"%%.2f,%%.2f,%%.2f\\n\", $1, $2, $3)}}' %s >> %s && mv %s %s", HEADER, HEADER, RESULTS, TMP, TMP, RESULTS))
