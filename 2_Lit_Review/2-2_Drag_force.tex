%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Drag Force Theory}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

When there is relative motion between a fluid and an immersed body, the fluid will exert a force on that object. For flows over a plane surface parallel to the direction of the free stream, such as a thin flat plate, the force is due to the shear stress caused by the viscous interaction between the fluid and the surface. This is usually known as the skin friction drag. In the case where the surface of the body is not completely parallel to the direction of the flow, there is an additional drag force arising from changes in pressure over the surface. This is termed the pressure drag. It is also referred to as the form drag since the magnitude of the force is dependent on the shape of the object. The total drag $F$, often termed the profile drag, is therefore the sum of these two forces:
%
\begin{equation}
F = F_S + F_P
\label{eq:total-drag}
\end{equation}
%
where $F_S$ is the drag due to skin friction and $F_P$ is the drag due to pressure.
\nomenclature[ff]{$F_S$}{Skin friction drag}
\nomenclature[ff]{$F_P$}{Pressure drag}
\nomenclature[ff]{$F$}{Total drag force}

The relative proportion of friction and pressure drag that accounts for the total drag is dependent on the shape of the object and its orientation in the fluid stream. For example, a thin flat plate held parallel to the flow, as shown in Fig.~\ref{fig:streamlined-vs-bluff}a, will experience a drag force that is almost entirely due to skin friction. On the other hand, if the same plate is rotated so that its surface is perpendicular to the flow, as shown in Fig.~\ref{fig:streamlined-vs-bluff}b, the skin friction will become negligible and the pressure drag will dominate.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.75\textwidth]{Streamlined_vs_bluff_(Douglas_2005)}
\caption{Streamlines for: (a) streamlined object; (b) bluff object. Reproduced from \citet[pp. 398]{Douglas-05}.}
\label{fig:streamlined-vs-bluff}
\end{figure}

Although $F_S$ and $F_P$ in Eq.~\eqref{eq:total-drag} can be determined analytically by integrating around the surface of the body, this requires detailed knowledge of the distribution of pressure and shear stress around the object. Therefore, in practice the total drag force $F$ is usually determined experimentally and is related to certain parameters via the following expression:
%
\begin{equation}
F = \frac{1}{2} \rho C_d A u^2
\label{eq:drag-classical}
\end{equation}
%
where $\rho$ is the density of the fluid; $C_d$ is the dimensionless drag coefficient; $A$ is the reference area of the body; and $u$ is the reference velocity.
\nomenclature[zzzr]{$\rho$}{Density}
\nomenclature[ccd]{$C_d$}{Drag coefficient}
\nomenclature[aap]{$A_p$}{Projected area}
\nomenclature[uu]{$U$}{Mean flow velocity}

In experiments with cylinders and investigations into plant drag, the reference area is usually taken as the projected area $A_p$ of the body perpendicular to the flow, while the reference velocity corresponds to the free stream velocity, typically denoted as $u_m$ or $U$. It can be seen that for cases where the reference area and drag coefficient are constant, such as a bluff inflexible body in a flow of relatively constant Reynolds number, the drag force follows a quadratic relationship with velocity.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Drag Coefficient}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The equation used to determine the total drag (Eq.~\ref{eq:drag-classical}) is based on an idealized situation where the fluid comes to a complete stop over the projected area of the body and the force on the rigid or bluff object is completely due to stagnation pressure over the whole front area. Since no real object exactly corresponds to this behaviour, a drag coefficient $C_d$ is introduced. This drag coefficient is the ratio of drag for any real object to that of the ideal object and incorporates the effects of both skin friction and pressure drag. For instance, the ideal plate shown in Fig.~\ref{fig:ideal-plate} would have a drag coefficient of 1, while the actual plate, depicted in Fig.~\ref{fig:real-plate}, experiences additional drag due to a suction pressure on the rear face as the fluid flows around it and subsequently has a higher drag coefficient of around 1.15 \citep{Shames-03}.

\begin{figure}[htbp]
\centering
\setkeys{Gin}{width=0.35\textwidth}
\subfloat[Ideal]{\label{fig:ideal-plate}\includegraphics{Cd_flat_plate_a}}
\hfill
\subfloat[Actual]{\label{fig:real-plate}\includegraphics{Cd_flat_plate_b}}
\caption{Stagnation pressure for flow around: (a) an idealized plate; (b) a real plate.}
\label{fig:stagnation-pressure}
\end{figure}

The value of the drag coefficient for a particular object is determined experimentally using Eq.~\eqref{eq:drag-classical}:
%
\begin{equation}
C_d = \frac{F}{\frac{1}{2} \rho A_p U^2}
\label{eq:Cd-classical}
\end{equation}

Since the drag coefficient is the ratio of two forces, it is the same for two dynamically similar flows and is, therefore, independent of the size of the object (although not of its shape) and is a function of the Reynolds number $Re$. Fig.~\ref{fig:Cd-vs-Re} illustrates how the value of the drag coefficient varies with the Reynolds number for a selection of two-dimensional bodies; the thick line represents an infinitely long, smooth circular cylinder held with its long axis perpendicular to the flow.
\nomenclature[rr]{$Re$}{Reynolds number}

\begin{figure}[htbp]
\centering
\includegraphics[width=\textwidth]{Cd-vs-Re_(Massey_2006)}
\caption{Variation in drag coefficient with Reynolds number for two-dimensional objects. Reproduced from \citet[pp. 330]{Massey-06}.}
\label{fig:Cd-vs-Re}
\end{figure}
