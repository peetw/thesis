%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Vegetative Drag and Resistance} \label{sec:veg-drag}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The most widely utilized approach for representing the hydraulic effect of vegetation has been through the use of friction coefficients such as Manning's $n$ or Darcy-Weisbach's $f$. These combine multiple roughness effects, such as bed friction, vegetative drag, effects of channel cross-section, \etc, into a single term by utilizing the superposition principle (\eg \citealp{Cowan-56, Petryk-75, Yen-02}). Recent studies, however, have criticized the use of the dimensional Manning's $n$ coefficient \citep{Ferguson-10} and instead favoured the use of the dimensionless Darcy-Weisbach friction factor $f$ (\eg \citealp{Jarvela-02a, Fathi-07}).

Focusing on flow situations where only bed friction and vegetative drag are of significance, the total friction factor can be decomposed into a bed roughness factor $f'$ and a form resistance factor $f''$ according to the linear superposition principle, \ie $f = f' + f''$. In investigations with rigid vegetation and cylinders (\eg \citealp{Li-73, Huthoff-07}), the form factor $f''$ is related to the spatially-averaged drag force $\langle F \rangle$ via:
%
\begin{equation}
f'' = \frac{8 \langle F \rangle}{s_x s_y \rho U^2}
\label{eq:form-factor}
\end{equation}
%
where $s_x$ and $s_y$ are the longitudinal and lateral spacing of the roughness elements; $\rho$ is the density of the fluid; and $U$ is the reference velocity, typically taken as the free stream velocity.
\nomenclature[f]{$f'$}{Bed roughness factor}
\nomenclature[f]{$f''$}{Form factor}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Idealized Vegetation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In order to calculate the spatially-averaged drag force, the drag force on a single element must be known. In the case of rigid cylinders and other idealized vegetation, the classical drag force equation (Eq.~\ref{eq:drag-classical}) can be employed without modification. There have been numerous tests on isolated cylinders and other standard objects, and the variation in drag coefficient $C_d$ with flow Reynolds number $Re$ is well documented for such cases (see Fig.~\ref{fig:Cd-vs-Re}). At higher Reynolds numbers, the drag coefficient is essentially constant and Eq.~\eqref{eq:drag-classical} describes a quadratic relationship between drag force and velocity. Indeed, this quadratic drag force relationship has been found to be applicable for artificially stiffened vegetation \citep{Armanini-05, Aberle-12}.

Although the variation in drag coefficient with Reynolds number for isolated cylinders is well understood, the flow pattern around multiple cylinders quickly becomes very difficult to predict as the number of elements increases. As such, research into the flow resistance of idealized vegetation has primarily been focused on the bulk drag coefficient $C_D$ of arrays of cylinders (\eg \citealp{Lindner-82, Wu-99, Stone-02, Tang-12}).

An important experimental study undertaken by \citet{Nepf-99} investigated the link between the bulk drag coefficient and the density of emergent cylinder arrays. Both random and staggered arrays of rigid cylinders were tested under uniform flow conditions in a 24~m long by 38~cm wide flume. Data was also collated from a number of similar previous studies. It was found that the bulk drag coefficient decreased both with increasing array density, due to wake interference, and with increasing stem Reynolds number. A model was also proposed that described the interdependence of drag, turbulence and diffusion within arrays of emergent rigid vegetation. The model was validated against experimental data with array densities ranging from 0.01\% to 0.5\% and with stem Reynolds numbers up to \num{1e5}.

Recent studies have also found a similar trend, with the bulk drag coefficient tending to decrease with increasing stem Reynolds number \citep{Ishikawa-00, Tanino-08, Kothyari-09}. However, there appears to be some contradiction with regards to the relationship between the bulk drag coefficient and the density of the cylinder array. For example, a study performed by \citet{Tanino-08} found that the bulk drag coefficient increased with increasing array density, rather than decreased, for arrays of randomly distributed cylinders. The experiments were carried out under steady, non-uniform flow conditions with the arrays having solid volume fractions of 0.1 to 0.35 and with stem Reynolds numbers ranging from 25 to 685.

Although the experimental conditions in \citet{Tanino-08} were somewhat different to those in \citet{Nepf-99}, the results from recent studies (\eg \citealp{Ishikawa-00, Kothyari-09, Stoesser-10}) agree that the bulk drag coefficient tends to increase with increasing array density, even for similar experimental conditions. \citet{Aberle-13} postulated that the contradictory findings may be a result of experimental differences, such as the establishment of quasi- or non-uniform flow, the manner in which the drag force is measured (\ie direct drag force sensors or indirectly through velocity measurements), and neglecting the effect of wake interference on the spatial variability of the drag force within the array.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Flexible Vegetation} \label{sec:flexible-veg-drag}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

When modelling flexible or non-cylindrical vegetation, a common compromise is to idealize the vegetation into rigid cylinders, as discussed above. The plant or tree is then assigned an equivalent diameter and a value for the drag coefficient is taken from the literature. For example, one of the earliest experimental studies investigating the drag force response of flexible vegetation was carried out by \citet{Hirata-51} who used small live cuttings of plants in a wind tunnel to estimate drag coefficients for full-scale vegetation.

Additionally, \citet{Mayhead-73} analysed data previously collected from wind tunnel experiments in 1962 and 1967, in which mature conifers (from 6~m to 8~m in height) were tested over wind velocities ranging from 9.1~\mps to 38.3~\mps. The projected area of each tree in still air was determined both photographically and also from an estimate based on the length and diameter of the crown. Using the measured drag force and the projected area of the tree in still air, \citeauthor{Mayhead-73} was able to recommend drag coefficients for use in wind force uprooting predictions.

In a study of the damage caused to trees and forests due to wind loading, \citet{Papesch-77} measured the drag force and bending moment of natural trees positioned on the outer edges of two separate forests. The projected area in still air was determined through photographic analysis and was assumed to stay constant with increasing wind speed so that the variation in drag coefficient with wind velocity could be calculated for each tree.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Reconfiguration}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Using an idealized approach, where an initial value for the projected area and a simple relationship for the drag coefficient are utilized, ensures that the resulting calculations are trivial to perform; however, the modelled drag force is often much higher than would be expected. This is because such models do not take into account any changes to the vegetations' structure, and thus projected area, that can occur with increasing velocity \citep{Vogel-84}. It is claimed that this `reconfiguration' is an essential mechanism employed by vegetation in order to reduce the stress induced by an external flow \citep{Harder-04, Nikora-10}.

Indeed, many studies investigating the drag force exerted on flexible vegetation have found that this reconfiguration causes the measured force-velocity relationship to deviate away from a quadratic response, even at high Reynolds numbers. For example, \citet{Fathi-97} placed saplings of pine and cedar in an open channel flume and recorded the effect of reconfiguration on the drag coefficient and the momentum absorbing area at different flow depths and velocities. Using dimensional analysis and the experimental results, the authors devised a functional relationship between the physical parameters (plant density and flexural stiffness) and the flow conditions (depth and velocity). However, while the model provided relatively accurate predictions for the friction factor, it has not been widely adopted, perhaps due to the practical difficulty in quantifying the spatial distribution of the vegetation's momentum absorbing area.

The non-quadratic force-velocity relationship for flexible vegetation has also been observed in numerous similar studies (\eg \citealp{Oplatka-98, Freeman-00, Jarvela-04a, Sand-Jensen-08a, Wilson-08, Schoneboom-11, Siniscalchi-13}). Some researchers have suggested that the force-velocity relationship for flexible vegetation may, in fact, be characterized as linear at certain velocities \citep{Vischer-98, Sand-Jensen-03, Armanini-05, Cullen-05, Xavier-09}. However, where authors have proposed models for predicting the drag force of flexible vegetation, they have typically been based on empirical approaches and it is therefore unclear whether they would be applicable to vegetation of differing morphology or scale.

For example, the reconfiguration and hydrodynamic drag of flexible macro-algae was investigated by \citet{Boller-06}. The drag force, projected area, and shape of \textit{Chondrus crispus} specimens were recorded in a flume for velocities up to 2~\mps. The change in a specimen's projected area with increasing velocity was found to contribute more to the reduction of the drag force than the change in the drag coefficient. A empirical model for the drag force was developed which assumed that the reduction in projected area and drag coefficient followed an exponential decay. The exponential relationship was chosen by the authors as it was claimed to give the best correlation. However, while the drag force predicted by the model was closer to the measured force than the classical drag equation, there was still significant deviation from the measured force for the majority of specimens.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Vogel Exponent}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The extent to which the vegetation reconfigures and the drag force deviates from the squared velocity relationship (Eq.~\ref{eq:drag-classical}), has often been expressed in terms of a Vogel exponent $\psi$. The idea was first introduced by \citet{Vogel-84} and later studied by, for example, \citet{Gaylord-94}, \citet{OHare-07}, and \citet{Gosselin-11}. The Vogel exponent was originally devised in order to investigate the drag force response of vegetation in regions of large deformation, \ie above a certain threshold velocity.

In general terms and not considering the dependency of $C_d$ on $Re$, the Vogel exponent modifies the power to which the velocity is raised in the classical drag formula, so that:
%
\begin{equation}
F \propto U^{2+\psi}
\label{eq:vogel-exponent}
\end{equation}
%
with most reported values of $\psi$ ranging between $-0.2$ and $-1.2$ \citep{Langre-12}.
\nomenclature[zzzps]{$\psi$}{Vogel exponent}

For a rigid object or plant $\psi = 0$ and Eq.~\eqref{eq:vogel-exponent} returns to a classical squared relation, while a value of $\psi = -1$ would indicate a linear force-velocity relationship. \citet{Alben-02} and \citet{Langre-08} showed that for flexible fibres, scaling predicts an exponent of $\psi = -2/3$, with similar values reported for individual leaves \citep{Albayrak-12}. The dependence of the Vogel exponent on the flexural rigidity of the object was highlighted by \citet{Oplatka-98}, who tested full-scale trees in a flume and found that for a fully stiff tree $\psi = 0$, for a partially stiffened tree $\psi = -0.36$ and for a fully flexible tree $\psi = -1$.

The Vogel exponent concept is also represented in the work of \citet{Jarvela-04a}, who proposed that the form resistance factor $f''$ for just submerged vegetation is dependent on the leaf area index (LAI) and certain species-specific parameters:
%
\begin{equation}
f'' = 4 C_{d\chi} LAI \left(\frac{U}{U_\chi}\right)^\chi
\label{eq:form-factor-chi}
\end{equation}
%
where $C_{d\chi}$ is a species-specific drag coefficient; $\chi$ is also unique to a particular species and accounts for the reconfiguration of flexible vegetation; and $U_\chi$ is a scaling value included to ensure dimensional homogeneity, equal to the lowest velocity used in determining $\chi$.
\nomenclature[ccdx]{$C_{d\chi}$}{Species-specific drag coefficient}
\nomenclature[zzzx]{$\chi$}{Power equivalent to the Vogel exponent}
\nomenclature[uu]{$U_\chi$}{Scaling velocity}

Under the assumption that the LAI varies linearly with vegetation height, Eq.~\eqref{eq:form-factor-chi} can be extended to emergent vegetation by scaling the LAI term by the relative depth (\ie $H / \zeta \le 1$ where $H$ is the vegetation height and $\zeta$ is the water depth). It can also be found from rearranging Eqs.~\eqref{eq:form-factor} and \eqref{eq:form-factor-chi} that the $\chi$ value is equivalent to the Vogel exponent, \ie $F \propto U^{2+\chi}$ \citep{Aberle-13}.

The use of Eq.~\eqref{eq:form-factor-chi} has been prevalent in a number of recent studies. For example, \citet{Dittrich-12} measured the spatially-averaged drag force for arrays of submerged artificial plants in a 32~m long, 0.6~m wide and 0.4~m deep flume for velocities up to 0.8~\mps. The authors found that Eq.~\eqref{eq:form-factor-chi} accurately described the variation in $f''$ when using $\chi$ values of $-0.74$ for the staggered arrays and $-0.73$ for the in-line arrays.

The impact of varying the LAI was investigated by \citet{Jalonen-13a}, where the spacing and vertical leaf distribution of arrays of artificial plants were systematically modified. The spatially-averaged drag forces were recorded for just-submerged conditions and the velocities ranged from 0.11~\mps to 0.92~\mps. The experiments were carried out in the same flume as described in \citet{Dittrich-12}. The authors varied the LAI between 0.2 and 3.2 and found that the $f''$ could be described using a relatively constant value of $\chi \approx -0.9$, thus suggesting that the $\chi$ parameter is indeed species-specific.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Flexural Rigidity}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

While the studies listed above have shown that Eq.~\eqref{eq:form-factor-chi} may be a useful model for predicting the flow resistance of leafy vegetation, it remains to be seen whether the approach can be applied to defoliated vegetation since in such cases the LAI will be equal to zero. Additionally, Eq.~\eqref{eq:form-factor-chi} relies on an empirical scaling factor $U_\chi$ that must be determined experimentally and is not based on physical reasoning. Furthermore, such models do not explicitly include the vegetation's flexural rigidity, but rather incorporate it into the Vogel exponent.

As a result, it is difficult to accurately predict the Vogel exponent (or $\chi$ value) \textit{a priori}. Therefore, various studies have sought to decouple the flexural rigidity from the Vogel exponent by including it explicitly in the proposed drag force model. This is typically achieved via the addition of a dimensionless `vegetative' Cauchy number, as discussed by \citet{Langre-08}:
%
\begin{equation}
Ca = \frac{\rho U^2 S^3}{E}
\label{eq:cauchy-number-de-langre}
\end{equation}
%
where $\rho$ is the density of the fluid; $U$ is the flow velocity; $S$ is a `slenderness' number, defined as the ratio of the minimum to maximum cross-sectional dimensions; and $E$ is the modulus of elasticity.
\nomenclature[ccx]{$Ca$}{Cauchy number}
\nomenclature[ee]{$E$}{Modulus of elasticity}
\nomenclature[ss]{$S$}{Slenderness ratio}

One of the first studies to parameterize the relationship between reconfiguration and flexural rigidity in such a manner was undertaken by \citet{Gosselin-10b}, where the drag force for two simple geometries of flexible plates under aerodynamic loading was recorded. The authors performed a dimensional analysis and found that the force-velocity measurements collapsed onto a single curve when relating the magnitude of the reconfiguration to the scaled Cauchy number. In regions of large deformation the models accurately predicted that the flexible rectangular plate would have a Vogel exponent of $\psi = -2/3$ and that the flexible circular disk would have an exponent of $\psi = -4/3$.

This work was extended by \citet{Gosselin-11}, who measured the drag force exerted on rigid and flexible poroelastic systems in a wind tunnel. A similar dimensional analysis was performed and the force-velocity measurements were observed to collapse onto a single curve when the reconfiguration was related to the Cauchy number and the surface density. The authors found that as the surface density increased, the Vogel exponent in regions of large deformation transitioned from $\psi = -2/3$ to a relatively constant value of $\psi = -1$.

The Cauchy number approach was applied to real, albeit simplified, aquatic vegetation by \citet{Luhar-11}, where it was defined as:
%
\begin{equation}
Ca = \frac{\rho C_D U^2 l^3 b}{2 EI}
\label{eq:cauchy-number-luhar}
\end{equation}
%
where $l$ and $b$ were the length and breadth of the idealized beam, respectively.

Although the model, in conjunction with a buoyancy term, accurately predicted the reconfiguration and drag force of the specimens considered, it is unclear whether the definition of the Cauchy number would be applicable to full-scale vegetation, such as trees and other riparian vegetation, since it was based on an idealized slender beam.

In order to model the drag force exerted on vegetation with more complex morphology, \citet{Whittaker-13} replaced the length and breadth terms in Eq.~\eqref{eq:cauchy-number-luhar} by the vegetation's volume and height. The redefined Cauchy number was then incorporated into the classical drag equation (Eq.~\ref{eq:drag-classical}) via dimensional analysis to produce the following drag force model:
%
\begin{equation}
F = \frac{1}{2} \rho K \left(U\sqrt{\frac{\rho V H}{EI}}\right)^\psi U^2
\label{eq:drag-cauchy-old}
\end{equation}
%
where $K$ corresponds to an initial $C_d A_p$ value; $V$, $H$, and $EI$ are the vegetation's volume, height and flexural rigidity, respectively; $\psi$ is a species-specific Vogel exponent; and the terms within the parentheses represent the vegetative Cauchy number.
\nomenclature[hh]{$H$}{Vegetation height}
\nomenclature[ee]{$EI$}{Flexural rigidity}

The model (Eq.~\ref{eq:drag-cauchy-old}) was able to predict the drag force exerted on submerged full-scale riparian woodland trees (in both foliated and defoliated states) with fairly good accuracy once species-specific values for the Vogel exponent $\psi$ and linear relationships between $K$ and the tree's volume were determined. However, limited data availability meant that the model was unable to be validated against an independent data set so it remains to be seen whether such a model would be applicable to vegetation of a different scale or relative level of submergence.

Table~\ref{tab:drag-force-models} provides a summary of the drag force models currently available for flexible vegetation and their required parameters.

\begin{table}[htbp]
\caption{Summary of the currently available models for predicting the drag force exerted on flexible vegetation.}
\centering
\begin{footnotesize}
\begin{tabular}{p{3cm}llp{4cm}}
\toprule
{Model} & {Coefficients} & {Physical Properties} & {Notes} \\ \midrule
\citet{Kouwen-00} & $\alpha$, $\beta$ & $m$, $E$ & Applicable for non-submerged and foliated vegetation only \\
\citet{Jarvela-04a} & $C_{d\chi}$, $U_\chi$, $\chi$ & LAI, $H$ & Scaling velocity $U_\chi$ may not be theoretically correct \\
\citet{Xavier-09} & $C_d$, $U_t$, $\psi$ & $A_p$ & Assumes that drag force is linear above $U_t$ threshold velocity \\ 
\citet{Whittaker-13} & $K$, $\psi$ & $H$, $V$ & Replaces $C_d A_p$ term with $K$, which is determined via a linear relationship with volume \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:drag-force-models}
\end{table}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Numerical Modelling Studies}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Accompanying the experimental research detailed above, there have been numerous numerical modelling studies that have investigated the impact of vegetation on key flow properties. These studies can typically be split into two categories based on the scales at which the models operate, namely: the stem scale, where the interactions between the flow and vegetation at the individual plant level are modelled (Fig.~\ref{fig:stem-scale-model}); and the reach scale, where only the overall impact of vegetation on flow resistance is taken into account (Fig.~\ref{fig:reach-scale-model}).

\begin{figure}[htbp]
\centering
\setkeys{Gin}{width=0.45\textwidth}
\hfill{}
\subfloat[Stem-scale grid]{\label{fig:stem-scale-model}\includegraphics{model-stem-scale}}
\subfloat[Reach-scale grid]{\label{fig:reach-scale-model}\includegraphics{model-reach-scale}}
\hfill{}
\caption{Comparison of finite difference grid resolutions for: (a) stem-scale; and (b) reach-scale models. Individual plants or stems are represented as circles.}
\label{fig:model-scales}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Stem-scale Modelling}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In order to investigate local properties of the flow, such as turbulence and wake effects, the vegetation elements must be significantly larger than the individual grid cells used to discretize the flow field (Fig.~\ref{fig:stem-scale-model}). To model these complex phenomenon, three-dimensional (3D) large eddy simulations (LES) are often employed to solve the governing equations (see \autoref{sec:governing-eqs}).

For example, \citet{Stoesser-09} utilized LES to model the flow through submerged vegetation, as represented by idealized rigid cylinders. The authors compared the results to those recorded by an experimental study of the same set-up \citep{Liu-08} and found that the model accurately predicted the flow velocities and turbulence intensities. The model also correctly represented larger-scale flow structures, such as horseshoe and von K\'{a}rm\'{a}n vortices.

This approach was extended by \citet{Stoesser-10}, who used high-resolution LES to investigate the flow through different densities of emergent rigid cylinders. Again, there was found to be good agreement for the velocities and turbulence intensities between the model and the experimental measurements \citep{Liu-08}. More importantly, however, the high-resolution of the simulations allowed the authors to explicitly calculate the pressure and skin friction drag forces acting on the cylinders, without the need for empirical relationships. The findings suggest that flow resistance due to rigid vegetation increases with both planting density and stem Reynolds number.

Although the previous study showed that high-resolution LES can provide accurate predictions for the flow around rigid vegetation with minimal calibration, the computational cost is prohibitive. Therefore, \citet{Kim-11} investigated two, more computationally efficient, approaches. The first method incorporated the drag force as a sink term in the Reynolds-averaged Navier-Stokes (RANS) momentum equations (see Eq.~\ref{eq:RANS-x}), while the second utilized LES with a low-resolution grid (20 times coarser than that used by \citealp{Stoesser-10}).

The results from both models were compared to the experimental data of \citet{Tanino-08} and also the results from the high-resolution LES study of \citet{Stoesser-10}. The authors found that both models were able to accurately predict the observed velocities and turbulence intensities. However, the RANS model required careful calibration of the empirical drag coefficient, especially at higher vegetation densities. The authors also provided a summary of previous rigid cylinder modelling studies, along with the values that were used for the drag coefficient and any modifications that were made to the turbulence closure schemes.

Further study of the flow around vegetation at the stem-scale was carried out by \citet{Huai-11}, who used 3D LES to model emergent rigid cylinders in super- and sub-critical flows. The velocity profiles from the model were corroborated by experimental data and the authors found that the drag coefficient for the vegetation decreased with increasing Froude number.

In addition, \citet{Mattis-12} investigated the relationship of the drag coefficient with respect to the stem Reynolds number for flows through densely vegetated wetlands (idealized as rigid cylinders). This was achieved by modelling the flow using high-resolution LES. At lower Reynolds numbers, the well-established Darcy's Law was found to be applicable, while at higher Reynolds numbers two relationships were proposed: a modified Darcy-Forchheimer equation and a power law both provided reasonable approximations for the drag coefficient.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Reach-scale Modelling}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

While the stem-scale models are capable of directly determining the drag on idealized vegetation without the need for empirical relationships, they are not easily applied to flexible vegetation and are too computationally expensive to run over the reach scale, as required for effective flood risk management analysis. Therefore, modelling vegetation at the reach scale has typically relied on bulk friction coefficients, such as Manning's $n$, Chezy's $C$ or the Darcy-Weisbach factor $f$. However, the range of possible values for these coefficients is large (\eg \citealp{Chow-73, Acrement-89}) and choosing a correct value for vegetated regions requires considerable experience and calibration.

To overcome these limitations, the drag force is often included directly into the momentum equations that govern fluid flow (see \autoref{sec:governing-eqs}). This approach reduces calibration uncertainties and offers a more physically-based model for incorporating vegetation resistance than the traditional use of a bulk friction coefficient. For example, \citet{Fischer-Antze-01} modelled a number of partially vegetated channels with the addition of a drag force term and a $k$-$\epsilon$ turbulence model. Although the vegetation was idealized as rigid cylinders, the resulting velocity profiles were in good agreement with the experimental measurements.

\citet{Stoesser-03} used the method validated by the experimental studies of \citet{Fischer-Antze-01} and \citet{Lopez-01} to model a 100~year flood event on a 3.5~km stretch of the River Rhine, Germany. The river floodplains contained a mix of riparian woodland which were idealized as arrays of rigid cylinders. Good agreement in the mean flow velocities on the floodplains was found between the measured values and the results of the 3D modelling. It should be noted that the $k$-$\epsilon$ turbulence closure model utilized in the study was not modified to take into account the presence of vegetation since the drag force term was previously found to be dominant over the diffusive turbulent terms within emergent and submerged vegetation \citep{Fischer-Antze-01, Stoesser-02}.

The hydraulic resistance of two species of willow trees (\textit{Salix alba} and \textit{Salix fragilis}) was also investigated for a compound channel over a test reach of 170~m by \citet{Wilson-06b}. The authors directly included the drag force in the momentum equations and varied the parameterization of the trees' projected areas using uniform and non-uniform equivalent cylinder diameters and an empirical relationship with plant height. 3D modelling showed that the uniform parameterization resulted in relatively greater velocities in the region close to the bed and lower velocities in the upper region, compared to the non-uniform parameterization. This was particularly evident in the impact on bed shear stresses, where there was a 150\% relative difference between the two approaches.

This work was further extended by \citet{Wilson-06a}, who included the effects of reconfiguration via a simple angle of deflection model. The study demonstrated the need to include reconfiguration in drag force models and accurately predicted velocity profiles for a flood event in the same compound channel as the previous study. However, the reconfiguration model was not based on the physical processes and did not included key biomechanical properties, such as flexural rigidity.

More recently, \citet{Wang-11} evaluated the effect of submerged aquatic vegetation on the velocity profiles within a shallow lake. The drag force was included as a sink term in the momentum equations and the $k$-$\epsilon$ scheme was used to model the turbulence. The velocity profiles predicted by the model were found to be in good agreement with those recorded at the site. The authors also compared the model to a simplified version, where the vegetation was only represented as an increase in the bed roughness. This showed that the drag force approach was more than twice as accurate as the bed roughness approach.
