%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Governing Equations} \label{sec:governing-eqs}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Navier-Stokes Equations} \label{sec:governing-eqs-ns}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The velocity field of a fluid can generally be described by the Navier-Stokes equations, which can be derived by applying Newton's second law to the fluid, along with pressure gradients and assumptions about the viscous stresses. In most practical engineering problems, the fluid flow can be assumed to be incompressible and is usually characterized by turbulent fluctuations. As a result, the instantaneous Navier-Stokes equations are often time-averaged to provide the Reynolds-averaged Navier-Stokes equations \citep[\eg][]{Massey-06}:
%
\begin{subequations}
\begin{equation}
\frac{\partial u}{\partial t} + u\frac{\partial u}{\partial x} + v\frac{\partial u}{\partial y} + w\frac{\partial u}{\partial z} = X - \frac{1}{\rho}\frac{\partial P}{\partial x} + \frac{1}{\rho}\left(\frac{\partial \sigma_{xx}}{\partial x} + \frac{\partial \tau_{yx}}{\partial y} + \frac{\partial \tau_{zx}}{\partial z}\right)
\label{eq:RANS-x}
\end{equation}
\begin{equation}
\frac{\partial v}{\partial t} + u\frac{\partial v}{\partial x} + v\frac{\partial v}{\partial y} + w\frac{\partial v}{\partial z} = Y - \frac{1}{\rho}\frac{\partial P}{\partial y} + \frac{1}{\rho}\left(\frac{\partial \tau_{xy}}{\partial x} + \frac{\partial \sigma_{yy}}{\partial y} + \frac{\partial \tau_{zy}}{\partial z}\right)
\label{eq:RANS-y}
\end{equation}
\begin{equation}
\frac{\partial w}{\partial t} + u\frac{\partial w}{\partial x} + v\frac{\partial w}{\partial y} + w\frac{\partial w}{\partial z} = Z - \frac{1}{\rho}\frac{\partial P}{\partial z} + \frac{1}{\rho}\left(\frac{\partial \tau_{xz}}{\partial x} + \frac{\partial \tau_{yz}}{\partial y} + \frac{\partial \sigma_{zz}}{\partial z}\right)
\label{eq:RANS-z}
\end{equation}
\label{eq:RANS}
\end{subequations}

\noindent{}where $t$ is time; $u$, $v$, and $w$ are the velocity components in each of the principal Cartesian axes $x$, $y$, and $z$ respectively; $\rho$ is the fluid density; $P$ is the pressure; and the normal $\sigma$ and shear $\tau$ stresses can be described using Einstein notation:

\begin{equation}
\sigma_{ii} = \mu \frac{\partial u_i}{\partial x_i} - \rho \overline{u'_i u'_i}; \qquad \tau_{ij} = \mu \frac{\partial u_j}{\partial x_i} - \rho \overline{u'_i u'_j}
\label{eq:RANS-shear}
\end{equation}
%
where $\mu$ is the dynamic viscosity.
\nomenclature[zzzm]{$\mu$}{Dynamic viscosity}

The variables $X$, $Y$, and $Z$ represent external body forces and typically take the following values when considering gravity and the Coriolis effect:
%
\begin{subequations}
\begin{align}
X &=  2 \omega v \sin \phi \\
Y &= -2 \omega u \sin \phi \\
Z &= -g
\end{align}
\label{eq:RANS-body-forces}
\end{subequations}
%
where $\omega$ is the angular frequency of Earth ($\num{7.27e-5}$~\radians); $\phi$ is the latitude; and $g$ is the acceleration due to gravity ($9.81$~\acc).
\nomenclature[zzzo]{$\omega$}{Angular frequency of Earth's rotation}
\nomenclature[zzzf]{$\phi$}{Latitude}
\nomenclature[g]{$g$}{Acceleration due to gravity}

While the equations of \eqref{eq:RANS} describe the motion or momentum of a fluid, a fourth relationship is required to maintain the conservation of mass within a control volume \citep{Douglas-05}. This is known as the continuity equation:
%
\begin{equation}
\frac{\partial u}{\partial x} + \frac{\partial v}{\partial y} + \frac{\partial w}{\partial z} = 0
\label{eq:RANS-continuity}
\end{equation}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Turbulence Closure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

When supplied with sufficient boundary and initial conditions, the combined momentum and continuity equations (Eqs.~\ref{eq:RANS} and \ref{eq:RANS-continuity}) can be used in a wide variety of modelling applications, including aeronautics, oceanography, physics and hydraulics. However, before being of use, the turbulent or Reynolds stresses $-\rho \overline{u'_i u'_j}$ (Eq.~\ref{eq:RANS-shear}) must be modelled. This is because the Reynolds stresses are non-linear due to their fluctuating nature and therefore further assumptions and modelling are required in order to close the RANS equations \citep{Rodi-93}.

This has led to the creation of a number of different `turbulence-closure' models. These models introduce relationships between the mean motion of the fluid and the Reynolds stresses and thus provide the equations needed to solve for the extra unknowns (\ie the time-averaged product of the fluctuating velocities $\overline{u'_i u'_j}$).

The first step towards such a model was proposed by Boussinesq in 1877. In an analogy with the kinematic viscosity of a fluid and the molecular viscosity of a gas, he defined an eddy viscosity in order to relate the Reynolds stresses to the local mean velocity gradients. For incompressible flow, the Boussinesq hypothesis can be written using Einstein notation as \citep[\eg][]{Massey-06}:
%
\begin{equation}
- \overline{u'_i u'_j} = \nu_t \left(\frac{\partial \overline{u_i}}{\partial x_j} + \frac{\partial \overline{u_j}}{\partial x_i}\right) - \frac{2}{3} k \delta_{ij}
\label{eq:boussinesq}
\end{equation}
%
where $\nu_t$ is the turbulent eddy viscosity; $k = \frac{1}{2} \overline{u'_i u'_i}$ is the turbulent kinetic energy (TKE); and $\delta_{ij}$ is the Kronecker delta.

In early applications, the eddy viscosity had to be determined empirically and had a constant value. To address these issues Prandtl introduced the mixing length hypothesis in 1925, which assumes that the eddy viscosity is proportional to the turbulent velocity fluctuations and a `mixing length' \citep[\eg][]{Shames-03}.

For wall-bounded flows, the eddy viscosity must vary with the distance from the wall so that:
%
\begin{equation}
\nu_t = l_m^2 \left|\frac{\partial u}{\partial y}\right|
\end{equation}
%
where $y$ is the distance normal to the wall; and $l_m$ is the mixing length.

The mixing length depends on the nature of the flow and is a function of space. For example:
%
\begin{center}
\begin{tabular}{lll}
At a wall, \ie $y = 0$ & & $l_m = 0$ \\
Near a wall & & $l_m = \kappa y$ \\
Unbounded flow & & $l_m = C \delta$
\end{tabular}
\end{center}
%
where $\kappa$ is von Karman's constant ($\approx 0.41$); $C$ is a constant; and $\delta$ is the boundary layer thickness.

Prandtl's mixing length hypothesis is still in common use today. This is because calculations based on the model are easy to undertake, since no additional differential equations need to be solved. However, there are a number of limitations. For instance, where the flow is bounded by non-planar walls it is impossible to estimate the distribution of mixing lengths with acceptable accuracy \citep{Rodi-93}.

To overcome these problems many more turbulence closure models have been developed. These include the so called `zero-equation' or algebraic models, such as a depth-averaged or mixing length model, that do not solve any additional differential equations in order to predict the contributions of the turbulence \citep[\eg][]{Beffa-95, Wu-04, Defina-05}. Slightly more advanced are the `one-equation' models, such as Spalart-Allmaras (SA) and Goldberg, which calculate the eddy viscosity based on mean flow characteristics and empirical constants \citep[\eg][]{Spalart-94, Li-07, Li-10}. Similarly, there are a number of `two-equation' models (\eg $k$-$\epsilon$ and $k$-$\omega$) that solve a further differential equation for the transport of turbulent kinetic energy \citep[\eg][]{Lopez-01, Katul-04, Wu-04, King-12}.

Another popular closure model is that of large eddy simulation (LES), where the large-scale eddies within the flow are resolved numerically, while only the small-scale turbulent eddies are modelled \citep[\eg][]{Finnigan-09, Stoesser-09}. Also of note is direct numerical simulation (DNS), whereby the spatial and temporal resolutions of the computational domain are increased to such an extent that all the turbulent processes can be resolved \citep[\eg][]{Zhou-99, Breugem-06}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Shallow Water Equations} \label{sec:governing-eqs-swe}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Often in fluid dynamics the horizontal scale of the area under consideration is much greater than the vertical scale. For example, this occurs in harbours, estuaries and even oceans, where the ocean depth is negligible when compared to the horizontal lengths. Under these conditions, the continuity equation implies that the vertical velocity component $w$ is small in comparison to the horizontal components.

Integrating the Reynolds-averaged Navier-Stokes and continuity equations (Eqs.~\ref{eq:RANS} and \ref{eq:RANS-continuity}) over the flow depth removes the vertical velocity component. The resulting two-dimensional (2D) equations are known as the shallow water equations (SWE) and, once numerically solved, provide the depth averaged velocity field. The depth-averaged continuity and momentum components of the SWEs can be written in general form as:
%
\begin{subequations}
\begin{equation}
\frac{\partial \eta}{\partial t} + \frac{\partial q_x}{\partial x} + \frac{\partial q_y}{\partial y} = 0
\label{eq:swe-continuity}
\end{equation}
\begin{align}
\begin{split}
\frac{\partial q_x}{\partial t} + \frac{\partial \left(\beta q_x^2 / \zeta\right)}{\partial x}  + \frac{\partial \left(\beta q_x q_y / \zeta\right)}{\partial y} = X - \frac{\zeta}{\rho}\frac{\partial P_a}{\partial x} - g\zeta\frac{\partial \eta}{\partial x} \\- \frac{\tau_{bx}}{\rho} + \frac{\tau_{wx}}{\rho} + \nu_t \left(2\frac{\partial^2 q_x}{\partial x^2} + \frac{\partial^2 q_x}{\partial y^2} + \frac{\partial^2 q_y}{\partial x \partial y}\right)
\end{split}
\label{eq:swe-momentum-x}
\end{align}
\begin{align}
\begin{split}
\frac{\partial q_y}{\partial t} + \frac{\partial \left(\beta q_x q_y / \zeta\right)}{\partial x}  + \frac{\partial \left(\beta q_y^2 / \zeta\right)}{\partial y} = Y - \frac{\zeta}{\rho}\frac{\partial P_a}{\partial y} - g\zeta\frac{\partial \eta}{\partial y} \\- \frac{\tau_{by}}{\rho} + \frac{\tau_{wy}}{\rho} + \nu_t \left(\frac{\partial^2 q_y}{\partial x^2} + 2\frac{\partial^2 q_y}{\partial y^2} + \frac{\partial^2 q_x}{\partial x \partial y}\right)
\end{split}
\label{eq:swe-momentum-y}
\end{align}
\label{eq:swe}
\end{subequations}
%
where $t$ is time; $\eta$ is the water surface elevation; $\zeta = h + \eta$ is the total water depth; $q_x$ and $q_y$ are the discharges per unit width in the $x$ and $y$ directions, respectively; $\beta$ is the correction factor for non-uniform vertical velocity profiles, where $\beta = 1.016$ for a seventh power-law velocity distribution \citep[\eg][]{Shames-03}; $X$ and $Y$ are the depth-averaged body forces, such as the Coriolis force (see Eqs.~\ref{eq:RANS-body-forces}a and \ref{eq:RANS-body-forces}b); $P_a$ is the atmospheric pressure; $g$ is the acceleration due to gravity; $\tau_b$ and $\tau_w$ are the bed and wind shear stresses, respectively; and $\nu_t$ is the kinematic eddy viscosity (assuming that the kinematic viscosity of water is negligible in comparison).
\nomenclature[zzzi]{$\eta$}{Water surface elevation}
\nomenclature[zzzz]{$\zeta$}{Water depth}
\nomenclature[h]{$h$}{Bed elevation}
\nomenclature[q]{$q$}{Discharge per unit width}
\nomenclature[zzzb]{$\beta$}{Momentum correction factor}
\nomenclature[zzzt]{$\tau_b$}{Bed shear stress}
\nomenclature[zzzn]{$\nu_t$}{Kinematic eddy viscosity}

It should be noted that although a vertical velocity term is not explicitly included in the shallow water equations, the vertical velocity is not necessarily zero. Once the horizontal velocities and free surface level have been solved for, the vertical velocity can be recovered via the continuity equation. This is important since the vertical velocity cannot be zero where there is a change in the bed depth \citep{Liang-07c}.
