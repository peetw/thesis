%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Model Optimization} \label{sec:model-optimization}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The explicit TVD-MacCormack solution scheme detailed above is particularly computationally efficient, especially when compared to other implicit methods \citep{Liang-06a}. However, the original version of DIVAST-TVD may be considered as a naive implementation in terms of optimization. Therefore, three approaches are taken to improve program run time, namely: general serial optimizations on a single central processing unit (CPU); parallel processing on multiple CPU cores using shared memory; and parallel processing on a graphical processing unit (GPU). Combined, these optimizations enable simulations at a high spatial resolution over a large computational domain ($> \num{2e5}$ grid points) to be completed in a more manageable time-frame (hours versus days).

In addition to the following CPU and GPU optimizations, a number of improvements were made to the original code. While not strictly optimizations in the sense that they decreased program execution time, they none-the-less enhance the functionality and maintainability of the code. 

These `quality-of-life' improvements include converting the code from a mix of Fortran 77 and 95 with GNU extensions, to strict Fortran 2003. This ensured that the code would be fully portable across different compilers and hardware. It was checked using the \texttt{gfortran} compiler option \texttt{-std=f2003}. Pointer arithmetic and operations were also used where applicable in order to reduce the number of lines of code required for certain tasks.

The program was also moved from a single, monolithic source file to a more modular, distributed system. In this, each logical part of the program, such as file input and output routines, were assigned to separate modules and files. This results in easier comprehension of program flow and function and faster program compilation since only those files that have changed need to be re-compiled. Such a structure also allows additional modules to be quickly added and incorporated into the program.

Other minor changes include the use of \texttt{implicit none} throughout the code to reduce accidental programming errors. This was enforced using the \texttt{gfortran} compiler option \texttt{-fimplicit-none}. Stronger error checking and more useful error messages and exit codes were also introduced. Finally, the file input routines were modified so that the input data required to run a simulation is now split into multiple files containing various parameters. This results in a smaller overall project size since the larger and more constant data, such as the domain specification or digital elevation data, is no longer duplicated when creating alternate scenarios.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{CPU Optimization}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Serial Optimizations}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

When performing optimization of serial Fortran code, gains can easily be obtained by analysing the order in which nested loops are executed. For example, consider the two code snippets:

\begin{minipage}[b]{0.45\linewidth}
\begin{lstlisting}[caption={Row-major indexing},label=lst:row-major]
do i = 1, n
  do j = 1, n
    a(i,j) = b(i,j)
  end do
end do
\end{lstlisting}
\end{minipage}
\hfill
\begin{minipage}[b]{0.45\linewidth}
\begin{lstlisting}[caption={Column-major indexing},label=lst:col-major]
do j = 1, n
  do i = 1, n
    a(i,j) = b(i,j)
  end do
end do
\end{lstlisting}
\end{minipage}

Although the two listings are functionally identical, the swapped order of the outer loop in List.~\ref{lst:col-major} will enable more efficient use of the cache on the CPU. This is because Fortran stores arrays in linear memory using column-major order. When the program requests the value stored at a particular array location, the CPU will also pre-fetch contiguous memory addresses into the CPU cache. Therefore, accessing array elements by varying the left-most subscript with a stride of one will allow the CPU to reuse data already loaded into the cache and minimize the number of comparatively costly memory fetches.

Cache efficiency must also be considered when selecting the level of precision required for each value or array. For example, an array of 4 byte values will be traversed almost twice as fast as its 8 byte counterpart. This is due to the fact that twice as many 4 byte values can be stored in the CPU cache compared to 8 byte values.

Other miscellaneous improvements made to the code include moving loop counters to the outermost loop possible to reduce unnecessary updating; removing inefficient memory allocation and deallocation by pre-allocating memory during program initialization; and ensuring that the intent of each subroutine and function argument is explicitly declared so that the compiler can make suitable optimizations.

The impact of the aforementioned serial optimizations was determined by benchmarking the modified code against the original code. The hardware and software used in the benchmarking tests are detailed in Table~\ref{tab:benchmark-hardware-cpu}, while the model parameters for the test case are listed in Table~\ref{tab:benchmark-params}.

\begin{table}[htbp]
\caption{Hardware and software used to benchmark DIVAST-TVD code.}
\centering
\begin{footnotesize}
\begin{tabular}{lr}
\toprule
CPU model & Intel Core i5-2500 \\
CPU clock frequency & 3.3~GHz \\
CPU cores / threads & 4 / 4 \\
CPU cache (L1 / L2 / L3) & 64 / 256 / 6,144~kB \\ \midrule
Memory & 4~GB DDR3 \\
Memory clock frequency & 1,333~MHz \\ \midrule
Operating system & Linux 3.5.0-36 \\
Compiler & gfortran v4.7.3 \\
Compiler flags & \texttt{-O3 -mtune=native} \\
\bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:benchmark-hardware-cpu}
\end{table}

\begin{table}[htbp]
\caption{Model parameters used to benchmark DIVAST-TVD code.}
\centering
\begin{footnotesize}
\begin{tabular}{lr}
\toprule
Grid cells & 213,526 \\
Grid size & 1~m \\
Simulation time & 48~hrs \\
Time-step & $0.01 \leq \Delta t \leq 1$~s \\
\bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:benchmark-params}
\end{table}

The results of the initial benchmarking of the original and optimized serial codes are presented in Table~\ref{tab:benchmark-serial}. It can be seen that even with these relatively modest changes a speed-up of 1.62 is obtained. It should also be noted that the speed gains were achieved even though the optimized serial code performed slightly more computational work due to new features, such as the drag force and eddy viscosity models detailed above, being added.

\begin{table}[htbp]
\caption{Benchmarking results for serial DIVAST-TVD code. Speed-ups are calculated relative to original code.}
\centering
\begin{footnotesize}
\begin{tabular}{lcS[table-format=1.2]}
\toprule
 & Duration & \\ 
Code & (hh:mm:ss) & {Speed-up} \\ \midrule
Original & 22:41:21 & 1 \\
Serial & 14:00:20 & 1.62 \\
\bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:benchmark-serial}
\end{table}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Parallel Optimizations}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The shift in focus from serial to parallel applications has been motivated by the relative stagnation in the raw clock frequency of CPUs since the beginning of the 21\sps{st} century (Fig.~\ref{fig:CPU-speeds-vs-time}). This is mainly due to problems with heat dissipation and power consumption at high clock speeds. The number of transistors available per area is still currently following Moore's law, however, and the increase in transistor count has been invested into multiple cores on the same CPU microchip.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.75\textwidth]{CPU-speeds-vs-time}
\caption{Variation in CPU properties over time for the Intel range of CPUs. Reproduced from \cite{Sutter-05}.}
\label{fig:CPU-speeds-vs-time}
\end{figure}

Modern CPUs typically have 4 to 16 separate cores that can process instructions and data in parallel. In order to access that parallelism, program code must be modified to split up its work into chunks or `threads' that then execute individually and in parallel. This is usually implemented using a third-party application program interface (API). The two most common shared-memory APIs for Fortran are Pthreads and OpenMP. In the case of distributed systems, where multiple processing elements are connected by a network, a message passing API, such as MPI, is required.

In this study, the hardware used to benchmark the optimized codes consisted of a single workstation (Table~\ref{tab:benchmark-hardware-cpu}). Therefore, the system utilized shared memory and the choice of APIs was between Pthreads and OpenMP. The former provides a very low-level API for fine-grained control over individual threads, while the later provides a higher-level API. OpenMP was chosen for this study since it enables rapid development and ease of scalability with regards to thread count. Another advantage of OpenMP is that utilizes comment style pragmas to instruct the compiler and can thus be easily compiled into serial code if required (see List.~\ref{lst:openmp}). This ensures that the code remains portable across parallel and non-parallel architectures.

\begin{lstlisting}[caption={Example parallelization using OpenMP},label=lst:openmp,float=htbp]
!$omp parallel do private(i,j)
do j = 1, n
  do i = 1, n 
    a(i,j) = b(i,j)
  end do
end do
!$omp end parallel do
\end{lstlisting}

Although some routines in DIVAST-TVD, such as file input and output, cannot be parallelized, the explicit scheme used to solve the SWEs is particularly amenable to parallelization. However, the maximum speed-up of a program as a result of parallelization is governed by Amdahl's law:
%
\begin{equation}
S(N_p) = \frac{1}{\left(1 - P\right) + \frac{P}{N_p}}
\label{eq:amdahl-law}
\end{equation}
%
where $S$ is the speed-up; $N_p$ is the number of processors; and $P$ is the proportion of the program which can be parallelized.
\nomenclature[nn]{$N_p$}{Number of computational processors}

In the benchmarking tests carried out herein, four processors were available (Table~\ref{tab:benchmark-hardware-cpu}). Referring to Eq.~\eqref{eq:amdahl-law} and assuming that roughly 90\% of the DIVAST-TVD code is suitable for parallelization, the maximum achievable speed-up is 3.08. The speed-up compared to the original code, however, should be greater since the serial optimizations detailed above have increased the efficiency of the non-parallelizable sections of the code.

\begin{table}[htbp]
\caption{Benchmarking results for CPU parallelized DIVAST-TVD code. Speed-ups are calculated relative to original code.}
\centering
\begin{footnotesize}
\begin{tabular}{lcS[table-format=1.2]}
\toprule
 & Duration & \\ 
Code & (hh:mm:ss) & {Speed-up} \\ \midrule
Original & 22:41:21 & 1 \\
Serial & 14:00:20 & 1.62 \\
Parallel (CPU) & 06:21:45 & 3.57 \\
\bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:benchmark-parallel}
\end{table}

The results of the parallel optimizations are presented in Table~\ref{tab:benchmark-parallel}. It can be seen that the OpenMP parallelized model was over three and a half times faster than the original model and was 2.2 times quicker than the optimized serial code. The reason that the maximum theoretical speed-up of 3.08 over the optimized serial code was not obtained is likely due to the overhead of thread creation and work sharing, which can be significant for those loops where the computational effort is low.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{GPU Optimization}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In conjunction with the shift from serial to parallel processing, there has been a rise in general-purpose computing on graphics processing units (GPGPU). This is because modern GPUs often exceed the computational power of the CPU that instructs them, due to the massively parallel architecture demanded by complex 3D graphics (Fig.~\ref{fig:CPU-vs-GPU}). However, until recently, the power of such graphics cards was obscured by the need to translate generic computational problems into problems that could be rendered by the OpenGL graphics API.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.65\textwidth]{CPU-vs-GPU}
\caption{Comparison of theoretical floating-point operations (FLOP) per second between modern Intel CPUs and Nvidia GPUs. Reproduced from \citet{Nvidia-13}.}
\label{fig:CPU-vs-GPU}
\end{figure}

To simplify the process of utilizing a GPU's resources, Nvidia released the Compute Unified Device Architecture (CUDA) API for the C language in the spring of 2007. The CUDA API makes it significantly easier to interact with the GPU and provides the necessary tools to communicate between the CPU host and the GPU device.

While there are Fortran interfaces for the CUDA API, they are currently proprietary and therefore the entire DIVAST-TVD code base (excluding the file input/output routines) was translated into the C language to enable the use of the CUDA API in this study. The use of an open-source API, such as OpenCL, was not considered since the CUDA API is optimized specifically for Nvidia GPUs, whereas OpenCL is designed more towards portability. In CUDA terminology, the CPU is referred to as the host and the GPU is called the device. The layout of a CUDA-enabled GPU is shown schematically in Fig.~\ref{fig:CUDA-GPU}.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.7\textwidth]{CUDA-GPU}
\caption{Schematic of a CUDA-enabled GPU architecture. Reproduced from \citet{Nvidia-13}.}
\label{fig:CUDA-GPU}
\end{figure}

Each GPU device provides multiple streaming multiprocessors (SMs), that are themselves comprised of individual processors. Once data has been sent from the host to the device, it is stored in the device, or global, memory. Global memory is accessible by all the SMs and has the largest capacity. The SMs can also make use of faster, although smaller, blocks of shared memory that are only visible within each SM. Finally, each processor has a small number of on-board registers that provide rapid access to important variables. The speed at which each processor can access data in the different types of memory is visually represented by the proximity of the memory to the processors in Fig.~\ref{fig:CUDA-GPU}. The technical specifications for the Nvidia GPU used herein are presented in Table~\ref{tab:benchmark-hardware-gpu}.

\begin{table}[htbp]
\caption{Technical specifications of the GPU used to benchmark the GPU optimized DIVAST-TVD code.}
\centering
\begin{footnotesize}
\begin{tabular}{lr}
\toprule
Model & Nvidia Quadro 4000 \\
Core clock frequency & 475~MHz \\
Memory & 2~GB GDDR5 \\
Memory clock frequency & 700~MHz \\
Memory bandwidth & 89.6~\gbps \\
CUDA cores & 256 \\
CUDA compute capability & 2.0 \\
\bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:benchmark-hardware-gpu}
\end{table}

The processors within each SM can run in parallel by independently executing the same sets of operations on different sets of data (otherwise known as SIMD). In CUDA, these operations are defined in `kernels', which loosely correlate to loops or subroutines in a serial application. The kernels are launched from the CPU host and are split into grids of `blocks' on the GPU so that each block is handled by a single SM. The blocks are then also split into `threads', where each thread is assigned to one of the SM's processors.

In order to minimize costly host-device memory transfers, every subroutine in DIVAST-TVD was rewritten as a device kernel. Combined with the large global memory space (Table~\ref{tab:benchmark-hardware-gpu}), this enabled the entire computation domain to be kept on the device, without the need for host-device memory transfers at every time-step. The overall program control flow for the GPU optimized version of DIVAST-TVD is shown in Fig.~\ref{fig:DIVAST-GPU-flow}. During the rewriting process, the efficient use of registers, shared memory and global memory was a high priority.

\begin{figure}[htbp]
\centering
\includegraphics[height=11.5cm]{DIVAST-GPU-flow}
\caption{Schematic of the program control flow for the GPU optimized version of DIVAST-TVD. Items on the left are run on the host, while items on the right are run on the device. The abbreviations `HtoD' and `DtoH' indicate host-device and device-host memory transfers, respectively.}
\label{fig:DIVAST-GPU-flow}
\end{figure}

\begin{table}[htbp]
\caption{Benchmarking results for GPU parallelized DIVAST-TVD code. Speed-ups are calculated relative to original code.}
\centering
\begin{footnotesize}
\begin{tabular}{lcS[table-format=1.2]}
\toprule
 & Duration & \\ 
Code & (hh:mm:ss) & {Speed-up} \\ \midrule
Original & 22:41:21 & 1 \\
Serial & 14:00:20 & 1.62 \\
Parallel (CPU) & 06:21:45 & 3.57 \\
Parallel (GPU) & 03:41:37 & 6.14 \\
\bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:benchmark-gpu}
\end{table}

The execution time and relative speed-up of the GPU optimized code compared to the original code are presented in Table~\ref{tab:benchmark-gpu}. It can be seen that the GPU optimized code runs over six times faster than the original code. Referring to Amdahl's law and Table~\ref{tab:benchmark-hardware-gpu}, the maximum theoretical speed-up achievable with 256 cores is 9.66. The slightly lower observed speed-up of 6.14 suggests that the GPU code is memory bound. Although efficient use of registers and shared memory are already employed, future work to utilize texture memory may alleviate this issue.

It is important to note that a purely CPU-based parallel optimization approach may be faster in cases where computational effort per time-step is low (\ie small grid domains), due to the overhead involved in launching each kernel and also the low host-device memory bandwidth.
