%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Case Study: Lopen Brook} \label{sec:lopen-brook}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The numerical modelling of riparian woodland in this chapter focuses on one case study site near Wigborough in Somerset, UK. In 2005 the Forestry Commission planted a mixture of native woodland (English oak, alder, birch, poplar, willow, \etc) on the floodplain of Lopen Brook in order to restore woodland that had previously been lost due to agriculture. The woodland covers a region of roughly 5~ha at a planting density of 3~m by 2~m (equivalent to 1,666 trees \perhectare). The location of the case study site is shown in Fig.~\ref{fig:domain-location} and the planning document for the plantation is given in Fig.~\ref{fig:planting-diagram}.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.9\textwidth]{domain-location}
\caption{Location of the case study site with the modelling domain contained within the rectangle. Green areas indicate woodland and the black lines on Lopen Brook show locations of survey cross-sections.}
\label{fig:domain-location}
\end{figure}

Lopen Brook flows north-east into the River Parrett, which then flows north-west and eventually discharges into the Severn Estuary at Burnham-on-Sea. The 1D model that the EA has constructed for the River Parrett does not extend as far south as Lopen Brook and therefore the River Parrett is excluded from the modelling domain in this study for lack of suitable flow and bathymetry data. However, the distance between the upstream area of interest (woodland plantation) and the downstream confluence is approximately 500~m and is sufficient to minimize any backwater effects.

The main channel of Lopen Brook varies in width from 1.5~m to 3~m, gradually increasing downstream. The bed is comprised of mud and silt with patches of gravel riffles and in-stream vegetation such as sedges. The banks are relatively steep and covered in grasses and other underbrush. A typical channel cross-section is shown in Fig.~\ref{fig:photo-river}.

During visits to the site in 2012 and 2013 the general characteristics of the trees within the floodplain woodland were assessed. The trees were predominantly singe-stemmed with the height at which they begin to branch typically dependent on their species, but generally ranging from 1~m to 4~m. The range in tree height was similarly dependent on species, with the poplar being approximately 8~m in height and the alder roughly 3~m tall. The average main-stem diameter at chest height ($\approx 1$~m) was determined from a selection of 20 representative trees and was 64.7~mm ($\sigma = 11.2$~mm). The floodplain itself consists mainly of wild and grazed grass, with clusters of denser underbrush such as nettles and brambles. A photograph of the woodland on the left-hand floodplain is provided in Fig.~\ref{fig:photo-woodland}.

\begin{landscape}
	\begin{figure}[htbp]
	\centering
	\includegraphics[height=0.85\textheight]{photo-river}
	\caption{Typical channel cross-section for the upper section of Lopen Brook, looking upstream.}
	\label{fig:photo-river}
	\end{figure}

	\begin{figure}[htbp]
	\centering
	\includegraphics[height=0.85\textheight]{photo-woodland}
	\caption{View of the woodland on the left-hand floodplain of Lopen Brook, looking downstream. Photograph taken on the 5\sps{th} June 2013.}
	\label{fig:photo-woodland}
	\end{figure}
\end{landscape}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Digital Elevation Model}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The creation of a digital elevation model (DEM) for the Lopen Brook site was split into three parts: collection of channel bathymetry; processing of remotely sensed ground elevation data; and stitching of the channel bathymetry into the ground elevation data.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Channel Bathymetry}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The channel bathymetry for Lopen Brook was collected during May and June of 2013, in conjunction with Huw Thomas of Forest Research. A total of 62 cross-sections were surveyed using a digital level and levelling staff. Elevation measurements were taken at intervals of roughly 10~cm along each cross-section, providing good resolution of channel features. The upstream and downstream boundary cross-sections for the modelling domain are presented in Fig.~\ref{fig:xs-profiles}.

\begin{figure}[htbp]
\centering
\includegraphics{xs-profiles}
\caption{Surveyed cross-sections: (a) upstream boundary; (b) downstream boundary. Shaded areas show water levels at time of channel survey.}
\label{fig:xs-profiles}
\end{figure}

The cross-sections were surveyed at a denser spacing at the upstream end of the reach where the floodplain woodland plantations are located. Towards the lower-third of the reach, the channel becomes straighter and more uniform in shape. Therefore, the density of cross-sections at the downstream end was reduced accordingly. The streamwise distance between each cross-section varied from 3.9~m to 139~m, with an average of 18.4~m. In addition to the channel bathymetry, the water level at each cross-section was also recorded. The channel thalweg and water level long profiles are plot in Fig.~\ref{fig:long-profile-survey} ($x_s$ is the streamwise distance).

\begin{figure}[htbp]
\centering
\includegraphics{long-profile-survey}
\caption{Long profile for Lopen Brook survey data. Points show locations of individual cross-sections.}
\label{fig:long-profile-survey}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{LiDAR Data}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Light Detection and Ranging (LiDAR) is an airborne mapping technique, which uses a laser to measure the distance between the aircraft and the ground. Up to 100,000 measurements per second are made of the ground, allowing highly detailed terrain models to be generated at spatial resolutions of between 25~cm and 2~m. The LiDAR data used in this study was provided by the Environment Agency (EA) and covered an area of 2~km by 2~km at a resolution of 2~m, with a vertical accuracy of $\pm 5$--15~cm. The data is referenced using the British National Grid OSGB36 and all elevations are given in metres above Ordnance Datum Newlyn.

Both the Digital Surface Model (DSM) and Digital Terrain Model (DTM) were available. The DTM was selected as the basis for the DEM since it represents the true ground elevation, while the DSM represents the surface elevation, including objects such as vegetation and buildings.

Before incorporating the channel bathymetry into the LiDAR data, the DTM was processed to prepare it for use in the hydrodynamic model. Firstly, the DTM was cropped to enclose the area of interest (\ie Lopen Brook; see Fig.~\ref{fig:domain-location}). A rectangular region roughly 21.5~ha in area and angled at $35^{\circ}$ to the horizontal was chosen as the domain extent. This minimized the domain size and thus the computational effort. The DTM was then interpolated onto the angled grid and the resolution of the DTM was increased to 1~m so that the channel bathymetry could be resolved with greater accuracy.

\begin{figure}[htbp]
\centering
\includegraphics[width=\textwidth]{map-plain}
\caption{Elevation contour map for the Lopen Brook DEM. Heights are given in metres above Ordnance Datum Newlyn.}\label{fig:map-plain}
\end{figure}

Once the DTM had been processed, the 1D channel bathymetry data was included into the DTM via a final interpolation. The resulting DEM is represented as an elevation contour map in Fig.~\ref{fig:map-plain}. The total number of grid cells in the domain is 213,526. A radial basis function method was used for the interpolation operations as it provides a more stable and accurate interpolation than an inverse distance weighted approach for large, gradually varying surfaces \citep{Eldrandaly-11, Forti-12}.

\begin{figure}[htbp]
\centering
\includegraphics{long-profile-model}
\caption{Long profile for Lopen Brook DEM. Points refer to original survey cross-section locations.}
\label{fig:long-profile-model}
\end{figure}

When interpolating cross-section bathymetry data into a grid of lower resolution, it is important to preserve the cross-sectional area of the channel. This was checked for the Lopen Brook DEM by visually inspecting the width of the channel and by comparing the long profile from the survey data (Fig.~\ref{fig:long-profile-survey}) to that of the DEM (Fig.~\ref{fig:long-profile-model}). It can be seen that the DEM accurately reflects the original channel bathymetry, with only minor variations in bed slope.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Calibration}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In order to ensure that the model properly represented the physical site, a calibration of the channel bathymetry and bed roughness was undertaken. Typically, calibration of a river model would be performed against stage-discharge data obtained from fixed gauging stations over a period of months or years. In this case, however, gauging station data was not available due to the small size of the river. Therefore, a number of in-channel flow measurements were taken manually during August 2013.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Flow Data}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In total, stage-discharge data for Lopen Brook were collected at five cross-sections. The cross-sections were situated at the upstream and downstream boundaries (A \& E), the two water level meter locations (B \& D) and immediately upstream of the main footbridge (C). These locations are highlighted in Fig.~\ref{fig:map-calib-points}.

\begin{figure}[htbp]
\centering
\includegraphics{map-calib-points}
\caption{Cross-sections A--E on Lopen Brook where stage-discharge data was collected for use in model calibration.}
\label{fig:map-calib-points}
\end{figure}

The method to quantify the flow rate was based on the velocity-area method whereby velocities are measured at each vertical. The procedure used is as follows: 
%
\begin{enumerate}
	\item Stretch a measuring tape perpendicularly across the channel at the desired location.
	\item Divide the cross-section into roughly 20 segments of equal width. Note that this may not be possible for smaller streams since the minimum segment width is approximately 15~cm.
	\item Starting at the left-hand bank (as looking downstream), measure the depth and velocity using a propeller type velocity meter at the pre-determined widths.
	\item Once the entire cross-section has been measured, the total flow rate can be calculated as the sum of the segments' flow rates.
\end{enumerate}

The United States Geological Survey (USGS) velocity-area method assumes that at points where the depth is less than 2.5~ft (0.76~m), the average velocity occurs at six-tenths of the total depth. Where the stream is deeper than 2.5~ft, the velocity is measured at two-tenths and eight-tenths of the total depth, and the average of the two readings is used as the average velocity at that point \citep{Buchanan-76}.

The depth-velocity measurements and resulting flow rate calculations are presented in Tables~\ref{tab:WB-flow-upstream-bnd}--\ref{tab:WB-flow-downstream-bnd} for each of the five measurement cross-sections. The total flow rates varied from 0.11~\cumecs to 0.129~\cumecs with $\sigma = 0.007$~\cumecs. The variation in flow rate between cross-sections is therefore relatively minor and was most likely caused by small deviations from the velocity-area method's assumption of mean velocity depth. For the following calibrations, an average flow rate of 0.12~\cumecs was used at the upstream boundary, whilst the bed roughness was modified so that the resulting depths in the model were consistent with those recorded in Tables~\ref{tab:WB-flow-upstream-bnd}--\ref{tab:WB-flow-downstream-bnd}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{2D Calibration}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The bed friction module within DIVAST-TVD can represent bed roughness using either Manning's $n$ or roughness height $k_s$ (see Eqs.~\ref{eq:chezy-mannings}--\ref{eq:cw-transitional}). Whilst the use of Manning's $n$ is common in hydrodynamic modelling, the bed roughness height $k_s$ allows a more complex and physically correct analysis of bed friction to be performed \citep{Ferguson-10}.

In order to calibrate the value of $k_s$ for the in-channel bed roughness, the DIVAST-TVD model for Lopen Brook was run multiple times as a steady state analysis. The upstream boundary was specified as a flow rate of 0.12~\cumecs and the downstream boundary was set to a normal depth with a slope of 1:549 (\ie the measured water surface slope; see Fig.~\ref{fig:long-profile-survey}). The channel was initially dry so each model variation was run for 12~hrs before taking water level readings. This gave the model time to achieve a steady flow state.

After each run the water levels at each of the five cross-sections was inspected and compared with the data in Tables~\ref{tab:WB-flow-upstream-bnd}--\ref{tab:WB-flow-downstream-bnd} respectively. The in-channel roughness was then modified accordingly and the model run again as iterative procedure until the depths were in agreement with the measured stage-discharge data.

The results of the 2D calibration are presented in Table~\ref{tab:2d-calibration}. It can be seen that a roughness length of $k_s = 0.025$~m resulted in the closest match between the measured and predicted water levels at each of the five cross-sections and is therefore used in the following modelling. The bed elevation and water level long profiles for the calibration where $k_s = 0.025$~m are shown in Fig.~\ref{fig:long-profile-calib}.

\begin{table}[htbp]
\caption{In-channel bed roughness 2D calibration for Lopen Brook using DIVAST-TVD. The total errors $\epsilon$ were calculated as the sum of the errors between the predicted and measured water levels at each of the five cross-sections.}
\centering
\sisetup{table-format=2.3}
\begin{footnotesize}
\begin{tabular}{S[table-format=1.3]SSSSSS[table-format=1.3]S[table-format=1.2]}
\toprule
{$k_s$} & \multicolumn{5}{c}{Cross-section WL (m AOD)} & \multicolumn{2}{c}{Total error} \\ \cmidrule(lr){2-6} \cmidrule(lr){7-8}
{(m)} & {A} & {B} & {C} & {D} & {E} & {$\epsilon_\textnormal{abs}$ (m)} & {$\epsilon_{\%}$ (\%)} \\ \midrule
0.01 & 21.063 & 20.842 & 20.591 & 20.268 & 19.25 & 0.186 & 0.91 \\
0.025 & 21.09 & 20.866 & 20.601 & 20.295 & 19.274 & 0.076 & 0.37 \\
0.05 & 21.122 & 20.87 & 20.619 & 20.323 & 19.31 & 0.104 & 0.51 \\
0.075 & 21.144 & 20.873 & 20.626 & 20.35 & 19.339 & 0.186 & 0.91 \\
0.1 & 21.189 & 20.889 & 20.633 & 20.406 & 19.355 & 0.294 & 1.44 \\ \midrule
{Measured} & 21.1 & 20.9 & 20.6 & 20.3 & 19.3 & {-} & {-} \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:2d-calibration}
\end{table}

\begin{figure}[htbp]
\centering
\includegraphics{long-profile-calib}
\caption{Long profile for Lopen Brook during calibration with $k_s = 0.025$~m. Points refer to original survey cross-section locations.}
\label{fig:long-profile-calib}
\end{figure}

Unfortunately there is no data available to calibrate the roughness length of the floodplain. However, a value of $k_s = 0.075$~m is assumed to be applicable given the long grass and occasional underbrush at the site (\citealp{Chow-73}; see Figs.~\ref{fig:photo-river} and \ref{fig:photo-woodland}). This is consistent with field measurements of the roughness lengths for similar land cover types \citep{Medeiros-12}.

The maximum value for $k_s$ is constrained by the Colebrook-White equation \eqref{eq:cw-rough} and is limited to $k_s < 12 \zeta_\textnormal{min}$, where $\zeta_\textnormal{min}$ is the minimum depth allowed during the simulation. In this study, $\zeta_\textnormal{min} = 0.01$~m and thus both the in-channel and floodplain roughness lengths satisfy the restriction of $k_s \le 0.12$~m.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Boundary Conditions}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Upstream}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In the absence of gauging station data for Lopen Brook, a Revitalized Flood Hydrograph (ReFH) model was run for the catchment to generate flood hydrographs with return periods of 5 to 500 years (Fig.~\ref{fig:flood-hydrographs}). The ReFH model is based on robust hydrological modelling techniques and is considered to be an improvement over the standard Flood Estimation Handbook (FEH) model \citep{Kjeldsen-05}.

\begin{figure}[htbp]
\centering
\includegraphics{flood-hydrographs}
\caption{Flood hydrographs for Lopen Brook.}
\label{fig:flood-hydrographs}
\end{figure}

The catchment area used in the ReFH analysis was 18~km\sps{2}, while the rainfall duration was 9.5~hrs. The time to peak was 3.76~hrs and the base discharge was 0.45~\cumecs. The model was run with the upstream boundary set to the base discharge as a steady state analysis for 12~hrs so that
initial water levels and flow rates could be determined for use in the following simulation scenarios.
\nomenclature[qq]{$Q$}{Flow rate}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsubsection{Downstream}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

No special treatment of the downstream boundary was required and it was therefore set to an open type boundary where the fluid can flow freely in and out of the domain. This was achieved by setting a zero gradient for the water elevation and flow rates (\ie $\partial \eta / \partial m = \partial q / \partial m = 0$ where $m$ is the direction normal to the outlet plane).
