%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Numerical Model}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Since the shallow water equations are a set of hyperbolic partial differential equations, there is often no analytical solution, except for the most trivial of cases. Therefore, solutions have to be obtained via numerical approximation methods. For computational fluid dynamics there are two main approaches: finite difference and finite volume.

The finite difference method utilizes a grid system, usually of equally spacing $\Delta x$ and $\Delta y$, to divide the domain into cells. The variables are then represented at the intersections or mid points of these cells and are defined as a function of the variables in other cells so that when $\Delta x, \Delta y \rightarrow 0$, the finite difference approximation is consistent with the original differential equation. The solution is progressed by $\Delta t$ after each complete computational step.

The finite volume method is similar in that it also calculates variables at discrete points on a grid. However, the grid is not limited to being regular in shape and can take the form of an unstructured mesh. The variables are typically located at the centroids of the mesh cells and are integrated over the cell volume. A set of linear simultaneous equations then defines the fluxes between each cell.

One of the main advantages of the finite difference method is that it requires less computational time compared to the finite volume method, as shown by \citet{Weare-76}, even though the finite volume discretization generally requires fewer grid points. Another advantage is that it is relatively straightforward to transfer differential equations into finite difference form. However, since the method must be carried out on a regular grid, irregular boundary conditions such as those found in alluvial river systems can be hard to represent. 

On the other hand, the unstructured nature of the finite volume approach means that is capable of handling any form of boundary condition. The finite volume method is also inherently mass conservative since the flux entering a given volume is identical to the flux leaving the adjacent volume.

In this chapter, an existing finite differencing scheme is employed as the basis for the numerical modelling. The choice of a finite differencing scheme over a finite volume scheme was motivated by the ease of grid generation, computational efficiency and amenability to additional numerical developments. While an unstructured finite volume mesh may have provided more resolution in critical areas, such as the main river channel, the time required to create such a mesh would have been significant. Further, the domain could easily be enclosed within a rectangular area and there was no need for irregular boundary conditions in the case study modelled herein.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{DIVAST-TVD}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

An existing in-house hydrodynamic modelling code was chosen as the basis for further development. The Depth-Integrated Velocity and Solute Transport Total Variation Decreasing (DIVAST-TVD) model solves the SWEs~\eqref{eq:swe} using a second-order accurate explicit scheme on a cell-centered grid \citep{Liang-06a}.

The model first applies the operator-splitting technique to the SWEs~\eqref{eq:swe}:
%
\begin{subequations}
\begin{equation}
\frac{\partial \mathbf{X}}{\partial t} + \frac{\partial \mathbf{F}}{\partial x} = \mathbf{S}
\label{eq:operator-splitting-x}
\end{equation}
\begin{equation}
\frac{\partial \mathbf{X}}{\partial t} + \frac{\partial \mathbf{G}}{\partial x} = \mathbf{T}
\label{eq:operator-splitting-y}
\end{equation}
\label{eq:operator-splitting}
\end{subequations}
%
where $\mathbf{X}$ represents the independent variables $\eta$, $q_x$, and $q_y$; $\mathbf{F}$ and $\mathbf{G}$ represent the flux terms; and $\mathbf{S}$ and $\mathbf{T}$ represent the source terms.

The MacCormack `predictor-corrector' scheme \citep[\eg][]{Wendt-09} is then used to solve the two one-dimensional hyperbolic equations, \ie for Eq.~\eqref{eq:operator-splitting-x}:
%
\begin{subequations}
\begin{equation}
\mathbf{X}^p_i = \mathbf{X}_i - \left(\mathbf{F}_i - \mathbf{F}_{i-1}\right) \cdot \Delta t / \Delta x + \mathbf{S} \cdot \Delta t
\label{eq:maccormack-predictor}
\end{equation}
\begin{equation}
\mathbf{X}^c_i = \mathbf{X}_i - \left(\mathbf{F}^p_{i+1} - \mathbf{F}^p\right) \cdot \Delta t / \Delta x + \mathbf{S}^p \cdot \Delta t
\label{eq:maccormack-corrector}
\end{equation}
\label{eq:maccormack}
\end{subequations}
%
where the superscripts $p$ and $c$ indicate the predictor and corrector steps, respectively; the subscripts denote the spatial grid cell; and $\Delta t$ and $\Delta x$ are the temporal and spatial intervals, respectively.

An additional TVD step is then performed at the corrector stage so that spurious numerical oscillations are avoided in regions of sharp-gradients, or shocks. This allows the model to accurately capture trans- and super-critical flow conditions. The full details of the numerical solution scheme can be found in \cite{Liang-06a}.

In this study, the barometric pressure and wind shear stress terms in Eq.~\eqref{eq:swe} are neglected. The vegetative drag force term requires no special treatment, while the second-order turbulent diffusion terms are discretized using a standard central differencing scheme. The cross-derivative turbulent diffusion terms are assumed to be negligible. The drag force and turbulent eddy viscosity models are detailed below.

The choice of a TVD scheme over a more traditional solution scheme, such as the Alternating Direction Implicit (ADI) method, was originally motivated by the need for a shock-capturing model in order to model steep, upper-river catchments where super-critical flows are likely to occur during large flood events. While the final site chosen for modelling in this study is situated towards the upper-catchment, the surrounding floodplain is relatively flat. This suggests that a shock-capturing scheme is less crucial and that a side-centered, ADI approach may also be suitable; however, considerable program development had been undertaken on DIVAST-TVD prior to the finalization of the site location.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Bed Shear Stress}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The bed shear stress within DIVAST-TVD is represented in the form of a quadratic friction law. In the $x$ direction it is given by:
%
\begin{equation}
\tau_{bx} = \frac{\rho g q_x \sqrt{q_x^2 + q_y^2}}{\zeta^2 C^2}
\label{eq:bed-shear-x}
\end{equation}
%
where $C$ is the Chezy coefficient, which typically varies between 20~\chezy and 70~\chezy in rivers and floodplains \citep{Weiyan-92}.
\nomenclature[cc]{$C$}{Chezy roughness coefficient}

The Chezy coefficient can be calculated in a number of different ways within DIVAST-TVD. The simplest is to specify a constant value for each grid point at the start of the simulation. However, this approach obviously prevents any variation with flow depth or Reynolds number. A more common approach relates the Chezy coefficient to the local flow depth and Manning's $n$ via:
%
\begin{equation}
C = \frac{R_h^{1/6}}{n}
\label{eq:chezy-mannings}
\end{equation}
%
where $R_h$ is the hydraulic radius and is taken as the flow depth $\zeta$ in wide, open channel flow.
\nomenclature[rr]{$R_h$}{Hydraulic radius}

For fully rough, turbulent flows (\ie $Re \gg 1000$), the Chezy coefficient can also be related to the local bed roughness using a modified version of the Colebrook-White equation:
%
\begin{equation}
C = -2 \sqrt{8g} \log_{10} \left(\frac{k_s}{12 \zeta}\right)
\label{eq:cw-rough}
\end{equation}
%
where $k_s$ is the equivalent sand grain size, or roughness length \citep{Chow-73}.
\nomenclature[k]{$k_s$}{Bed roughness height}

The advantage of using Eq.~\eqref{eq:cw-rough} is that the Chezy coefficient can be more closely related to the physical properties of the bed surface, such as dunes, ripples or boulders. However, for transitional flows ($500 < Re < 2000$) the Chezy coefficient is also dependent on the Reynolds number, \ie:
%
\begin{equation}
C = -2 \sqrt{8g} \log_{10} \left(\frac{k_s}{12 \zeta} + \frac{2.5}{\sqrt{8g} Re} C\right)
\label{eq:cw-transitional}
\end{equation}
%
where $C$ is solved for in an iterative process.

In this study, the high grid resolution of the computational domain (made possible via the optimizations detailed in the next section) enables local differences in water levels, velocities and bed shear stresses due to inclusion or exclusion or Reynolds number effects to be investigated.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Vegetative Drag Force} \label{sec:numerical-model-drag}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Decoupling the bed friction and drag force, instead of artificially increasing the surface roughness, ensures that the bed shear stress remains physically correct. Therefore, the vegetation in the following case study is incorporated into the SWEs via a drag force term in the momentum equations (Eqs.~\ref{eq:swe-momentum-x} and \ref{eq:swe-momentum-y}) and a porosity term in the continuity equation (Eq.~\ref{eq:swe-continuity}). The drag force terms in the $x$ and $y$ directions, respectively, are:
%
\begin{subequations}
\label{eq:veg-drag}
\begin{equation}
F_x = \frac{1}{2} N C_d A_p \frac{q_x \sqrt{q_x^2 + q_y^2}}{\zeta^2}
\label{eq:veg-drag-x}
\end{equation}
\begin{equation}
F_y = \frac{1}{2} N C_d A_p \frac{q_y \sqrt{q_x^2 + q_y^2}}{\zeta^2}
\label{eq:veg-drag-y}
\end{equation}
\end{subequations}
%
where $N$ is the number of trees per m\sps{2}.
\nomenclature[nn]{$N$}{Number of trees per m\sps{2}}

In the new Cauchy drag force model (Eq.~\ref{eq:drag-cauchy}; see \autoref{sec:cauchy-model-theory}), the characteristic drag coefficient $C_d A_p$ varies with velocity to mimic the effects of flexible vegetation's reconfiguration (Eq.~\ref{eq:cauchy-CdAp}) and is determined as a function of the vegetative Cauchy number (Eq.~\ref{eq:cauchy-number}).

The implementation within DIVAST-TVD allows the user to specify a relationship between the flow depth $\zeta$ and projected area $A_{p0}$ for each vegetation type, thus allowing the model to be applied to vegetation in both submerged and non-submerged conditions.

The blockage effect of the vegetation is accounted for via a porosity term:
%
\begin{equation}
\phi = 1 - N \frac{\pi D^2}{4}
\label{eq:veg-porosity}
\end{equation}
%
where $D$ is the equivalent cylinder diameter of the submerged portion of the vegetation.

Considering the blockage effect in the SWEs, the conservation of mass equation (Eq.~\ref{eq:swe-continuity}) then becomes:
%
\begin{equation}
\phi \frac{\partial \eta}{\partial t} + \frac{\partial q_x}{\partial x} + \frac{\partial q_y}{\partial y} = 0
\end{equation}
%
while the bed shear stress equation (Eq.~\ref{eq:bed-shear-x}) is similarly modified to account for the bed area occupied by the vegetation, \ie in the $x$ direction:
%
\begin{equation}
\tau_{bx} = \phi \frac{\rho g q_x \sqrt{q_x^2 + q_y^2}}{\zeta^2 C^2}
\end{equation}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Turbulence Model}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In order to include the turbulent diffusion terms within DIVAST-TVD, a model for the eddy viscosity is developed as these terms were originally neglected. Assuming that the turbulent shear stress is dominated by the bottom friction, \citet{Fischer-79} proposed the following model for the depth-averaged eddy viscosity:
%
\begin{equation}
\nu_t = C_e u_* \zeta
\label{eq:fischer-vt}
\end{equation}
%
where $C_e$ is an empirical coefficient; $u_* = \sqrt{\tau_b / \rho}$ is the shear velocity; and $\zeta$ is the flow depth \citep{Fischer-79}.

Theoretically, $C_e$ should be equal to $\kappa / 6$, where $\kappa$ is the von Karman's constant. However, it is commonly accepted that $C_e$ may take values ranging from 0.15 in flumes to 1.5 in coastal and estuarine areas \citep{Elder-59,Fischer-79}. In the absence of calibration data, a value of $C_e = 1.2$ is recommended \citep{Falconer-91}. Referring to Eq.~\eqref{eq:bed-shear-x} and rearranging, the final depth-averaged eddy viscosity model can thus be written:
%
\begin{equation}
\nu_t = \frac{C_e \sqrt{g (q_x^2 + q_y^2)}}{C}
\label{eq:depth-averaged-vt}
\end{equation}

Although Eq.~\eqref{eq:depth-averaged-vt} is applicable in regions of open flow, significant errors may be introduced near rigid walls since it cannot account for horizontal velocity gradients. To address this issue, \citet{Wu-04} introduced a modified mixing length model by combining the depth-averaged approach with Prandtl's mixing length theory:
%
\begin{equation}
\nu_t = \sqrt{\left(\alpha u_* \zeta\right)^2 + \left(l_h^2 |\overline{S}|\right)^2}
\end{equation}
%
where $\alpha$ is an empirical coefficient, analogous to $C_e$; $l_h$ is the horizontal mixing length; and $|\overline{S}| = \left[2\left(\partial u / \partial x\right)^2 + 2\left(\partial v / \partial y\right)^2 + \left(\partial u / \partial y + \partial v / \partial x\right)^2\right]^{1/2}$.

The horizontal mixing length is a function of the distance to the nearest surface, \ie:
%
\begin{equation}
l_h = \kappa \min \left(c_m \zeta, y\right)
\end{equation}
%
where $c_m$ is an empirical constant set to 1.2; $\zeta$ is the flow depth; and $y$ is the distance to the nearest wall. In the case of river and floodplain modelling, a dry cell is counted as a wall when determining $y$.

More advanced turbulence models, such as the `one-equation' SA or `two-equation' depth-averaged $k$-$\epsilon$ model proposed by \citet{Rastogi-78}, are not implemented in this study due to their higher computational cost. Furthermore, it has been shown that the velocity distributions predicted by the depth-averaged and mixing length models are very similar to those predicted by the $k$-$\epsilon$ model and its variants for simple river channels \citep{Wu-04}.
