#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

set terminal pdfcairo enhanced size 5.2,2.5 font "NimbusRomNo9L-Regu,22" fontscale 0.4

set output "../images/map-calib-points.pdf"

CNTRS = "../data/contours.dat"
XMAX = 885
YMAX = 240

set macro
set lmargin at screen 0.11
set bmargin at screen 0.17
set tmargin at screen 0.95

set xlabel "{/NimbusRomNo9L-ReguItal x} (m)" offset 0,0.75
set ylabel "{/NimbusRomNo9L-ReguItal y} (m)" offset 1,0
set xrange [0:XMAX]
set yrange [0:YMAX]
set xtics add ("%g" XMAX) font ",18" offset 0,0.25
set ytics add ("%g" YMAX) font ",18"


##################################################################################################################
# PLOT
##################################################################################################################

ARROW_STYLE = 'nohead front'
LABEL_STYLE = 'font ",20" front'

set arrow 1 from 5,195 to 5,175 @ARROW_STYLE
set arrow 2 from 115,165 to 125,185 @ARROW_STYLE
set arrow 3 from 232,137 to 240,160 @ARROW_STYLE
set arrow 4 from 332,100 to 335,122 @ARROW_STYLE
set arrow 5 from 875,190 to 838,205 @ARROW_STYLE

set label 1 "A" at 15,200 @LABEL_STYLE
set label 2 "B" at 130,190 @LABEL_STYLE
set label 3 "C" at 245,165 @LABEL_STYLE
set label 4 "D" at 340,125 @LABEL_STYLE
set label 5 "E" at 810,200 @LABEL_STYLE

load "north-arrow.gnu"
load "bed-elev-contours-labelled.gnu"
