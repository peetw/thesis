#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

set terminal pdfcairo enhanced size 5.2,2.5 font "NimbusRomNo9L-Regu,22" fontscale 0.4

set output "../images/map-plain.pdf"

CNTRS = "../data/contours.dat"
XMAX = 885
YMAX = 240

set lmargin at screen 0.11
set bmargin at screen 0.17
set tmargin at screen 0.95

set xlabel "{/NimbusRomNo9L-ReguItal x} (m)" offset 0,0.75
set ylabel "{/NimbusRomNo9L-ReguItal y} (m)" offset 1,0
set xrange [0:XMAX]
set yrange [0:YMAX]
set xtics add ("%g" XMAX) font ",18" offset 0,0.25
set ytics add ("%g" YMAX) font ",18"


##################################################################################################################
# PLOT
##################################################################################################################

set label 1 "Lopen Brook" at 270,130 tc rgb 'grey50' font ",15"

load "north-arrow.gnu"
load "bed-elev-contours-labelled.gnu"
