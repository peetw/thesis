#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

set terminal pdfcairo monochrome enhanced dashed size 4,2.8 font "NimbusRomNo9L-Regu,22" fontscale 0.4

set output "../images/chezy-shear-comparison.pdf"

set xlabel "{/NimbusRomNo9L-ReguItal Re} (-)"
set ylabel "{/rtxmi τ}_{/NimbusRomNo9L-ReguItal b} (N m^{-2})" offset -1,0
set xrange [10**3:10**5]
set logscale xy
set format xy "10^{%L}"
set key left Left reverse invert box lw 0.5 width 2


##################################################################################################################
# PLOT
##################################################################################################################

ks = 0.075
c(dp) = -18 * log10(ks / (12.*dp))
c_re(dp,re,ctmp) = -18*log10(ks / (12.*dp) + 5*ctmp / (18.*re))
c_iter(dp,re,ctmp) = (abs(c_re(dp,re,ctmp)-ctmp) < 0.5 ? c_re(dp,re,ctmp) : c_iter(dp,re,c_re(dp,re,ctmp)))

rho = 1000
g = 9.81
visc = 1.3*10**-6
tau_b(dp, re, c) = rho * g * (re * visc)**2 / (16 * dp**2 * c**2)

set label 1 "ζ = 0.1 m" at 30000,tau_b(0.1, 30000, c(0.1))+0.3 font ",18" rotate by 20
set label 2 "ζ = 0.5 m" at 33000,tau_b(0.5, 33000, c(0.5))-0.002 font ",18" rotate by 20

plot	NaN w lines lt 2 t "{/NimbusRomNo9L-ReguItal C_{Re}}", \
		for [i=1:5] tau_b(i/10., x, c_iter(i/10., x, c(i/10.))) w lines lt 2 notitle, \
		NaN w lines lt 1 t "{/NimbusRomNo9L-ReguItal C_ζ}", \
		for [i=1:5] tau_b(i/10., x, c(i/10.)) w lines lt 1 notitle
