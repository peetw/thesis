##################################################################################################################
# SETTINGS
##################################################################################################################

# Contour variables
xi = 0; xa = XMAX; yi = 0; ya = YMAX;
xl = xi + 0.075*(xa - xi); xh = xa - 0.075*(xa-xi);
yl = yi + 0.075*(ya - yi); yh = ya - 0.075*(ya-yi);
x0 = 0; y0 = 0; b = 0

# Contour functions
g(x,y)=(((x > xl && x < xh && y > yl && y < yh) ? (x0 = x, y0 = y) : 1), 1/0)
ws(x,y) = ( (x == x0 && y == y0) ? 1 : 1/0)
lab(x,y) = ( (x == x0 && y == y0) ? stringcolumn(3) : "")

# Contour macros
ZERO = "x0 = xi - (xa-xi), y0 = yi - (ya-yi), b = b+1"
SEARCH = "CNTRS index b u 1:(g($1,$2)) notitle"
PLOT = "CNTRS index b u 1:2 w lines lt 1 lw 1.5 lc rgb 'grey' notitle"
SPACE = "CNTRS index b u 1:2:(ws($1,$2)) w points pt 5 ps 1.5 lc rgb 'white' notitle"
LABEL = "CNTRS index b u 1:2:(lab($1,$2)) w labels tc rgb 'grey50' font ',16' notitle"

set macro


##################################################################################################################
# PLOT
##################################################################################################################

# Plot contours with labels
plot @SEARCH, @PLOT, @SPACE, @LABEL, @ZERO, \
@SEARCH, @PLOT, @SPACE, @LABEL, @ZERO, \
@SEARCH, @PLOT, @SPACE, @LABEL, @ZERO, \
@SEARCH, @PLOT, @SPACE, @LABEL, @ZERO, \
@SEARCH, @PLOT, @SPACE, @LABEL, @ZERO, \
@SEARCH, @PLOT, @SPACE, @LABEL, @ZERO, \
@SEARCH, @PLOT, @SPACE, @LABEL
