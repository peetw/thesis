#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

set terminal pdfcairo monochrome enhanced dashed size 4,4.5 font "NimbusRomNo9L-Regu,22" fontscale 0.4

set output "../images/s1-hydrographs.pdf"

DATA = "../data/s1-hydrographs.dat"

set macro
set xrange [-4:52]
set yrange [-1.5:11.5]
set multiplot layout 3,2

lab_plot(s) = sprintf('set label 1 "(%s)" at graph 0.1,0.9 font ",18"', s)

TMARGIN = "set tmargin at screen 0.96; set bmargin at screen 0.68"
CMARGIN = "set tmargin at screen 0.68; set bmargin at screen 0.40"
BMARGIN = "set tmargin at screen 0.40; set bmargin at screen 0.12"
LMARGIN = "set lmargin at screen 0.12; set rmargin at screen 0.52"
RMARGIN = "set lmargin at screen 0.52; set rmargin at screen 0.92"


##################################################################################################################
# PLOT Q5
##################################################################################################################

set xtics 12 scale 0.5 font ",18"
set ytics 2 scale 0.5 font ",18"
set format x ""
unset key
@TMARGIN
@LMARGIN
eval lab_plot("a")

plot	DATA index 0 u 1:2 w lines lt 1 t "Upstream", \
		DATA index 0 u 1:3 w lines lt 2 t "Downstream"

unset border
unset ylabel
unset xtics
unset ytics
set key center Left reverse box lw 0.5 height 0.5 width 1 samplen 2
@TMARGIN
@RMARGIN
unset label 1

plot	1/0 w lines lt 1 t "Upstream", \
		1/0 w lines lt 2 t "Downstream"


##################################################################################################################
# PLOT Q10
##################################################################################################################

set border
set ylabel "{/NimbusRomNo9L-ReguItal Q} (m^{3}s^{-1})"
set xtics 12 scale 0.5 font ",18"
set ytics 2 scale 0.5 font ",18"
set format y "%g"
unset key
@CMARGIN
@LMARGIN
eval lab_plot("b")

plot	DATA index 1 u 1:2 w lines lt 1, \
		DATA index 1 u 1:3 w lines lt 2


##################################################################################################################
# PLOT Q25
##################################################################################################################

unset ylabel
set format y ""
@CMARGIN
@RMARGIN
eval lab_plot("c")

plot	DATA index 2 u 1:2 w lines lt 1, \
		DATA index 2 u 1:3 w lines lt 2


##################################################################################################################
# PLOT Q50
##################################################################################################################

set format xy "%g"
@BMARGIN
@LMARGIN
eval lab_plot("d")

plot	DATA index 3 u 1:2 w lines lt 1, \
		DATA index 3 u 1:3 w lines lt 2


##################################################################################################################
# PLOT Q100
##################################################################################################################

set xlabel "{/NimbusRomNo9L-ReguItal t} (hrs)" offset -10,0.5
set format y ""
@BMARGIN
@RMARGIN
eval lab_plot("e")

plot	DATA index 4 u 1:2 w lines lt 1, \
		DATA index 4 u 1:3 w lines lt 2
