#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

set terminal pdfcairo monochrome enhanced dashed size 4,2.8 font "NimbusRomNo9L-Regu,22" fontscale 0.4

DATA = "../data/flood-hydrographs.dat"

set output "../images/flood-hydrographs.pdf"

set xlabel "{/NimbusRomNo9L-ReguItal t} (hrs)"
set ylabel "{/NimbusRomNo9L-ReguItal Q} (m^3s^{-1})"
set xrange [0:48]
set xtics 6 scale 0.5
set ytics scale 0.5
set key Left reverse box lw 0.5 height 0.75 width 1 autotitle columnheader


##################################################################################################################
# PLOT
##################################################################################################################

plot	for[i=0:4] DATA index i u ($1/(60.*60)):2 w lines, \
		for[i=5:6] DATA index i u ($1/(60.*60)):2 w lines lc rgb 'gray70'
