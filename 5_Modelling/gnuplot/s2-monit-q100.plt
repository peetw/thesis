#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

set terminal pdfcairo monochrome enhanced dashed size 5.2,6 font "NimbusRomNo9L-Regu,22" fontscale 0.4

set output "../images/s2-monit-q100.pdf"

DATA = "../data/s2-monit-q100.dat"
DPMIN = 0.01

set macro
set xrange [-4:52]
set xtics 0,12,48 scale 0.5 font ",18"
set ytics scale 0.5 font ",18"
set multiplot layout 3,3

lab_plot(s) = sprintf('set label 1 "(%s)" at graph 0.1,0.9 font ",16"', s)
lab_chan(i) = sprintf("C%i", (i+4)/6)
lab_veg(i) = sprintf("W%i", (i-26)/6)
lab_flood(i) = sprintf("F%i", (i-56)/6)
vel(h,e,qx,qy) = (h+e > DPMIN ? sqrt(qx**2 + qy**2) / (h+e) : 0)
shear(tx,ty) = sqrt(tx**2 + ty**2)

DP = "(column(i)+column(i+1))"
VEL = "(vel(column(i),column(i+1),column(i+2),column(i+3)))"
SHEAR = "(shear(column(i+4),column(i+5)))"

TB_TMARGIN = "set tmargin at screen 0.975; set bmargin at screen 0.675"
TB_CMARGIN = "set tmargin at screen 0.675; set bmargin at screen 0.375"
TB_BMARGIN = "set tmargin at screen 0.375; set bmargin at screen 0.075"
LR_LMARGIN = "set lmargin at screen 0.10; set rmargin at screen 0.39"
LR_CMARGIN = "set lmargin at screen 0.39; set rmargin at screen 0.68"
LR_RMARGIN = "set lmargin at screen 0.68; set rmargin at screen 0.97"

LSTYLE="lines lw 1"

##################################################################################################################
# PLOT DEPTH
##################################################################################################################

set ylabel "{/NimbusRomNo9L-ReguItal ζ} (m)" offset 2,0
set yrange [-0.25:2.25]
set ytics 0,0.5,2
set format x ""
unset key
eval lab_plot("a")
@TB_TMARGIN
@LR_LMARGIN

plot for [i=2:26:6] DATA u ($1/3600.):@DP w @LSTYLE

eval lab_plot("b")
unset ylabel
set format y ""
@LR_CMARGIN

plot for [i=32:56:6] DATA u ($1/3600.):@DP w @LSTYLE

eval lab_plot("c")
@LR_RMARGIN

plot for [i=62:74:6] DATA u ($1/3600.):@DP w @LSTYLE


##################################################################################################################
# PLOT VELOCITIES
##################################################################################################################

set ylabel "{/NimbusRomNo9L-ReguItal U} (m s^{-1})" offset 0.5,0
set yrange [-0.1:1.1]
set ytics 0,0.5,1
set format y "%g"
set key Left reverse box lw 0.5 height 0.25 width 1.5 samplen 3 font ",16"
eval lab_plot("d")
@TB_CMARGIN
@LR_LMARGIN

plot for [i=2:26:6] DATA u ($1/3600.):@VEL w @LSTYLE t lab_chan(i)

eval lab_plot("e")
unset ylabel
set format y ""
@LR_CMARGIN

plot for [i=32:56:6] DATA u ($1/3600.):@VEL w @LSTYLE t lab_veg(i)

eval lab_plot("f")
@LR_RMARGIN

plot for [i=62:74:6] DATA u ($1/3600.):@VEL w @LSTYLE t lab_flood(i)


##################################################################################################################
# PLOT BED SHEAR
##################################################################################################################

set ylabel "{/rtxmi τ}_b (N m^{-2})" offset 0.5,0
set yrange [-0.3:2.8]
set ytics 0,0.5,2.5
set format xy "%g"
unset key
eval lab_plot("g")
@TB_BMARGIN
@LR_LMARGIN

plot for [i=2:26:6] DATA u ($1/3600.):@SHEAR w @LSTYLE

set xlabel "{/NimbusRomNo9L-ReguItal t} (hrs)" offset 0,0.5
unset ylabel
set format y ""
eval lab_plot("h")
@LR_CMARGIN

plot for [i=32:56:6] DATA u ($1/3600.):@SHEAR w @LSTYLE

eval lab_plot("i")
unset xlabel
@LR_RMARGIN

plot for [i=62:74:6] DATA u ($1/3600.):@SHEAR w @LSTYLE
