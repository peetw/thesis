#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

set terminal pdfcairo enhanced size 5.2,7.75 font "NimbusRomNo9L-Regu,22" fontscale 0.4

set output "../images/s1-depths-q5.pdf"
CNTRS = "../data/contours.dat"
HYDRO = "../data/flood-hydrographs.dat"

XMAX = 885
YMAX = 240
DPMIN = 0.01
DPMAX = 2.5

set macro
set xlabel "{/NimbusRomNo9L-ReguItal x} (m)" offset 0,1
set ylabel "{/NimbusRomNo9L-ReguItal y} (m)" offset -0.5,0
set cblabel "{/NimbusRomNo9L-ReguItal ζ} (m)" offset -0.5,0
set xrange [0:XMAX]
set yrange [0:YMAX]
set cbrange [0:DPMAX]
set xtics add ("%g" XMAX) font ",18" scale 0.5 offset 0,0.5
set ytics 60 add ("%g" YMAX) font ",18" scale 0.5
set cbtics font ",18" offset -0.5,0

set view map
set lmargin at screen 0.11
set rmargin at screen 0.85
set palette defined(0 '#000090', 1 '#000fff', 2 '#0090ff', 3 '#0fffee', 4 '#90ff70', 5 '#ffee00', 6 '#ff7000', 7 '#ee0000', 8 '#7f0000')
unset key

set multiplot layout 4,1

load "north-arrow.gnu"
set title font ",18" offset 0,-0.75


##################################################################################################################
# PLOT T0
##################################################################################################################

DATA = "../data/s1-depths-q5-t0.dat"
set title "(a)   {/NimbusRomNo9L-ReguItal t} = 0 hrs"
set tmargin at screen 0.95
set bmargin at screen 0.80

splot	DATA u 1:2:($3+$4 > DPMIN ? $3+$4 : NaN) w image, \
		CNTRS u 1:2:1 with lines lw 1.5 lc rgb 'grey' notitle


##################################################################################################################
# PLOT T6
##################################################################################################################

DATA = "../data/s1-depths-q5-t6.dat"
set title "(b)   {/NimbusRomNo9L-ReguItal t} = 6 hrs"
set tmargin at screen 0.70
set bmargin at screen 0.55

splot	DATA u 1:2:($3+$4 > DPMIN ? $3+$4 : NaN) w image, \
		CNTRS u 1:2:1 with lines lw 1.5 lc rgb 'grey' notitle


##################################################################################################################
# PLOT T12
##################################################################################################################

DATA = "../data/s1-depths-q5-t12.dat"
set title "(c)   {/NimbusRomNo9L-ReguItal t} = 12 hrs"
set tmargin at screen 0.45
set bmargin at screen 0.30

splot	DATA u 1:2:($3+$4 > DPMIN ? $3+$4 : NaN) w image, \
		CNTRS u 1:2:1 with lines lw 1.5 lc rgb 'grey' notitle


##################################################################################################################
# PLOT T18
##################################################################################################################

DATA = "../data/s1-depths-q5-t18.dat"
set title "(d)   {/NimbusRomNo9L-ReguItal t} = 18 hrs"
set tmargin at screen 0.20
set bmargin at screen 0.05

splot	DATA u 1:2:($3+$4 > DPMIN ? $3+$4 : NaN) w image, \
		CNTRS u 1:2:1 with lines lw 1.5 lc rgb 'grey' notitle


##################################################################################################################
# PLOT HYDROGRAPHS
##################################################################################################################

unset title
unset xlabel
unset ylabel
unset xtics
unset ytics
unset label 10
unset arrow 10
unset arrow 11
unset arrow 12
unset arrow 13
set xrange [0:*]
set yrange [0:5.5]
set lmargin
set rmargin
set tmargin
set bmargin

set size 0.175,0.075
set origin 0.1,0.8
set object 1 circle at 0,0.45 size graph 0.04 front fc rgb 'black' fs solid
plot HYDRO index 0 u ($1/(60.0*60.0)):2 w lines lt 1 lc rgb 'black'

set size 0.175,0.075
set origin 0.1,0.55
set object 1 circle at 6,2.329
plot HYDRO index 0 u ($1/(60.0*60.0)):2 w lines lt 1 lc rgb 'black'

set size 0.175,0.075
set origin 0.1,0.3
set object 1 circle at 12,3.873
plot HYDRO index 0 u ($1/(60.0*60.0)):2 w lines lt 1 lc rgb 'black'

set size 0.175,0.075
set origin 0.1,0.05
set object 1 circle at 18,1.618
plot HYDRO index 0 u ($1/(60.0*60.0)):2 w lines lt 1 lc rgb 'black'
