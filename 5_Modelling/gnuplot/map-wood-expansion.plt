#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

set terminal pdfcairo enhanced size 5.2,2.5 font "NimbusRomNo9L-Regu,22" fontscale 0.4

set output "../images/map-wood-expansion.pdf"

CNTRS = "../data/contours.dat"
WOODLAND = "../data/woodland-area-expanded.dat"
XMAX = 885
YMAX = 240

set lmargin at screen 0.11
set bmargin at screen 0.17
set tmargin at screen 0.95

set xlabel "{/NimbusRomNo9L-ReguItal x} (m)" offset 0,0.75
set ylabel "{/NimbusRomNo9L-ReguItal y} (m)" offset 1,0
set xrange [0:XMAX]
set yrange [0:YMAX]
set xtics add ("%g" XMAX) font ",18" offset 0,0.25
set ytics add ("%g" YMAX) font ",18"
set style fill transparent pattern 5


##################################################################################################################
# PLOT
##################################################################################################################

load "north-arrow.gnu"

$LOCATIONS << EOD
	70 188 C1
	248 133 C2
	356 101 C3
	460 131 C4
	612 113 C5
	50 160 W1
	190 185 W2
	190 110 W3
	290 75 W4
	320 150 W5
	485 100 F1
	730 180 F2
	755 140 F3
EOD

plot	CNTRS u 1:2:1 w lines lw 1.5 lc rgb 'grey' notitle, \
		WOODLAND u 1:2 w filledcurves lc rgb 'grey50' notitle, \
		$LOCATIONS w labels font ",18" notitle
