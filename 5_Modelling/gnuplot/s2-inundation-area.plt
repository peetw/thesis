#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

set terminal pdfcairo monochrome enhanced size 4,2.5 font "NimbusRomNo9L-Regu,22" fontscale 0.4

DATA = "../data/s2-inundation-area.dat"

set output "../images/s2-inundation-area.pdf"

set xlabel "{/NimbusRomNo9L-ReguItal Q} (yrs)" offset 0,0.5
set ylabel "{/NimbusRomNo9L-ReguItal A_i} (ha)" offset 1,0
set xrange [1:500]
set yrange [0:10]
set logscale x
set xtic add("%g" 500)
set xtics scale 0.5
set ytics scale 0.5
set pointsize 0.5
set key left Left reverse box lw 0.5 height 0.25 width -0.5 samplen 2.5


##################################################################################################################
# PLOT
##################################################################################################################

plot	DATA u 1:($2 / 100.**2) w points pt 1 t "Scenario 1", \
		DATA u 1:($3 / 100.**2) w points pt 2 t "Scenario 2", \
		DATA u 1:($3 / 100.**2 - 0.75):4 w labels notitle font ",18"
