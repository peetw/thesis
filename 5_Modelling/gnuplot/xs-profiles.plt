#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

set terminal pdfcairo monochrome enhanced size 5,2 font "NimbusRomNo9L-Regu,22" fontscale 0.4

set output "../images/xs-profiles.pdf"

DATA = "../data/xs-upstream-downstream.dat"

set xlabel "{/NimbusRomNo9L-ReguItal x} (m)" offset 0,0.5
set ylabel "{/NimbusRomNo9L-ReguItal h} (mAOD)"
set xtics scale 0.5 font ",18"
set ytics 1 scale 0.5 font ",18"
set key Left reverse box lw 0.5 height 0.25 width -1 font ",18"
set multiplot layout 1,2

##################################################################################################################
# PLOT UPSTREAM
##################################################################################################################

set label 1 "(a)" at graph 0.05,0.9 font ",20"
set yrange [20:24]

plot	DATA index 0 u 1:2:3 w filledcurves below lc rgb 'grey' t "Water level", \
		DATA index 0 u 1:2 w lines lt 1 notitle


##################################################################################################################
# PLOT DOWNSTREAM
##################################################################################################################

set label 1 "(b)" at graph 0.05,0.9 font ",20"
set yrange [18:22]

plot	DATA index 1 u 1:2:3 w filledcurves below lc rgb 'grey' t "Water level", \
		DATA index 1 u 1:2 w lines lt 1 notitle
