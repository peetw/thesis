#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

set terminal pdfcairo monochrome enhanced dashed size 4,2.75 font "NimbusRomNo9L-Regu,22" fontscale 0.4

set output "../images/s5-xs-wl-q100.pdf"

DATA = "../data/s5-xs-wl-q100.dat"

set xlabel "{/NimbusRomNo9L-ReguItal y} (m)" offset 0,0.5
set ylabel "{/NimbusRomNo9L-ReguItal h} (mAOD)" offset 1,0
set xrange [240:0]
set yrange [19:25]
set xtics 60 scale 0.5
set ytics scale 0.5
set key bottom Left reverse box lw 0.5 width -1 samplen 2 font ",20"


##################################################################################################################
# PLOT Q100
##################################################################################################################

plot	DATA index 0 u 1:2 w lines lt 1 t "Bed elevation", \
		DATA index 0 u 1:($3 == 0 ? NaN : $2+$3) w lines lt 2 t "{/rtxmi η} Scenario 2", \
		DATA index 1 u 1:($3 == 0 ? NaN : $2+$3) w lines lt 3 lw 2 t "{/rtxmi η} Scenario 5"
