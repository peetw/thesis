#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

set terminal pdfcairo monochrome enhanced dashed font "NimbusRomNo9L-Regu,22" fontscale 0.4

DATA = "../data/long-profiles.csv"

set datafile separator ","

set xlabel "{/NimbusRomNo9L-ReguItal x_s} (km)"
set ylabel "{/NimbusRomNo9L-ReguItal h} (mAOD)"
set yrange [18.5:21.5]
set xtics scale 0.5
set ytics scale 0.5
set format y "%.1f"
set key Left reverse box lw 0.5 width -1
set pointsize 0.4

f(x) = m*x + c
g(x) = n*x + d


##################################################################################################################
# PLOT SURVEY DATA
##################################################################################################################

set output "../images/long-profile-survey.pdf"

fit f(x) DATA index 0 u ($2/1000.):3 via m,c
fit g(x) DATA index 0 u ($2/1000.):4 via n,d

plot	DATA index 0 u ($2/1000.):3 w linespoints lt 1 pt 5 t "Thalweg", \
		f(x) w lines lt 3 title sprintf("Bed slope 1:%.0f", -1000./m), \
		DATA index 0 u ($2/1000.):4 w lines lt 2 t "Water level", \
		g(x) w lines lt 3 title sprintf("WL slope 1:%.0f", -1000./n)


##################################################################################################################
# PLOT MODEL DATA
##################################################################################################################

set terminal pdfcairo size 3.75,2.5
set output "../images/long-profile-model.pdf"

fit f(x) DATA index 1 u ($2/1000.):3 via m,c

plot	DATA index 1 u ($2/1000.):3 w linespoints lt 1 pt 5 t "Thalweg", \
		f(x) w lines lt 3 title sprintf("Bed slope 1:%.0f", -1000./m)


##################################################################################################################
# PLOT CALIBRATION DATA
##################################################################################################################

set terminal pdfcairo size 4.5,3
set output "../images/long-profile-calib.pdf"

$calib << EOD
	11.2805,21.1
	152.685,20.9
	268.426,20.6
	452.128,20.3
	1119.52,19.3
EOD

fit f(x) DATA index 2 u ($2/1000.):3 via m,c
fit g(x) DATA index 2 u ($2/1000.):4 via n,d

plot	DATA index 2 u ($2/1000.):3 w linespoints lt 1 pt 5 t "Thalweg", \
		f(x) w lines lt 3 title sprintf("Bed slope 1:%.0f", -1000./m), \
		DATA index 2 u ($2/1000.):4 w lines lt 2 t "Water level", \
		g(x) w lines lt 3 title sprintf("WL slope 1:%.0f", -1000./n), \
		$calib u ($1/1000.):2 w points pt 2 ps 0.6 t "WL calibration"

!rm fit.log
