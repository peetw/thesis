#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

set terminal pdfcairo monochrome enhanced dashed size 5.2,2 font "NimbusRomNo9L-Regu,22" fontscale 0.4

set output "../images/s3-monit-q100.pdf"

DATA_1 = "../data/s1-monit-q100.dat"
DATA_2 = "../data/s3-monit-q100.dat"
DPMIN = 0.01

set macro
set xlabel "{/NimbusRomNo9L-ReguItal t} (hrs)" offset 0,0.5
set xrange [0:48]
set xtics 12 scale 0.5 font ",18"
set ytics scale 0.5 font ",18"
set multiplot
unset key

lab_plot(s) = sprintf('set label 1 "(%s)" at graph 0.8,0.9 font ",16"', s)
lab_veg(i) = sprintf("W%i", (i-26)/6)
vel(h,e,qx,qy) = (h+e > DPMIN ? sqrt(qx**2 + qy**2) / (h+e) : 0)
shear(tx,ty) = sqrt(tx**2 + ty**2)

VEL = "(vel(column(i),column(i+1),column(i+2),column(i+3)))"
SHEAR = "(shear(column(i+4),column(i+5)))"

LSTYLE="lines lw 2"


##################################################################################################################
# PLOT VELOCITIES
##################################################################################################################

set origin 0,0
set size 0.425,1
set ylabel "{/NimbusRomNo9L-ReguItal U} (m s^{-1})" offset 0.75,0
set ytics 0.1
eval lab_plot("a")

plot	for [i=32:56:6] DATA_1 u ($1/3600.):@VEL w @LSTYLE, \
		for [i=32:56:6] DATA_2 u ($1/3600.):@VEL w @LSTYLE lc rgb 'grey'


##################################################################################################################
# PLOT BED SHEAR
##################################################################################################################

set origin 0.425,0
set ylabel "{/rtxmi τ}_b (N m^{-2})" offset 0.75,0
set ytics 0.4
eval lab_plot("b")

plot	for [i=32:56:6] DATA_1 u ($1/3600.):@SHEAR w @LSTYLE, \
		for [i=32:56:6] DATA_2 u ($1/3600.):@SHEAR w @LSTYLE lc rgb 'grey'


##################################################################################################################
# PLOT KEY
##################################################################################################################

set origin 0.85,0
set size 0.15,1
set yrange [0:1]
set key Left reverse box lw 0.5 height 0.5 width 2 font ",14"
unset xlabel
unset ylabel
unset xtics
unset ytics
unset border
unset label 1

plot	for [i=32:56:6] 1/0 w @LSTYLE t lab_veg(i), \
		for [i=32:56:6] 1/0 w @LSTYLE lc rgb 'grey' t lab_veg(i)
