##################################################################################################################
# SETTINGS
##################################################################################################################

ANGLE = 35

set angles degrees
set arrow 10 from graph 0.94,0.15 length graph 0.03 angle (90-ANGLE) size graph 0.02,15,45 filled front
set arrow 11 from graph 0.94,0.15 length graph 0.04 angle (270-ANGLE) nohead front
set arrow 12 from graph 0.94,0.15 length graph 0.02 angle (180-ANGLE) nohead front
set arrow 13 from graph 0.94,0.15 length graph 0.02 angle (360-ANGLE) nohead front
set label 10 'N' at graph 0.938,0.1575 center rotate by (360-ANGLE) offset graph (0.05*cos(90-ANGLE)),(2*0.05*sin(90-ANGLE)) front font ",16"
