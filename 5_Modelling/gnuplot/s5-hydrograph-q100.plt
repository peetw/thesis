#!/usr/bin/env gnuplot

##################################################################################################################
# SETTINGS
##################################################################################################################

set terminal pdfcairo monochrome enhanced dashed size 3,2 font "NimbusRomNo9L-Regu,22" fontscale 0.4

set output "../images/s5-hydrograph-q100.pdf"

DATA_1 = "../data/s2-hydrographs.dat"
DATA_2 = "../data/s5-hydrograph-q100.dat"

set xlabel "{/NimbusRomNo9L-ReguItal t} (hrs)" offset 0,0.5
set ylabel "{/NimbusRomNo9L-ReguItal Q} (m^{3}s^{-1})" offset 1,0
set xrange [0:48]
set yrange [0:10]
set xtics 12 scale 0.5
set ytics scale 0.5
set key Left reverse box lw 0.5 width -1 samplen 2


##################################################################################################################
# PLOT Q100
##################################################################################################################

plot	DATA_1 index 4 u 1:2 w lines lt 1 t "Upstream", \
		DATA_1 index 4 u 1:3 w lines lt 2 t "Scenario 2", \
		DATA_2 index 0 u 1:3 w lines lt 3 t "Scenario 5"
