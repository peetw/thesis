%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Results \& Discussion}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The data from the previous section are now used to perform a number of modelling scenarios to investigate the hydraulic impact of the floodplain woodland at Lopen Brook. Initially, the flood is allowed to propagate across an empty floodplain without woodland so that baseline flood properties can be determined. This also allows the impact of the choice of bed roughness parameterization (\ie with or without Reynolds number effects) to be discussed. The model is then run for an additional four different scenarios: the existing woodland; replacing the existing woodland with short rotation coppice; expanding the woodland areas to cover the whole floodplain; and a mix of woodland and floodplain storage.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Scenario 1: No Woodland} \label{sec:scenario-1}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

To run the first scenario, the woodland areas are removed and the cells set to open floodplain. The model is then run twice for each input flood hydrograph (Q1 to Q100), with the Chezy coefficient switching from including (Eq.~\ref{eq:cw-transitional}) or excluding (Eq.~\ref{eq:cw-rough}) Reynolds number effects. Each model is run for a total simulated time of 48~hrs, which allows the whole domain to reach a steady state after the flood peak has passed. Hereafter, the models utilizing Chezy numbers that include or exclude Reynolds number effects shall be denoted $C_{Re}$ and $C_\zeta$, respectively.

Before discussing the differences between the $C_{Re}$ and $C_\zeta$ cases, the general flood propagation properties are first described. Taking the flood with the lowest modelled return period (Q5), it can be seen from Fig.~\ref{fig:s1-depths-q5} that the water initially spills over from the main channel onto the downstream floodplain ($t \approx 6$~hrs). After eight hours, patches of the upstream reach also begin to become inundated on both the left and right floodplains. The maximum overland flow depths occur just after the flood peak has passed ($t \approx 12$~hrs). The inundation extent at the downstream reach is limited by the comparatively steep valley walls that run parallel to the main channel. The upstream reach, however, is not fully flooded for the Q5 return period, indicating that there is significant additional flood storage capacity given the relatively flat gradient of the floodplain at this point.

\begin{figure}[htbp]
\centering
\includegraphics[height=0.9\textheight]{s1-depths-q5}
\caption{Flood propagation at Lopen Brook for a Q5 flood. Flow depths shown at: (a) $t = 0$~hrs; (b) $t = 6$~hrs; (c) $t = 12$~hrs; and (d) $t = 18$~hrs.}
\label{fig:s1-depths-q5}
\end{figure}

\begin{figure}[htbp]
\centering
\includegraphics[height=0.9\textheight]{s1-depths-q100}
\caption{Flood propagation at Lopen Brook for a Q100 flood. Flow depths shown at: (a) $t = 0$~hrs; (b) $t = 6$~hrs; (c) $t = 12$~hrs; and (d) $t = 18$~hrs.}
\label{fig:s1-depths-q100}
\end{figure}

The mode of flood propagation for the Q100 return period (Fig.~\ref{fig:s1-depths-q100}) is similar to that of the Q5 flood. The main differences are that the water overflows onto the downstream floodplain earlier ($t \approx 5$~hrs) and that the depths on the floodplain are greater. The flood also extends to cover a much larger area of the upstream floodplain. It should be noted that in all of the modelled flood return periods (Q5--Q100) standing water remains on both the upstream and downstream floodplains due to the slightly raised banks of the main channel.

\begin{figure}[htbp]
\centering
\includegraphics{s1-hydrographs}
\caption{Upstream and downstream flood hydrographs for Lopen Brook in Scenario 1: (a) Q5; (b) Q10; (c) Q25; (d) Q50; and (e) Q100.}
\label{fig:s1-hydrographs}
\end{figure}

The effect of the flow over-topping from the main channel onto the floodplain can be seen in Fig.~\ref{fig:s1-hydrographs}. The hydrographs at the downstream boundary show that the flood peak is delayed by 1~hr and reduced by 17.3\% to 23.2\%. The greatest reduction in peak flood flow at the downstream boundary occurred during the Q100 flood, while the least reduction was observed for the Q5 flood. This is to be expected since the main channel will be able to carry proportionally less of the flow during a larger flood, thus forcing more water out onto the floodplains.

The total inundation area $A_i$ for each modelled flood return period is presented in Fig.~\ref{fig:s1-inundation-area}. It can be seen that the difference in inundation area between the lower return periods (Q5--Q10) is greater than that at the higher return periods (Q50--Q100). This is due to the upper-reach floodplain, which remains relatively dry during the Q5 flood (see Fig.~\ref{fig:s1-depths-q5}), becoming increasingly inundated during the Q10 and Q25 floods. For the Q50 and Q100 return periods, the floodplain flows become deeper rather than expanding since they are constrained by the valley sides.

\begin{figure}[htbp]
\centering
\includegraphics{s1-inundation-area}
\caption{Total inundation area at each return period for the $C_{Re}$ and $C_\zeta$ cases in Scenario 1. The percentage difference between the $C_{Re}$ and $C_\zeta$ cases is also labelled.}
\label{fig:s1-inundation-area}
\end{figure}

The differences in inundation area between the $C_{Re}$ and $C_\zeta$ cases for each return period are also labelled in Fig.~\ref{fig:s1-inundation-area}. There appears to be no correlation with return period, but the magnitudes of the differences indicate that the inundation areas are virtually identical between the two cases at each return period. Comparing the flood velocities and bed shear stress from the $C_{Re}$ and $C_\zeta$ cases results in a similar lack of differentiation.

To illustrate the relative impact of including or excluding Reynolds number effects in the bed friction calculation, the variation in bed shear stress with flow Reynolds number is plot in Fig.~\ref{fig:chezy-shear-comparison}. As expected, the bed shear stresses become independent of Reynolds number effects at the onset of fully developed turbulent flow ($2000 < Re < 4000$). Below this limit the bed shear stresses diverge slightly, with those calculated using a Chezy value incorporating Reynolds number effects being marginally greater than those without. However, this variation is essentially negligible since while the percentage difference may be large, the magnitude of the deviation is minor. For example, at the most reasonable extreme case of $\zeta = 0.5$~m and $Re = 10^3$ ($U = \num{6.5e-4}$~\mps), the bed shear stresses with and without Reynolds number effects are $\tau_b = \num{4.52e-6}$~\stress and $\tau_b = \num{3.53e-6}$~\stress, respectively.

\begin{figure}[htbp]
\centering
\includegraphics{chezy-shear-comparison}
\caption{Bed shear stress calculated using Chezy values derived with and without Reynolds number effects for a range of typical flow depths on Lopen Brook floodplain. The floodplain roughness was set to $k_s = 0.075$~m.}
\label{fig:chezy-shear-comparison}
\end{figure}

Therefore, it is recommended that Reynolds number effects be neglected in the determination of the bed friction on a vegetated floodplain due to the higher computational cost and negligible impact on flood characteristics. Reynolds number effects may play a larger role in determining the bed friction in models where the flow depth is greater and the bed is smoother, \eg tidal estuaries or lagoons.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Scenario 2: Existing Woodland} \label{sec:scenario-2}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The second scenario introduces the woodland planted by the Forestry Commission in 2005, which is modelled using the vegetative drag force and porosity terms described in Eqs.~\eqref{eq:veg-drag} and \eqref{eq:veg-porosity} respectively. The woodland area is shown in Fig.~\ref{fig:map-monit-points} and covers a region of roughly 5~ha at a planting density of 3~m by 2~m (equivalent to 1,666 trees \perhectare or $N = 0.1667$~m$^{-2}$).

\begin{figure}[htbp]
\centering
\includegraphics{map-monit-points}
\caption{Model monitoring points, split into: in-channel (C); woodland (W); and downstream floodplain (F) locations. The woodland extent is marked by the shaded areas.}
\label{fig:map-monit-points}
\end{figure}

The woodland at Lopen Brook is comprised of native species, such as English oak, poplar, willow, birch and alder. A mix of the three species from this study (common alder, black poplar and white willow; see \autoref{sec:hydralab-data}) is therefore used in the model. This was done by randomly assigning one of the three species to each domain cell within the woodland area.

The drag forces were modelled using the Cauchy reconfiguration model (Eqs.~\ref{eq:cauchy-number} and \ref{eq:drag-cauchy}), which required the trees' heights $H$ (Table~\ref{tab:hydralab-dimensions}), main-stem flexural rigidities $EI$ (Table~\ref{tab:hydralab-stiffness}), rigid drag coefficients $C^*_{d0}$ (Table~\ref{tab:Cd-rigid-Re-params}), and Vogel exponents $\psi$ (Table~\ref{tab:vogel-exp-stats}). The variation in projected area $A_{p0}$ with depth for each species is discretized using the species-averaged sigmoid functions in Table~\ref{tab:Ap-vs-height-params}. The trees' average mid-stem diameter ($d = 64.7$~mm), as measured from the site survey (see \autoref{sec:lopen-brook}), was used as the equivalent cylinder diameter in the porosity calculation (Eq.~\ref{eq:veg-porosity}).

The model was run with the same topography and boundary conditions as for Scenario 1, with the addition of the drag force and blockage effect within the woodland areas. The inundation areas for each of the flood return periods (Q5--Q100) are compared to those from Scenario 1 in Fig.~\ref{fig:s2-inundation-area}. It is evident that while the woodland does reduce total inundation area for each of the return periods, the overall effect is very minor (0.03\% to 0.44\%).

\begin{figure}[htbp]
\centering
\includegraphics{s2-inundation-area}
\caption{Total inundation area at each return period for Scenarios 1 and 2. The percentage difference between the scenarios is also labelled.}
\label{fig:s2-inundation-area}
\end{figure}

Similarly, the woodland had a negligible impact on the downstream flood hydrograph. This is due to a combination of the low planting density and the topography of the case study site. The Lopen Brook floodplain can be split into upstream ($x < 400$~m) and downstream ($x > 400$~m) sections, where only the main channel conveys flow between the two (see Fig.~\ref{fig:s1-depths-q5}c). As previously discussed, the main channel banks are slightly raised and the flood water does not fully drain from the floodplains. Therefore, the increased resistance provided by the woodland on the upper reach does not translate to a reduction or delay in peak flood flows at the downstream boundary.

While the introduction of floodplain woodland in the current configuration may not result in a noticeable change to the flood hydrograph, it is expected that there will be a reduction in flow velocities within the vegetated areas. In order to capture the effects of the woodland on the flow properties, a number of monitoring points are chosen for further inspection. The exact locations of the points are shown in Fig.~\ref{fig:map-monit-points} and they can be split into three groups: five points within the main channel (C1--C5); five points within the woodland area (W1--W5); and three points on the downstream floodplain (F1--F3).

For brevity, the data for the Q5--Q50 floods are not discussed herein since they are similar in pattern to the Q100 flood. The variation in flow properties at each of the monitoring points over the duration of a Q100 flood is presented in Fig.~\ref{fig:s2-monit-q100}. It can be seen that around 5~cm to 20~cm of water is left standing on the upstream floodplain, while the downstream floodplain is more inundated with 10~cm to 30~cm of water remaining once the flood peak passes.

The velocities within the main channel rise and fall in a similar manner to the flood hydrograph, as might be predicted. On the other hand, the velocities at the woodland monitoring locations show that the water on the upstream floodplains is only flowing during the initial over-topping and expansion period. The monitoring points on the downstream floodplains show a mixture of the two behaviours, with the flow on the right floodplain (F3) travelling at approximately the same velocity as the main channel for $t > 24$~hrs.

\begin{figure}[htbp]
\centering
\includegraphics{s2-monit-q100}
\caption{Flow depths, velocities and bed shear stresses for Scenario 2 during a Q100 flood at each of the model monitoring points: in-channel C1--C5 (a,d,g); woodland W1--W5 (b,e,h); and downstream floodplain F1--F3 (c,f,i).}
\label{fig:s2-monit-q100}
\end{figure}

A comparison of the flow properties at each of the monitoring points for a Q100 flood between the current scenario and Scenario 1 is provided in Table~\ref{tab:s2-monit-q100}. Both the differences between the simulation-average and maximum values are calculated, with negative values indicating a reduction from Scenario 1. Overall there is a decrease in nearly every flow property at each of the monitoring sites. However, the magnitude of the reductions for the in-channel and downstream floodplain locations are essentially negligible, with all but two reductions being less than 1\%. 

The reduction in velocities and bed shear stresses within the woodland areas is more significant. For example, the peak velocities at points W1--W5 are decreased by up to 4.02\%, with similar reductions for the average velocities. The peak bed shear stresses at the same points are reduced by up to 13.8\% and the average stresses are reduced by up to 12.3\%. The flow depths within the woodland areas are much less sensitive, only decreasing by a maximum of 0.77\%. The bed shear stress is more strongly affected by the vegetation since it is dependent on the flow velocity and wetted bed area, both of which are modified by its presence.

\begin{table}[htbp]
\caption{Differences in average and maximum flow properties between Scenarios 1 and 2 for a Q100 flood. Monitoring point locations are split into in-channel (C), woodland (W) and downstream floodplain (F) areas.}
\centering
\sisetup{table-format=3.2}
\begin{footnotesize}
\begin{tabular}{lSSSSS[table-format=4.2]S[table-format=4.2]}
\toprule
 & {$\Delta \zeta_\textnormal{avrg}$} & {$\Delta \zeta_\textnormal{max}$} & {$\Delta U_\textnormal{avrg}$} & {$\Delta U_\textnormal{max}$} & {$\Delta \tau_{b_\textnormal{avrg}}$} & {$\Delta \tau_{b_\textnormal{max}}$} \\
Location & {(\%)} & {(\%)} & {(\%)} & {(\%)} & {(\%)} & {(\%)} \\ \midrule
C1 & 0.01 & -0.06 & 0.02 & 0.24 & 0.02 & 0.5 \\
C2 & -0.02 & 0 & -0.04 & -0.05 & -0.19 & -0.04 \\
C3 & -0.04 & -0.07 & -0.06 & -0.99 & -0.27 & -2.04 \\
C4 & -0.01 & -0.07 & -0.03 & 0 & -0.06 & -0.03 \\
C5 & 0 & 0 & -0.05 & -0.35 & -0.07 & -0.97 \\ \midrule
W1 & -0.28 & -0.77 & 0.35 & -1.7 & -0.44 & -2.88 \\
W2 & -0.19 & -0.77 & -2.73 & -4.02 & -12.26 & -13.76 \\
W3 & -0.67 & -0.33 & -4.89 & -1.42 & -12.04 & -9.25 \\
W4 & -0.06 & -0.44 & -0.09 & -0.29 & -5.79 & -5.36 \\
W5 & -0.17 & -0.66 & -1.25 & -2.21 & -10.06 & -10.15 \\ \midrule
F1 & -0.04 & -0.26 & -0.44 & -0.65 & -0.64 & -1.36 \\
F2 & -0.06 & -0.37 & 0.02 & -0.29 & -0.02 & -0.73 \\
F3 & -0.02 & 0 & -0.02 & -0.24 & -0.04 & -0.36 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:s2-monit-q100}
\end{table}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Scenario 3: Short Rotation Coppice} \label{sec:scenario-3}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Although the results from the second scenario suggest that increasing the resistance of the upstream floodplain has little effect on the downstream flood hydrograph, the woodland none-the-less reduced local flow velocities and bed shear stresses. To further investigate this phenomenon, the native woodland is replaced by short rotation coppice (SRC), which is typically planted at much greater densities and therefore imposes more resistance to the flood flow.

The establishment of willow and poplar SRC plantations has more in common with agriculture than forestry. The land is initially ploughed and prepared before planting and, in the UK, the first season's growth is cut back to encourage production of more shoots the following year. The trees are then harvested at periods of 2 to 4~years using specialized machinery. Several of these 2 to 4~year cycles can take place before the crop needs to be replaced due to declining yields.

The wood from SRC is generally used as a low carbon energy source, with the main benefits being a reduced demand for chemical inputs and maintenance when compared to conventional arable crops and an enhancement to local biodiversity \citep{Tubby-02}. Further, it has been shown that SRC may play an important hydrological role in reducing flood risk by reducing water yields up to 50\% when planted in place of grass or arable crops \citep{Hall-96}. As a result of the high water uptake, SRC can also help tackle nitrate and other diffuse pollution \citep{Elowson-94}.

The optimal planting density and harvesting frequency of SRC was examined by \citet{Bullard-02}. The authors considered the yield from planting densities of 8,625 to 111,000 trees \perhectare and found that a density of 15,625 trees \perhectare provided the best economic return over the SRC's lifetime. \citet{Defra-04} state that the current best practice for commercial willow SRC crops is a planting pattern consisting of rectangular grids with 0.75~m by 0.59~m spacing and 1.5~m between each twin row for access. This equates to 15,000 trees \perhectare ($N = 1.5$~m$^{-2}$) and, therefore, the same density will be used in this scenario. The SRC are modelled using the same parameters as for the willow trees in the previous scenario, with the exception that the species-specific drag coefficient is raised slightly to $C_{d\chi} = 1.5$ to account for the multi-stem nature of coppiced trees.

The model is run again using the same topography and boundary conditions as for the first scenario. For brevity, only the Q100 flood is discussed herein since the lower flood return periods follow a similar pattern. As predicted, the increased upstream floodplain resistance has a negligible impact on the hydrograph compared to the first scenario. The total inundation area also remains relatively constant, with the inundation area for the Q100 flood being only 0.06\% less than Scenario 1.

\begin{figure}[htbp]
\centering
\includegraphics{s3-monit-q100}
\caption{Comparison of velocities (a) and bed shear stresses (b) for a Q100 flood between Scenarios 1 (black) and 3 (grey) at each of the woodland monitoring points.}
\label{fig:s3-monit-q100}
\end{figure}

The dense planting pattern for the SRC does, however, have a significant impact on the velocities and bed shear stresses within the woodland areas (Fig.~\ref{fig:s3-monit-q100}). From Table~\ref{tab:s3-monit-q100} it can be seen that the average and maximum velocities are reduced by up to 10.9\% and 7.1\%, respectively. The in-channel and downstream floodplain values are omitted from Table~\ref{tab:s3-monit-q100} as there are very similar to those from the second scenario (Table~\ref{tab:s2-monit-q100}).

\begin{table}[htbp]
\caption{Differences in average and maximum flow properties between Scenarios 1 and 3 for a Q100 flood at each of the woodland monitoring points.}
\centering
\sisetup{table-format=3.2}
\begin{footnotesize}
\begin{tabular}{lSSS[table-format=4.2]SS[table-format=4.2]S[table-format=4.2]}
\toprule
 & {$\Delta \zeta_\textnormal{avrg}$} & {$\Delta \zeta_\textnormal{max}$} & {$\Delta U_\textnormal{avrg}$} & {$\Delta U_\textnormal{max}$} & {$\Delta \tau_{b_\textnormal{avrg}}$} & {$\Delta \tau_{b_\textnormal{max}}$} \\
Location & {(\%)} & {(\%)} & {(\%)} & {(\%)} & {(\%)} & {(\%)} \\ \midrule
W1 & 0.18 & 0.77 & -6.72 & -1.08 & -46.08 & -60.77 \\
W2 & -0.62 & 0.77 & -2.88 & -0.08 & -37.5 & -36.13 \\
W3 & -1.57 & 0.33 & -0.15 & -3.68 & -38.14 & -41.68 \\
W4 & 0.43 & 0.88 & -7.28 & -5.68 & -43.27 & -42.73 \\
W5 & 0.5 & 0.98 & -10.99 & -7.1 & -49.29 & -46.16 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:s3-monit-q100}
\end{table}

The lower velocities in the floodplain woodland areas result in greatly reduced bed shear stresses, with the average and maximum bed shear stresses decreasing by up to 53.6\% and 64.1\%, respectively. The reduction is particularly visible at point W4 in Fig.~\ref{fig:s3-monit-q100}, where the peak bed shear stress is reduced from approximately 1.15~\stress to 0.65~\stress.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Scenario 4: Woodland Expansion} \label{sec:scenario-4}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The previous scenarios have shown that the addition of riparian woodland at Lopen Brook can have a significant impact on local flow properties, namely velocity and bed shear stress. However, these effects remain localized and the downstream flow is essentially unaffected. Therefore, the current scenario expands the SRC from the previous scenario so that the entire floodplain is covered (Fig.~\ref{fig:map-wood-expansion}). This assumes that the full area is suitable for planting, but will none-the-less provide a model for the maximum impact that SRC can achieve at this site.

\begin{figure}[htbp]
\centering
\includegraphics{map-wood-expansion}
\caption{Expansion of the SRC (shaded areas) to cover the entire floodplain at Lopen Brook. Model monitoring points are split into: in-channel (C); original woodland (W); and downstream floodplain (F) locations.}
\label{fig:map-wood-expansion}
\end{figure}

The model is run again using the same topography and boundary conditions as for the previous scenarios. For brevity, only the Q100 flood is discussed herein since the lower flood return periods follow a similar pattern. The addition of the SRC onto the downstream floodplain has a negligible impact on the downstream flood hydrograph for the Q100 case when compared to the first scenario (Fig.~\ref{fig:s4-hydrograph-q100}). The total inundation area is similarly insensitive and is only 0.06\% greater than for Scenario 1. The minor increase in total inundation area is most likely a result of raised water levels due to the blockage effect of the SRC.

\begin{figure}[htbp]
\centering
\includegraphics{s4-hydrograph-q100}
\caption{Downstream flood hydrographs during a Q100 flood for Scenarios 1 and 4.}
\label{fig:s4-hydrograph-q100}
\end{figure}

Indeed, when comparing the flow depths at each of the monitoring points to those of the first scenario, it can be seen that the average and maximum flow depths at the upstream and downstream woodland locations are around 1\% to 2\% greater (Table~\ref{tab:s4-monit-q100}). As might be expected, the most notable effect of the additional SRC is a reduction in the flow velocities and bed shear stresses on the downstream floodplain. For these points, Table~\ref{tab:s4-monit-q100} reports that the average and maximum velocities are reduced by 13.6\% and 10.5\%, respectively, while the average and maximum bed shear stresses are decreased by up to 51.5\% and 49.4\%, respectively.

\begin{table}[htbp]
\caption{Differences in average and maximum flow properties between Scenarios 1 and 4 for a Q100 flood. Monitoring point locations are split into in-channel (C), woodland (W) and downstream floodplain (F) areas.}
\centering
\sisetup{table-format=4.2}
\begin{footnotesize}
\begin{tabular}{lSSSSSS}
\toprule
 & {$\Delta \zeta_\textnormal{avrg}$} & {$\Delta \zeta_\textnormal{max}$} & {$\Delta U_\textnormal{avrg}$} & {$\Delta U_\textnormal{max}$} & {$\Delta \tau_{b_\textnormal{avrg}}$} & {$\Delta \tau_{b_\textnormal{max}}$} \\
Location & {(\%)} & {(\%)} & {(\%)} & {(\%)} & {(\%)} & {(\%)} \\ \midrule
C1 & -0.3 & 0 & -0.7 & -0.56 & -0.69 & -1.08 \\
C2 & -0.21 & 0.07 & -0.66 & -0.6 & -0.65 & -1.27 \\
C3 & -0.08 & 0.07 & -1.25 & -3.11 & -2.17 & -5.66 \\
C4 & 0.14 & 0.14 & -1.76 & -1.89 & -3.28 & -3.81 \\
C5 & 0.3 & 0.36 & -2.5 & -3.41 & -4.26 & -7.16 \\ \midrule
W1 & 0.03 & 0.77 & -7.43 & -7.06 & -48.51 & -65.1 \\
W2 & 0.67 & 0.77 & -2.77 & -0.28 & -37.23 & -36.89 \\
W3 & 0.98 & 0.33 & -0.03 & -2.69 & -37.38 & -40.67 \\
W4 & 0.51 & 0.88 & -7.22 & -5.61 & -43.12 & -42.75 \\
W5 & 0.72 & 0.98 & -10.7 & -6.89 & -49.18 & -46.04 \\ \midrule
F1 & 0.71 & 0.53 & -1.91 & -3.25 & -35.42 & -33.35 \\
F2 & 2.34 & 1.12 & -13.65 & -10.53 & -51.49 & -49.42 \\
F3 & 1.02 & 0.75 & -10.14 & -7.98 & -48.92 & -47.36 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:s4-monit-q100}
\end{table}

The evolution of the flow velocities and bed shear stresses at the downstream floodplain monitoring locations is presented in Fig.~\ref{fig:s4-monit-q100}. The reduction in values compared to the first scenario is particularly evident at points F2 and F3 (see Fig.~\ref{fig:map-wood-expansion}). This is because these points are located towards the end of the downstream reach, where the flood water has been significantly retarded by the SRC as it flows parallel to the main channel.

\begin{figure}[htbp]
\centering
\includegraphics{s4-monit-q100}
\caption{Comparison of flow velocities (a) and bed shear stresses (b) for a Q100 flood between Scenarios 1 (black) and 4 (grey) at each of the downstream floodplain monitoring points.}
\label{fig:s4-monit-q100}
\end{figure}

It can also be seen from Table~\ref{tab:s4-monit-q100} that the expansion of the SRC to cover the entire floodplain has reduced the velocities and bed shear stresses within the main channel, especially towards the downstream boundary (points C3 to C5). This is in contrast with the previous scenario where the SRC was confined to the extent of the original woodland and had a relatively negligible impact on in-channel flow. The 7\% reduction in peak bed shear stress at point C5 indicates that the expansion of the SRC may decrease the amount of bed erosion and sediment transport within the main channel, thus helping to minimize sediment deposition further downstream.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Scenario 5: Floodplain Storage} \label{sec:scenario-5}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In the final scenario, the potential of floodplain storage to reduce the flood peak at Lopen Brook is investigated. This is achieved by restoring two floodplain bunds that were created by the Forestry Commission in 2005. Originally, the bunds ran perpendicular to the main channel near the downstream boundary and were approximately 0.5~m high (see Fig.~\ref{fig:photo-bund}). However, they have since subsided somewhat, which has been further exacerbated by the movement of livestock (cattle, horses, \etc) and are now barely visible.

\begin{figure}[htbp]
\centering
\includegraphics[width=0.75\textwidth]{photo-bund}
\caption{View of the previous floodplain bund at the downstream reach of Lopen Brook.}
\label{fig:photo-bund}
\end{figure}

The bunds were introduced back into the DEM as close as possible to their original positions. Each bund is 0.5~m high by 1~m wide and extends for roughly 30~m on either side of the main channel until they reach the valley side slopes. The elevation and locations of the restored floodplain bunds are shown in Fig.~\ref{fig:map-bund}.

\begin{figure}[htbp]
\centering
\includegraphics{map-bund}
\caption{Location of the bunds on the downstream floodplain at Lopen Brook. Shaded areas indicate woodland extent. Model monitoring points are split into: in-channel (C); original woodland (W); and downstream floodplain (F) locations.}
\label{fig:map-bund}
\end{figure}

The SRC from the previous two scenarios is replaced with the existing woodland (Scenario 2) so that the impact of the bunds can be examined with respect to the current conditions at Lopen Brook. The model is run again using the same boundary conditions as the previous scenarios. For brevity, only the Q100 flood is discussed herein since the lower flood return periods follow a similar pattern.

The downstream flood hydrograph for the Q100 flood is presented in Fig.~\ref{fig:s5-hydrograph-q100}. The introduction of the bunds reduce and delay the downstream flood peak by 32.5\% and 2~hrs, respectively, when compared to the upstream boundary and by 12.1\% and 1~hr, respectively, when compared to the second scenario. The reduction and delay in peak flow is due to the increased storage capacity of the downstream floodplain where, in previous scenarios, water could flow freely towards the downstream boundary. It can also be seen that the tail of the hydrograph is slightly delayed, which is caused by the water on the downstream floodplain draining back into the main channel once the water level in the main channel begins to drop.

\begin{figure}[htbp]
\centering
\includegraphics{s5-hydrograph-q100}
\caption{Downstream flood hydrographs during a Q100 flood for Scenarios 2 and 5.}
\label{fig:s5-hydrograph-q100}
\end{figure}

The increase in floodplain storage due to the bunds is clearly visible in Fig.~\ref{fig:s5-xs-wl-q100}, which compares the water surface elevations between Scenarios 2 and 5 for a cross-section located at $x = 700$~m. The data were taken at the time of maximum flow depths on the downstream floodplain ($t = 12$~hrs), just after the peak on the upstream flood hydrograph. The cross-section location is chosen to be representative of the downstream floodplain overall. It can be seen that the water level rises so that the left hand floodplain is no longer disconnected from the main channel at the highest flow rates.

\begin{figure}[htbp]
\centering
\includegraphics{s5-xs-wl-q100}
\caption{Cross-section of Lopen Brook at $x = 700$~m showing water surface elevation at $t = 12$~hrs during a Q100 flood for Scenarios 2 and 5.}
\label{fig:s5-xs-wl-q100}
\end{figure}

The magnitudes of the bunds' effects are summarized in Table~\ref{tab:s5-monit-q100} for the in-channel and downstream floodplain locations. The woodland points (W) are omitted since the backwater effects of the bunds do not extend to these points and hence the differences in flow properties are very minor. The main change is the increase in flow depths, both on the downstream floodplain (as seen in Fig.~\ref{fig:s5-xs-wl-q100}) and also in the main channel, with the effects tailing off at around point C3. On the downstream floodplain, the average and maximum flow depths are raised by up to 60.7\% and 102.6\%, respectively.

As a result of the increased flow depths, the inundation area for the current scenario is 5.48\% greater than for the second scenario. While the inundation area would typically be kept to a minimum during flood risk management, the current case study is confined to a rural valley, where the additional inundation area will cause relatively little damage as opposed to a more developed area. Therefore, it is assumed that the modest increase in inundation area in this scenario is acceptable given the reduction and delay to peak flow at the downstream boundary.

\begin{table}[htbp]
\caption{Differences in average and maximum flow properties between Scenarios 2 and 5 for a Q100 flood. Monitoring point locations are split into in-channel (C) and downstream floodplain (F) areas.}
\centering
\sisetup{table-format=4.2}
\begin{footnotesize}
\begin{tabular}{lSSSSSS}
\toprule
 & {$\Delta \zeta_\textnormal{avrg}$} & {$\Delta \zeta_\textnormal{max}$} & {$\Delta U_\textnormal{avrg}$} & {$\Delta U_\textnormal{max}$} & {$\Delta \tau_{b_\textnormal{avrg}}$} & {$\Delta \tau_{b_\textnormal{max}}$} \\
Location & {(\%)} & {(\%)} & {(\%)} & {(\%)} & {(\%)} & {(\%)} \\ \midrule
C1 & 0.75 & 0.06 & -1.3 & -0.23 & -2.29 & -0.44 \\
C2 & 1.67 & -0.07 & -2.52 & -0.45 & -4.49 & -0.78 \\
C3 & 2.37 & 0.14 & -7.61 & -0.36 & -16.11 & -0.7 \\
C4 & 3.95 & 7.68 & -15.72 & -7.73 & -29.41 & -15.11 \\
C5 & 5.51 & 14.71 & -40.26 & -7.67 & -55.78 & 0.04 \\ \midrule
F1 & 25.92 & 32.19 & -18.34 & -1.56 & -20.6 & -6.43 \\
F2 & 60.69 & 102.61 & -54.26 & -6.22 & -54.98 & -26.61 \\
F3 & 33.35 & 68.66 & -13.29 & -4.32 & -34.54 & -28.76 \\ \bottomrule
\end{tabular}
\end{footnotesize}
\label{tab:s5-monit-q100}
\end{table}

In conjunction with the raised water levels of the floodplain flows, the velocities and bed shear stresses are correspondingly lower than those in the second scenario. From Table~\ref{tab:s5-monit-q100}, the average and maximum velocities are found to be decreased by up to 54.3\% and 6.22\%, respectively. The greater reduction in the average velocities compared to the maximum velocities is due to maximum velocities occurring during the initial wetting period when the presence of the bunds have minimal impact. On the other hand, once the floodplains become inundated, the bunds restrict the flow and thus the average velocities are reduced.
