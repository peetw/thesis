#!/usr/bin/env python

import os
import errno
import subprocess
import re
import pandas as pd
import matplotlib.pyplot as plt
import datetime

# Output directory and filename for graph
OUT_DIR = os.path.join(os.path.dirname(os.path.dirname(os.path.realpath(__file__))), "useful")
PDF_FILE = os.path.join(OUT_DIR, "git-stats.pdf")

# Create output directory if not exist
try:
	os.makedirs(OUT_DIR)
except OSError as exception:
	if exception.errno != errno.EEXIST:
		raise

# Initialize counts
total_insertions = 0
total_deletions = 0
timeseries = []
rows = []

# Get list of commits that modify either tex or plt files
cmd = "git log --date=iso --stat --reverse -- *.tex -- *.plt"
git_log = subprocess.check_output(cmd.split()).splitlines()

# Iterate over lines and parse info
for line in git_log:
	# Get commit SHA1
	if re.match('commit ', line):
		words = line.split()
		commit = words[1]
	# Get date time for commit
	elif re.match('Date:   ', line):
		words = line.split()
		date = words[1]
		time = words[2]
		date_time = datetime.datetime.strptime(date + ' ' + time, '%Y-%m-%d %H:%M:%S')
	# Get number of lines added or deleted and also total words in commit
	elif re.match(' (\d+) files changed', line):
		insertions = re.findall('(\d+) insertions?\(\+\)', line)
		deletions = re.findall('(\d+) deletions?\(-\)', line)

		if insertions:
			total_insertions += int(insertions[0])
		if deletions:
			total_deletions += int(deletions[0])

		# Get word count
		cmd = "git ls-tree -r --name-only" + " " + commit
		gitls = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
		cmd = "grep -e .*\.tex -e .*\.plt"
		files = subprocess.check_output(cmd.split(), stdin=gitls.stdout)
		cmd = "git archive" + " " + commit + " " + files
		gitarchive = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
		cmd = "tar -x -O"
		tararchive = subprocess.Popen(cmd.split(), stdin=gitarchive.stdout, stdout=subprocess.PIPE)
		cmd = "wc -w"
		word_count = int(subprocess.check_output(cmd.split(), stdin=tararchive.stdout))

		# Add data to rows
		timeseries.append(date_time)
		row = {'Total_lines': total_insertions - total_deletions,
			'Total_insertions': total_insertions,
			'Total_deletions': total_deletions,
			'Total_words': word_count}
		rows.append(row)


# Create DataFrame from list of rows
data = pd.DataFrame(rows)

# Plot data
f, (ax1, ax2) = plt.subplots(2, sharex=True)
f.suptitle("Thesis LaTeX and gnuplot source code stats", fontsize=16)

ax1.plot(timeseries, data['Total_lines'], label='Total lines')
ax1.plot(timeseries, data['Total_insertions'], label='Total insertions')
ax1.plot(timeseries, data['Total_deletions'], label='Total deletions')
ax1.set_ylabel("Lines of code")
ax1.legend(loc='upper left', fontsize=12)

ax2.plot(timeseries, data['Total_words'], label='Total words')
ax2.set_ylim(bottom=0)
ax2.set_ylabel("No. of words")
ax2.legend(loc='upper left', fontsize=12)

f.autofmt_xdate()
f.subplots_adjust(hspace=0.1)
plt.setp([a.get_xticklabels() for a in f.axes[:-1]], visible=False)

plt.savefig(PDF_FILE)
