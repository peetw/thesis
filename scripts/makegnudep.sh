#!/bin/sh

# Parses a list of gnuplot files (with extensions .plt or .gnu) and outputs
# the dependencies in a Makefile format.

# Loop over input files
for file in $@; do
	# Get outputs
	outputs=$(grep "$file" -H -e "set out" -e "RESULTS =" | sed 's/\(.*\/\).*\.[pltgnu]*.*"\(.*\)"/\1\2/' | tr '\n' ' ' | sed 's/[[:space:]]*$//')

	# Get inputs
	inputs=$(grep "$file" -H -e "DATA =" | sed 's/\(.*\/\).*\.[pltgnu]*.*"\(.*\)"/\1\2/' | tr '\n' ' ' | sed 's/[[:space:]]*$//')

	# Write dependencies
	echo "$outputs: $file $inputs"
done
