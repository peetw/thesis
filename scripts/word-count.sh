#!/bin/sh

viewHelp () {
cat << EOF
# -----------------------------------------------------------------------------
# Uses texcount to count words in thesis and outputs coloured HTML
#
#   Usage:
#       $0 [options]
#
#   Options:
#       -p    Count words using pdftotext and output to term (less accurate)
#       -h    View this text
# -----------------------------------------------------------------------------
EOF
[ "$1" ] && echo "\n  *** $1 ***\n"
exit 1
}

# Default values
SCRIPT_DIR="$(cd "$(dirname "$0")"; pwd -P)"
BASE_DIR="$(dirname "$SCRIPT_DIR")"
PDF="$BASE_DIR/Thesis.pdf"
TEX="$BASE_DIR/Thesis.tex"
mkdir -p "$BASE_DIR/useful"
HTML="$BASE_DIR/useful/word-count.html"

parseResult () {
	LINES=$(echo "$1" | awk '{print $1}')
	WORDS=$(echo "$1" | awk '{print $2}')
	CHARS=$(echo "$1" | awk '{print $3}')
}

pdfCount () {
	TMP=$(mktemp)
	pdftotext "$1" - | tr " " "\n" | sort | uniq | grep "^[A-Za-z]" | sed 's/\(^[a-zA-Z]*\)[[:punct:]]*.*/\1/g' | uniq > "$TMP"
	parseResult "$(pdftotext "$1" - | grep -f "$TMP" | sed 's/[[:punct:]]//g' | wc)"
	echo "$(basename "$1") contains: $LINES lines; $WORDS words; $CHARS characters."
	rm "$TMP"
}

# Process command line options
while getopts ":ph" opt; do
	case $opt in
		p)
			PDF_COUNT=1
			;;
		h)
			viewHelp
			;;
		\?)
			viewHelp "Invalid option: -$OPTARG"
			;;
		:)
			viewHelp "Option -$OPTARG requires an argument"
			;;
	esac
done

# Run advanced word count by default
if [ "$PDF_COUNT" ]; then
	pdfCount "$PDF"
else
	mkdir -p "$(dirname "$HTML")"
	texcount -v3 -html -inc -dir="$DIR" "$TEX" > "$HTML"
fi
