#!/bin/sh

SCRIPT_DIR="$(cd "$(dirname "$0")"; pwd -P)"
BASE_DIR="$(dirname "$SCRIPT_DIR")"
INPUT="$BASE_DIR/Thesis.pdf"
OUTPUT="$BASE_DIR/optimized.pdf"
TMP=$(mktemp)
METADATA=$(mktemp)

# Ghostscript overrides original metadata so save original
pdftk "$INPUT" dump_data_utf8 > "$METADATA"

# Optimize
ps2pdf -dPDFSETTINGS=/prepress "$INPUT" "$TMP"

# Restore metadata
pdftk "$TMP" update_info_utf8 "$METADATA" output "$OUTPUT"

# Clean
rm -f "$TMP" "$METADATA"
