#!/bin/sh

viewHelp () {
cat << EOF
# -----------------------------------------------------------------------------
# Recursively searches TeX (or gnuplot) files for the given pattern
#
#   Usage:
#       $0 [options] pattern
#
#   Options:
#       -f    Full search (checks backup directories)
#       -p    Search gnuplot files
#       -l    Only list file names (omit context)
#       -h    View this text
# -----------------------------------------------------------------------------
EOF
[ "$1" ] && echo "\n  *** $1 ***\n"
exit 1
}

# Default values
grep_opts="--colour=always -rn"
ext="tex"

# Process command line options
while getopts ":fplh" opt; do
	case $opt in
		f)
			full_search=1
			;;
		p)
			ext="plt"
			;;
		l)
			grep_opts="${grep_opts}l"
			;;
		h)
			viewHelp
			;;
		\?)
			viewHelp "Invalid option: -$OPTARG"
			;;
		:)
			viewHelp "Option -$OPTARG requires an argument"
			;;
	esac
done

# Remove options from argument list and check for arguments
shift $((OPTIND-1))
if [ "$#" -lt 1 ]; then
	viewHelp "Please specify a search pattern"
fi

# Search in main directory
SCRIPT_DIR="$(cd "$(dirname "$0")"; pwd -P)"
BASE_DIR="$(dirname "$SCRIPT_DIR")"
cd "$BASE_DIR"

if [ "$full_search" ]; then
	grep $grep_opts --include="*\.$ext" "$1" | sort
else
	grep $grep_opts --include="*\.$ext" --exclude-dir="bak" "$1" | sort
fi
